/**
  ******************************************************************************
  * File Name          : main.hpp
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  * This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* Includes ------------------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define MCU_SYS_POWER_Pin GPIO_PIN_13
#define MCU_SYS_POWER_GPIO_Port GPIOC
#define MCU_AP_NRESET_Pin GPIO_PIN_0
#define MCU_AP_NRESET_GPIO_Port GPIOC
#define MCU_ADC_KEY1_Pin GPIO_PIN_1
#define MCU_ADC_KEY1_GPIO_Port GPIOC
#define MCU_ADC_KEY2_Pin GPIO_PIN_2
#define MCU_ADC_KEY2_GPIO_Port GPIOC
#define MCU_ADC_BAT_Pin GPIO_PIN_3
#define MCU_ADC_BAT_GPIO_Port GPIOC
#define MCU_ADC_VEHICLE_Pin GPIO_PIN_1
#define MCU_ADC_VEHICLE_GPIO_Port GPIOA
#define MCU_TX2_DVR_Pin GPIO_PIN_2
#define MCU_TX2_DVR_GPIO_Port GPIOA
#define MCU_RX2_DVR_Pin GPIO_PIN_3
#define MCU_RX2_DVR_GPIO_Port GPIOA
#define MCU_GPIN_PM25_Pin GPIO_PIN_4
#define MCU_GPIN_PM25_GPIO_Port GPIOC
#define MCU_GPOUT_PM25_Pin GPIO_PIN_5
#define MCU_GPOUT_PM25_GPIO_Port GPIOC
#define MCU_RCAM_POWER_Pin GPIO_PIN_0
#define MCU_RCAM_POWER_GPIO_Port GPIOB
#define MCU_ACC_DETECT_Pin GPIO_PIN_1
#define MCU_ACC_DETECT_GPIO_Port GPIOB
#define MCU_AMP_DIAG_Pin GPIO_PIN_2
#define MCU_AMP_DIAG_GPIO_Port GPIOB
#define MCU_TX3_TPMS_Pin GPIO_PIN_10
#define MCU_TX3_TPMS_GPIO_Port GPIOB
#define MCU_RX3_TPMS_Pin GPIO_PIN_11
#define MCU_RX3_TPMS_GPIO_Port GPIOB
#define MCU_LED1_Pin GPIO_PIN_12
#define MCU_LED1_GPIO_Port GPIOB
#define MCU_LED2_Pin GPIO_PIN_13
#define MCU_LED2_GPIO_Port GPIOB
#define MCU_AMP_EN_Pin GPIO_PIN_14
#define MCU_AMP_EN_GPIO_Port GPIOB
#define MCU_BL_EN_Pin GPIO_PIN_15
#define MCU_BL_EN_GPIO_Port GPIOB
#define MCU_CANPHY_EN_Pin GPIO_PIN_6
#define MCU_CANPHY_EN_GPIO_Port GPIOC
#define MCU_CANPHY_STB_Pin GPIO_PIN_7
#define MCU_CANPHY_STB_GPIO_Port GPIOC
#define MCU_CANPHY_ERR_Pin GPIO_PIN_8
#define MCU_CANPHY_ERR_GPIO_Port GPIOC
#define MCU_AMP_MUTE_Pin GPIO_PIN_9
#define MCU_AMP_MUTE_GPIO_Port GPIOC
#define MCU_BTNPWM_TIM1CH1_Pin GPIO_PIN_8
#define MCU_BTNPWM_TIM1CH1_GPIO_Port GPIOA
#define MCU_TX1_TERM_Pin GPIO_PIN_9
#define MCU_TX1_TERM_GPIO_Port GPIOA
#define MCU_RX1_TERM_Pin GPIO_PIN_10
#define MCU_RX1_TERM_GPIO_Port GPIOA
#define MCU_BLPWM_TIM1CH4_Pin GPIO_PIN_11
#define MCU_BLPWM_TIM1CH4_GPIO_Port GPIOA
#define MCU_LCD_PWRON_Pin GPIO_PIN_12
#define MCU_LCD_PWRON_GPIO_Port GPIOA
#define MCU_BUZZER_Pin GPIO_PIN_15
#define MCU_BUZZER_GPIO_Port GPIOA
#define MCU_TX4_AP_Pin GPIO_PIN_10
#define MCU_TX4_AP_GPIO_Port GPIOC
#define MCU_RX4_AP_Pin GPIO_PIN_11
#define MCU_RX4_AP_GPIO_Port GPIOC
#define MCU_GPIO2_AP_Pin GPIO_PIN_3
#define MCU_GPIO2_AP_GPIO_Port GPIOB
#define MCU_GPIO1_AP_Pin GPIO_PIN_4
#define MCU_GPIO1_AP_GPIO_Port GPIOB
#define MCU_SETINT_AP_Pin GPIO_PIN_5
#define MCU_SETINT_AP_GPIO_Port GPIOB
#define MCU_ILL_DETECT_Pin GPIO_PIN_6
#define MCU_ILL_DETECT_GPIO_Port GPIOB
#define MCU_RGEAR_DETECT_Pin GPIO_PIN_7
#define MCU_RGEAR_DETECT_GPIO_Port GPIOB
#define MCU_RX1_VCAN_Pin GPIO_PIN_8
#define MCU_RX1_VCAN_GPIO_Port GPIOB
#define MCU_TX1_VCAN_Pin GPIO_PIN_9
#define MCU_TX1_VCAN_GPIO_Port GPIOB

/* ########################## Assert Selection ############################## */
/**
  * @brief Uncomment the line below to expanse the "assert_param" macro in the 
  *        HAL drivers code
  */
/* #define USE_FULL_ASSERT    1U */

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
 extern "C" {
#endif
void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)
#ifdef __cplusplus
}
#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
