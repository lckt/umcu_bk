#ifndef _DEFINES_H_
#define _DEFINES_H_

#include <stdio.h>
#include <string.h>

#include "stm32f1xx_hal.h"



//----------------------------------------------
//  PROGRAM OPTION
//----------------------------------------------
//#define ACC_NO_CHECK
//#define VEHICLE_SWAP

typedef enum
{
    VEHICLE_NAVARA  = 0,
    VEHICLE_SMART,
    VEHICLE_VITO,
}   eVEHICLE_TYPE;

//----------------------------------------------
//  BOOT & UPDATE
//----------------------------------------------
#define BOOT_START_ADDR         0x08000000
#define APP_START_ADDR          0x08010000

#define CHAR_DLOAD_AP           0x00004150
#define CHAR_DLOAD_PC           0x00005043

#define JUMP_RETRY_MAX          3


enum
{
    RUN_APP = 0,
    RUN_JUMP_APP,
    RUN_FACTORY,
    RUN_DLOAD_AP,
    RUN_DLOAD_PC,
    RUN_MAX
};


//----------------------------------------------
//  Variable TYPE
//----------------------------------------------
typedef enum
{
    LC_FALSE = 0,
    LC_TRUE
}   LC_BOOL;

typedef void                    LC_VOID;
typedef void                    (*LC_FUNCTION)(void *);
typedef void *                  LC_PVOID;
typedef void *                  LC_HANDLE;
typedef unsigned char           LC_BYTE;
typedef unsigned short          LC_WORD;
typedef int                     LC_S32;
typedef unsigned int            LC_DWORD;
typedef unsigned int            LC_U32;



//----------------------------------------------
//  Project Definitions
//----------------------------------------------
#include "cmsis_os.h"


//----------------------------------------------
//  Structures
//----------------------------------------------
//  Task
typedef struct
{
    void (*pvTask)(void *);
    LC_BYTE *pName;
    LC_WORD wID;
    LC_WORD wStackSize;
    osPriority xPriority;
}   xTaskTable;

enum
{
    ID_COMMAP   = 0,
    ID_CANV,
    ID_PCSIM,
    ID_UPDATE,
    ID_SYSTEM,
    ID_MC,
    ID_MAX,
};


//  Event
#define EVENT_MESSAGE           (1 <<  0)
#define EVENT_JUMP_BOOT         (1 <<  2)
#define EVENT_INIT              (1 <<  3)
#define EVENT_DLOAD_RESPONSE    (1 <<  4)
#define EVENT_APBOOT            (1 <<  5)
#define EVENT_UART_RC           (1 <<  6)
#define EVENT_UART_TC           (1 <<  7)
#define EVENT_TERM_RC           (1 <<  8)
#define EVENT_TERM_TC           (1 <<  9)
#define EVENT_CAN1_RC     		(1 << 10)
#define EVENT_CAN1_TC     		(1 << 11)

#define EVQ_MAX                 (ID_MAX << 0)

#define WAIT_INFINITE           0xFFFFFFFF


//  Queue
#define QMESSAGE_HEADER_SIZE    8

typedef struct
{
    LC_BOOL bOpen;

    LC_HANDLE hLock;
  
    LC_WORD  wHead;
    LC_WORD  wTail;
  
    LC_BYTE *pBuffer;
    LC_DWORD dwSize;
}   xLC_Q;

enum
{
    UARTQ_RECV      = (EVQ_MAX),
    UARTQ_SEND,
    TERMQ_RECV,
    TERMQ_SEND,
    CAN1_RECV,
    CAN1_SEND,
    LC_Q_MAX,
};

enum
{
    LOCK_NONE,
    LOCK_TASK,
    LOCK_ISR
};


//  Shared Resource
typedef enum
{
    SHARE_QUEUE     = 0,
    SHARE_BUFFER,
    SHARE_MAX,
}   eSHARE;

typedef struct
{
    LC_BYTE Buffer[8];
}   xSharedTable;


//  Timer
#define TIMER_MAX               40

typedef struct
{
    LC_BOOL bActive;
    LC_DWORD dwID;
    LC_DWORD dwElapse;
    LC_DWORD dwCount;
}   xTimerTable;


//  Device
typedef enum
{
    DEV_UART1       = 0,
    DEV_UART4,
    DEV_FLASH,
    DEV_CAN1,
    DEV_SPI,
    DEV_ADC,
    DEV_MAX,
}   eDEV;

typedef struct
{
    LC_HANDLE (*open)(eDEV, LC_PVOID);
    LC_VOID (*close)(LC_HANDLE);
    LC_S32 (*ioctl)(LC_HANDLE, LC_DWORD, LC_PVOID, LC_DWORD, LC_PVOID, LC_DWORD);
    LC_S32 (*read)(LC_HANDLE, LC_PVOID, LC_DWORD);
    LC_S32 (*write)(LC_HANDLE, LC_PVOID, LC_DWORD);
}   xDriverTable;


enum
{
    IOCTL_READ_CANINT,
    IOCTL_ERASE_FLASH,
    IOCTL_WRITE_FLASH,
    IOCTL_PARKINFO_INIT,
    IOCTL_LOAD_PAS,
    IOCTL_LOAD_OPS,
    IOCTL_READ_BUTTON,
    IOCTL_READ_BATTERY,
    IOCTL_PRINT_FROM_AP,
    IOCTL_PRINT_TO_AP,
    IOCTL_PRINT_AMP_LCD,
    IOCTL_PRINT_CAN_RECV,
    IOCTL_PRINT_CAN_DIFF,
};


//----------------------------------------------
//  RTC START
//----------------------------------------------
#define RTC_INIT_TIME           0x5A497A00      //  2018.01.01 00:00

enum
{
    RTC_POS_INIT    = RTC_BKP_DR1,
    RTC_POS_RECV,
    RTC_POS_RECV_SIM,
    RTC_POS_SEND,
    RTC_POS_SEND_SIM,
    RTC_POS_JUMP_COUNT,
    RTC_POS_BOOT_TYPE,
    RTC_POS_BRIGHTDATA1,
    RTC_POS_BRIGHTDATA2,
};


//----------------------------------------------
//  LED
//----------------------------------------------
#define HARD_FAULT_ELAPSE       10
#define AMP_DIAG_ELAPSE         1000
#define RTCSET_ERROR_ELAPSE     2000

//----------------------------------------------
//  Console
//----------------------------------------------
//  PRINT Option
#define PRINT_IO                TRUE
#define PRINT_CAN               FALSE
#define PRINT_KEY               FALSE
#define PRINT_TMR               FALSE
#define PRINT_MC                TRUE
#define PRINT_TW8823            FALSE
#define PRINT_TW8836            FALSE
#define PRINT_DEBUG             FALSE
#define PRINT_QUEUE             FALSE
#define PRINT_BSP               FALSE
#define PRINT_VCAN              TRUE
#define PRINT_MMCAN             FALSE

typedef struct
{
    LC_BYTE *pName;
    void (*pvCmd)(LC_BYTE chArgc, LC_BYTE **ppArgs);
    LC_BYTE *pComment;
}   xConsoleCmd;



//  Uart
#define RETRY_PACKET_MAX        10

typedef struct
{
    UART_HandleTypeDef *phPort;
    LC_BYTE *pTxDma;
    LC_S32   nRecvQ;
    LC_S32   nSendQ;
}   xUartPort;

typedef struct
{
    CAN_HandleTypeDef *phPort;
    LC_S32 nRecvQ;
    LC_S32 nSendQ;
}   xCanPort;


typedef struct
{
    LC_BOOL bActive;
    LC_BYTE *pBuffer;
    LC_DWORD dwSize;
}   xInfoRetry;

typedef struct
{
    LC_DWORD dwPos;
    LC_DWORD dwSize;
    xInfoRetry xInfo[RETRY_PACKET_MAX];
}   xUartRetry;

// CAN
#define CANID_PAS               0x33D
#define CANID_OPS               0x343
#define CANID_GEAR              0x318
#define CANID_HVAC              0x320

#define CANID_SMART_1A1         0x1A1
#define CANID_SMART_PAS         0x5A8
#define CANID_SMART_GEAR        0x590
#define CANID_SMART_KEY         0x58F
#define CANID_SMART_ILL         0x559
#define CANID_SMART_HVAC        0x31B
#define CANID_SMART_DOOR        0x558

#define CANID_VITO_PAS          0x003
#define CANID_VITO_OPS          0x2EE
#define CANID_VITO_GEAR         0x10C
#define CANID_VITO_ILL          0x069
#define CANID_VITO_RMC          0x045

#define CANID_VITO_DOOR         0x2EF
#define CANID_VITO_TRUNK        0x007
#define CANID_VITO_SPEED        0x19F
#define CANID_VITO_EMERGENCY    0x04B



typedef enum{
    EOL = 0,
    MASKID32BIT,
    MASKID16BIT,
    IDLIST32BIT,
    IDLIST16BIT
}   xFilterConfigType;


typedef struct
{
  LC_WORD wFilterIdH;
  LC_WORD wFilterIdL;
  LC_WORD wFilterMaskIdH;
  LC_WORD wFilterMaskIdL;
  LC_WORD wFilterFifoAssignment;
  
  LC_BYTE chFilterNumber;
  LC_BYTE chFilterMode;
  LC_BYTE chFilterScale;

  FunctionalState xFilterActivation;
} xCanFilterInitTypeDef;

typedef struct
{
    xFilterConfigType xFilterConfig;
    LC_BYTE chBank;
    LC_BYTE chFifo;
    LC_WORD wFR1H;
    LC_WORD wFR1L;
    LC_WORD wFR2H;
    LC_WORD wFR2L;
} xFilterBankInfoType;

//  SPI
#define PARK_INFO_SIZE          6

typedef struct
{
    LC_HANDLE hLock;
    LC_DWORD dwConter;
    LC_WORD awCarInfo[PARK_INFO_SIZE];
}   xParkInfo;


//  PAS / OPS Value
//  Smart : 0x983C ~ 0x8000 ~ 0x67A4 : Step 270.. 0x98C9 ~ 0x8000 ~ 0x6945
//  VITO  : 0x0BF0 ~ 0x1000 ~ 0x1416 : Step  46.. 0x0BC7 ~ 0x1000 ~ 0x140B
enum
{
    ePAS_DIR_LEFT   = 1,
    ePAS_DIR_RIGHT,
};
#define PAS_PICTURE_MAX         47
#define PAS_PICTURE_CENTER      23

#define PAS_SMART_LIMIT_LEFT    0x9A70
#define PAS_SMART_SECTION_SIZE  288

#define PAS_VITO_LIMIT_LEFT     0x0BC7
#define PAS_VITO_SECTION_SIZE   46

enum
{
    eOPS_DIR_FAR   = 1,
    eOPS_DIR_NEAR,
};
#define OPS_VITO_OFF            2
#define OPS_VITO_MAX            9

#define OPS_FAR_MAX             6
#define OPS_OFF_VAL             0xF0
    


//----------------------------------------------
//  API Include
//----------------------------------------------
#include "LC_API.h"
#include "LC_DRV.h"
#include "LC_Lib.h"
#include "LC_Q.h"


//----------------------------------------------
#define LCD_1ST_BRIGHT          70
#define LCD_MAX_BRIGHT          100
#define LCD_MIN_BRIGHT          30
#define LCD_OFF_BRIGHT          (LCD_MIN_BRIGHT - 1)

#define FLED_MIN_BRIGHT         0
#define FLED_DAY_BRIGHT         30
#define FLED_NIGHT_BRIGHT       10


//----------------------------------------------
//  Value Definitions
//----------------------------------------------
#define LC_DEINIT               -1


//----------------------------------------------
//  ERROR Definitions
//----------------------------------------------
#define LCERR_NOT_INITIALIZED   -1
#define LCERR_ALREADY_OPENED    -2
#define LCERR_DRIVER_OPEN		-3
#define LCERR_DRIVER_WRONG 		-4
#define LCERR_WRITE_TIEMEOUT	-5
#define LCERR_WRITE_BUSY		-6
#define LCERR_DRIVER_READ		-7
#define LCERR_READ_TIEMEOUT		-8
#define LCERR_READ_BUSY			-9
#define LCERR_QUEUE_EMPTY       -10
#define LCERR_DATASIZE_TOO_BIG  -11
#define LCERR_LOCK_INITIALIZED  -12
#define LCERR_SET_TIMER         -13
#define LCERR_POST_MESSAGE      -14
#define LCERR_WRONG_ID          -15


//----------------------------------------------
// Commands
//----------------------------------------------
#define IDM_CLOSE               0x0000
#define IDM_JUMP_BOOT           0x0001
#define IDM_PROCESS_SUSPEND     0x0002
#define IDM_PROCESS_RESUME      0x0003
#define IDM_NOTIFY_SLEEP        0x0004
#define IDM_NOTIFY_WAKEUP       0x0005
#define IDM_SET_TIMER           0x0006
#define IDM_KILL_TIMER          0x0007
#define IDM_ON_TIMER            0x0008
#define IDM_SW_TIMER            0x0009
#define IDM_POLLING_BUTTON      0x000A
#define IDM_APBOOT              0x0010
#define IDM_APSEND              0x0011
#define IDM_DOWNLOAD_RESPONSE   0x0020

#define IDM_AMP_STATUS          0x0049
#define IDM_LCD_STATUS          0x0050
#define IDM_RCAM_STATUS         0x0054

#define IDM_UART_RC             0x0080
#define IDM_UART_TC             0x0081
#define IDM_TERM_RC             0x0082
#define IDM_TERM_TC             0x0083
#define IDM_CAN1_RC     		0x0090
#define IDM_CAN1_TC     		0x0091

#define IDM_ACC_ON              0x00D0
#define IDM_ACC_OFF             0x00D1
#define IDM_ACC_TIMEOUT         0x00D2
#define IDM_CANDATA_RECEIVED    0x00D3
#define IDM_CANDATA_TIMEOUT     0x00D4
#define IDM_CANDATA_CLOSED      0x00D5
#define IDM_OFFREADY            0x00DA
#define IDM_REQ_OS_TIME         0x00DB
#define IDM_REQOFF              0x00DC
#define IDM_REQOFF_TIMEOUT      0x00DE
#define IDM_POWEROFF            0x00DE
#define IDM_POWEROFF_FAIL       0x00DF

#define IDM_EVENT_RCAM_ON       0x0100
#define IDM_EVENT_RCAM_OFF      0x0101
#define IDM_RGEAR_ON            0x0102
#define IDM_RGEAR_OFF           0x0103
#define IDM_PARKINFO_INIT       0x0104
#define IDM_LOAD_PAS            0x0105
#define IDM_LOAD_OPS            0x0106
#define IDM_STEP_BUZZER         0x0108

#define IDM_ILL                 0x0120

#define IDM_USER_BTNH           0x0200
#define IDM_USER_BTNL           0x0201
#define IDM_SEND_BUTTON         0x0202
#define IDM_SEND_BUTTON_VITO    0x0203

#define IDM_AMP_ON              0x0300
#define IDM_AMP_OFF             0x0301
#define IDM_AMP_SET             0x0302
#define IDM_DIAG_ON             0x0310
#define IDM_DIAG_OFF            0x0312

#define IDM_LCD_ON              0x0320
#define IDM_LCD_OFF             0x0322
#define IDM_LCD_CUTOFF          0x0323
#define IDM_LCD_BRIGHTNESS      0x0324
#define IDM_FLED_BRIGHTNESS     0x0334
#define IDM_FLED_DIM            0x0335

#define IDM_APSET_MUTE          0x0380

#define IDM_WDT_ELAPSE          0x0400
#define IDM_WDT_CLEAR           0x0401

#define IDM_TEST_SEND_MESSAGE   0x0F00
#define IDM_TEST_SWTIMER        0x0F01
#define IDM_TEST_PAS_AUTOMOVE   0x0F02
#define IDM_TEST_PAS_MANUALMOVE 0x0F03
#define IDM_TEST_OPS_AUTOMOVE   0x0F04
#define IDM_TEST_OPS_MANUALMOVE 0x0F05
#define IDM_TEST_BUZZER_MUTE    0x0F06
#define IDM_TEST_ADC            0x0F07

#define IDM_UART_IST            0x1000
#define IDM_UART_RECV_NOTIFY    0x1010
#define IDM_UART_SEND_NOTIFY    0x1011
#define IDM_UART_RFIFO_NOTIFY   0x1012
#define IDM_UART_SEND_RETRY     0x1020
#define IDM_UART_SEND_SIM       0x1030
#define IDM_TERM_RECV_NOTIFY    0x1050
#define IDM_TERM_SEND_NOTIFY    0x1051
#define IDM_TERM_RFIFO_NOTIFY   0x1052

#define IDM_CAN_IST             0x1100
#define IDM_CAN1_DIFF_NOTIFY    0x1110
#define IDM_CAN1_TEST           0x111F
#define IDM_UART2CAN_BYTE       0x1120
#define IDM_UART2CAN_BITS       0x1121
#define IDM_CAN2_DIFF_NOTIFY    0x1122
#define IDM_CAN2_TEST           0x112F
#define IDM_CAN_INFO            0x1130
#define IDM_CAN_KEY             0x1131
#define IDM_CAN_GEAR            0x1132
#define IDM_CAN_BRAKE           0x1133
#define IDM_CAN_ILL             0x1134
#define IDM_CAN_HVAC            0x1135
#define IDM_CAN_DOOR_OPEN       0x1136

#define IDM_ERASE_FLASH         0x1200
#define IDM_ERASE_RESPONSE      0x1201
#define IDM_WRITE_FLASH         0x1202
#define IDM_WRITE_RESPONSE      0x1203

#define IDM_ERASE_FLASH_PCSIM   0x1210
#define IDM_WRITE_FLASH_PCSIM   0x1212


//----------------------------------------------
//  UART Character
//----------------------------------------------
#define FMT_BASIC               0x00
#define FMT_CONTROL             0x01
#define FMT_CARINFO             0x02

#define COMM_ACK                0x00
#define COMM_EOF                0x7E
#define COMM_IDF                0x7D

#define ACK_REQUEST             0x80

#define CMD_PRINTF              0x20

#define CMD_OFFREADY            0xDC
#define CMD_REQOFF              0xDD
#define CMD_POWEROFF			0xDE
#define CMD_AP_REBOOT			0xAD
#define CMD_NOP                 0x00
#define CMD_DATA_REQUEST        0x01

#define CMD_AP_BOOT_COMPLETE    0x02
#define CMD_MCU_VERSION         0x03
#define CMD_MCU_TIME            0x04
#define CMD_OS_TIME             0x05
#define CMD_REQ_OS_TIME         0x06
#define CMD_AMP_MUTE            0x08
#define CMD_AMP_STATUS          0x09
#define CMD_WDT_ELAPSE			0x0C
#define CMD_WDT_CLEAR			0x0D
#define CMD_LCD_ONOFF			0x0E
#define CMD_LCD_BRIGHTNESS      0x0F
#define CMD_LCD_STATUS          0x10
#define CMD_LED_BRIGHTNESS      0x11
#define CMD_RCAM_STATUS         0x14
#define CMD_DLOAD_REQUEST		0x20
#define CMD_DLOAD_RESPONSE		0x21
#define CMD_DLOAD_DATA_SEND		0x22
#define CMD_DLOAD_DATA_RESPONSE 0x23

#define CMD_TEST_SYNC           0xF0
#define CMD_TEST_VEHICLE_TYPE   0xF1
#define CMD_TEST_PRINT_FROM_AP  0xF4
#define CMD_TEST_PRINT_TO_AP    0xF5
#define CMD_TEST_PRINT_AMP_LCD  0xF6
#define CMD_TEST_PRINT_ADC		0xF7
#define CMD_TEST_PRINT_CAN_RECV	0xF8
#define CMD_TEST_PRINT_CAN_DIFF	0xF9

#define CMD_CAR_STATUS			0x02
#define CMD_GEAR_POSITION		0x03
#define CMD_BRAKE				0x04
#define CMD_ILLUMINATION		0x05
#define CMD_HVAC				0x06
#define CMD_HANDLE_REMOCON		0x07
#define CMD_HANDLE_REMOCON_VITO 0x08
#define CMD_DOOR_OPEN           0x09

#define CMD_CANDATA_TIMEOUT     0xDE

#define CMD_TEST_RGEAR          0xF0
#define CMD_TEST_PAS_AUTOMOVE	0xF1
#define CMD_TEST_PAS_MANUALMOVE	0xF2
#define CMD_TEST_OPS_AUTOMOVE	0xF3
#define CMD_TEST_OPS_MANUALMOVE	0xF4
#define CMD_TEST_BUZZER_MUTE	0xF5
#endif
