#ifndef _LC_DRV_H_
#define _LC_DRV_H_

#include "defines.h"

extern LC_HANDLE LCDRV_Open(eDEV eDevice, LC_PVOID pvProperty);
extern LC_VOID LCDRV_Close(LC_HANDLE pvHandle);
extern LC_S32  LCDRV_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData);
extern LC_S32  LCDRV_Read(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData);
extern LC_S32  LCDRV_Write(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData);

#endif
