#ifndef _DEV_ADC_H_
#define _DEV_ADC_H_

#include "defines.h"

extern LC_HANDLE ADC_Open  (eDEV eDevice, LC_PVOID pvProperty);
extern LC_VOID   ADC_Close (LC_HANDLE pvHandle);
extern LC_S32    ADC_Ioctl (LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData);
#endif