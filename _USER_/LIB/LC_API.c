//----------------------------------------------
//	Headers
//----------------------------------------------
#include "LC_API.h"


//----------------------------------------------
//	Defines
//----------------------------------------------


//----------------------------------------------
//  Variables
//----------------------------------------------
extern LC_HANDLE *g_phEvent;
extern LC_HANDLE *g_phQueue;

extern LC_DWORD g_dwEvent;

extern ADC_HandleTypeDef hadc2;
extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern RTC_HandleTypeDef hrtc;
extern IWDG_HandleTypeDef hiwdg;

static eVEHICLE_TYPE m_eVehicleType;

static LC_BOOL m_bCanAcc = LC_TRUE;

static LC_S32 m_nDebugLedElapse;
static LC_S32 m_nErrorLedElapse;


//----------------------------------------------
//	Pre-Defined Functions
//----------------------------------------------
static LC_BOOL WaitRtcOperationOff(LC_WORD *pdwCRL);


//----------------------------------------------
//	Functions
//----------------------------------------------
//-------------- Task Create
LC_S32 LCAPI_CreateTask(LC_FUNCTION pvTaskFunc, LC_BYTE *pName, LC_PVOID pvStack, LC_WORD wStackSize, LC_PVOID pvArg, LC_DWORD dwPriority, LC_HANDLE pvTaskHandle)
{
    return (int)xTaskCreate(pvTaskFunc, (const char *)pName, wStackSize, pvArg, dwPriority, pvTaskHandle);
}

LC_HANDLE LCAPI_GetTaskHandle(LC_DWORD dwID)
{
    return osThreadGetId();
}

//-------------- Delay
LC_VOID LCAPI_Delaymsec(LC_DWORD dwDelay)
{
    vTaskDelay((TickType_t)dwDelay * (osKernelSysTickFrequency / 1000));
}

//-------------- Context Switch
LC_VOID LCAPI_ContextSW()
{
    taskYIELD();
}


//----------------------------------------------
//-------------- Create Event Handler
LC_HANDLE LCAPI_CreateEvent()
{
    return xEventGroupCreate();
}

//-------------- Wait until Event Receive
LC_U32 LCAPI_WaitEvent(void *pvEventHandle, LC_DWORD dwEventBits, LC_DWORD dwTimeout)
{
    return (LC_U32)xEventGroupWaitBits(pvEventHandle, dwEventBits, LC_TRUE, LC_FALSE, dwTimeout);
}

//-------------- Event Get
LC_U32 LCAPI_GetEvent(LC_DWORD dwTo)
{
    LC_DWORD dwEvent;
    dwEvent = (LC_U32)xEventGroupGetBits(*(g_phEvent + dwTo));
    if(dwEvent != 0)
        xEventGroupClearBits(*(g_phEvent + dwTo), dwEvent);
    return dwEvent;
}


//-------------- Event Set
LC_U32 LCAPI_PostEvent(LC_DWORD dwTo, LC_DWORD dwEvent)
{
    LC_U32 dwReturn;
    
    dwReturn = xEventGroupSetBits(*(g_phEvent + dwTo), dwEvent);
    if(dwReturn == 0)
        return dwEvent;
    else
        return dwReturn;
}

//-------------- Event + Queue Send
LC_S32 LCAPI_PostMessage(LC_DWORD dwTo, LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_S32 nReturn;
    
    nReturn = LCAPI_PutQMessage((LC_DWORD)*(g_phQueue + dwTo), dwCmd, (LC_BYTE *)pData, dwSize);
        
    if(nReturn >= 0)
    {
        nReturn = xEventGroupSetBits(*(g_phEvent + dwTo), EVENT_MESSAGE);
        if(nReturn == 0)
            return EVENT_MESSAGE;
    }

    return nReturn;
}

//-------------- Event ISR Get
LC_U32 LCAPI_GetEventISR()
{
    LC_U32 dwEvent = 0;
    
    if(g_dwEvent != 0)
    {
        __istate_t s = __get_interrupt_state();
        __disable_interrupt();
        
        dwEvent = g_dwEvent;
        g_dwEvent = 0;
        
        __set_interrupt_state(s);
    }
    
    return dwEvent;
}

//-------------- Event ISR Set
LC_U32 LCAPI_PostEventISR(LC_DWORD dwEvent)
{
    __istate_t s = __get_interrupt_state();
    __disable_interrupt();
    
    g_dwEvent = g_dwEvent | dwEvent;
    
    __set_interrupt_state(s);
    
    return g_dwEvent;
}

//----------------------------------------------
//-------------- Create Shared Resource
LC_HANDLE LCAPI_CreateLock(eSHARE eResource, LC_DWORD dwArg)
{
    return NULL;
}


//-------------- Lock
LC_S32 LCAPI_LockSharedResource(LC_HANDLE hLock, LC_DWORD dwLockState)
{
    return 1;
}


//-------------- Unlock
LC_S32 LCAPI_UnlockSharedResource(LC_HANDLE hLock, LC_DWORD dwLockState)
{
    switch(dwLockState)
    {
    case LOCK_TASK:
        break;
    case LOCK_ISR:
        break;
    }
    
    return 1;
}


//----------------------------------------------
LC_S32 LCAPI_SetTimer(LC_WORD wTaskID, LC_WORD wTID, LC_DWORD dwElapse)
{
    LC_DWORD adwBuffer[2];

    adwBuffer[0] = (wTaskID << 16) | (wTID & 0xFFFF);
    adwBuffer[1] = dwElapse;

    return LCAPI_PostMessage(ID_MC, IDM_SET_TIMER, (LC_PVOID)adwBuffer, sizeof(LC_DWORD) * 2);
}

LC_S32 LCAPI_KillTimer(LC_WORD wTaskID, LC_WORD wTID)
{
    LC_DWORD adwBuffer[1];

    adwBuffer[0] = (wTaskID << 16) | (wTID & 0xFFFF);
    
    return LCAPI_PostMessage(ID_MC, IDM_KILL_TIMER, (LC_PVOID)adwBuffer, sizeof(LC_DWORD) * 1);
}



//----------------------------------------------
//  HW Device APIs
//----------------------------------------------
//  Tick
LC_U32 LCAPI_GetSysTick()
{
    LC_DWORD dwTime;
	
	dwTime = HAL_GetTick();

	return dwTime;
}

//  RTC
//  stm32f1xx_hal_rtc.c : static uint32_t RTC_ReadTimeCounter(RTC_HandleTypeDef* hrtc)
LC_DWORD LCAPI_GetRtcTime()
{
    LC_DWORD dwTime;
    LC_DWORD dwHigh1, dwHigh2, dwLow;
    
    dwHigh1 = hrtc.Instance->CNTH & 0xFFFF;
    dwLow   = hrtc.Instance->CNTL & 0xFFFF;
    dwHigh2 = hrtc.Instance->CNTH & 0xFFFF;
    
    if(dwHigh1 != dwHigh2)
    {
        dwTime = (dwHigh2 << 16) | (hrtc.Instance->CNTL & 0xFFFF);
    }
    else
    {
        dwTime = (dwHigh1 << 16) | dwLow;
    }
    
    if(dwTime < RTC_INIT_TIME)
    {
        dwTime = RTC_INIT_TIME;
        LCAPI_SetRtcTime(dwTime);
    }
    
    return dwTime;
}

//  stm32f1xx_hal_rtc.c : static HAL_StatusTypeDef RTC_WriteTimeCounter(RTC_HandleTypeDef* hrtc, uint32_t TimeCounter)
LC_BOOL LCAPI_SetRtcTime(LC_DWORD dwTime)
{
    LC_WORD wCRL;
    
    if(WaitRtcOperationOff(&wCRL) == LC_FALSE)
        return LC_FALSE;

    hrtc.Instance->CRL = wCRL | RTC_CRL_CNF;

    hrtc.Instance->CNTH = dwTime >> 16; 
    hrtc.Instance->CNTL = dwTime;
    
    hrtc.Instance->CRL = wCRL & (~RTC_CRL_CNF);
    
    return WaitRtcOperationOff(&wCRL);
}

static LC_BOOL WaitRtcOperationOff(LC_WORD *pwCRL)
{
    int i = 0;

    do
    {
        if((*pwCRL = hrtc.Instance->CRL) & RTC_CRL_RTOFF)
            return LC_TRUE;
    }   while(i < 1000);

    return LC_FALSE;
}


//  Re-Init
LC_VOID LCAPI_RebootInit()
{
    //  Initial 1st
    LCAPI_PowerOnoff(LC_TRUE);

    //  LED BL Set
    LCAPI_SetLedBrightness(FLED_MIN_BRIGHT, LC_TRUE);
    //  LCD BL Set
///    LCAPI_SetLcdBrightness(LCD_MIN_BRIGHT, LC_TRUE);
    
    //  AMP
    LCAPI_SetAmpMute(LC_TRUE);
///    LCAPI_SetAmpOnoff(LC_TRUE);
    //  AP Release
///    LCAPI_ResetAP(LC_FALSE);
    //  LCD Enable
///    LCAPI_SetLcdOnoff(LC_TRUE);
    //  LCD BL Enable
///    LCAPI_SetBlOnoff(LC_TRUE);
    
    //  RCAM POWER ON
    LCAPI_SetRcamOnoff(LC_FALSE);
    
    //  RCAM SIGNAL to AP PIN : OFF
    LCAPI_SetRcamSignal(LC_FALSE);
    
    //  CAN PHY
    LCAPI_SetPhyEn(LC_TRUE);
    LCAPI_SetPhyStb(LC_TRUE);
    
    //  AP Release ?
//    LCAPI_ResetAP(LC_FALSE);
}

//  Download
LC_DWORD LCAPI_CheckDownload()
{
    LC_DWORD dwReadRtc;
    LC_DWORD dwReturn;

    dwReadRtc = (LC_DWORD)HAL_RTCEx_BKUPRead(&hrtc, RTC_POS_BOOT_TYPE);
    
    switch(dwReadRtc)
    {
    case CHAR_DLOAD_AP:
        dwReturn = RUN_DLOAD_AP;
        break;
    case CHAR_DLOAD_PC:
        dwReturn = RUN_DLOAD_PC;
        break;
    default:
        dwReturn = RUN_FACTORY;
        break;
    }

    return dwReturn;    
}

LC_VOID LCAPI_SetDownload(LC_DWORD dwDload)
{
    if(dwDload == RUN_DLOAD_AP)
        HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_BOOT_TYPE, CHAR_DLOAD_AP);
    else
        HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_BOOT_TYPE, CHAR_DLOAD_PC);
}

LC_VOID LCAPI_ClrDownload()
{
    HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_BOOT_TYPE, 0xFFFF);
}

LC_VOID LCAPI_Jump2Addr(LC_DWORD dwAddr)
{
    volatile LC_DWORD dwJumpAddr;
    void (*Jump_To_Application)(void);
    
    __disable_irq();
    
    dwJumpAddr = *(volatile LC_DWORD *)(dwAddr + 4);
//    dwJumpAddr = dwJumpAddr & ~0x03;
    Jump_To_Application = (void (*)(void))dwJumpAddr;
    
    __set_MSP(*(volatile LC_DWORD *)dwAddr);
    __set_PSP(*(volatile LC_DWORD *)dwAddr);
    __set_CONTROL(0);                           // stack mode change to SP_MAIN

    Jump_To_Application();
}


//  Jump Count
LC_DWORD LCAPI_GetJumpCount()
{
    LC_DWORD dwCount;
    dwCount = HAL_RTCEx_BKUPRead(&hrtc, RTC_POS_JUMP_COUNT);
    return dwCount;
}

LC_VOID LCAPI_SetJumpCount(LC_DWORD dwCount)
{
    HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_JUMP_COUNT, dwCount);
}


//  Transmit Link
LC_BYTE LCAPI_GetRecvLink()
{
    LC_BYTE chLink;
    
    chLink = (LC_BYTE)HAL_RTCEx_BKUPRead(&hrtc, RTC_POS_RECV);

    return chLink;
}

LC_VOID LCAPI_SetRecvLink(LC_DWORD dwLink)
{
    HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_RECV, dwLink);
}

LC_BYTE LCAPI_GetRecvLinkSim()
{
    LC_BYTE chLink;
    
    chLink = (LC_BYTE)HAL_RTCEx_BKUPRead(&hrtc, RTC_POS_RECV_SIM);

    return chLink;
}

LC_VOID LCAPI_SetRecvLinkSim(LC_DWORD dwLink)
{
    HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_RECV_SIM, dwLink);
}

//  Transmit Link
LC_BYTE LCAPI_GetSendLink()
{
    LC_BYTE chLink;
    
    chLink = (LC_BYTE)HAL_RTCEx_BKUPRead(&hrtc, RTC_POS_SEND);

    return chLink;
}

LC_VOID LCAPI_SetSendLink(LC_DWORD dwLink)
{
    HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_SEND, dwLink);
}

LC_BYTE LCAPI_GetSendLinkSim()
{
    LC_BYTE chLink;
    
    chLink = (LC_BYTE)HAL_RTCEx_BKUPRead(&hrtc, RTC_POS_SEND_SIM);

    return chLink;
}

LC_VOID LCAPI_SetSendLinkSim(LC_DWORD dwLink)
{
    HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_SEND_SIM, dwLink);
}

//  VEHICLE
eVEHICLE_TYPE LCAPI_GetVehicle()
{
    return m_eVehicleType;
}

LC_VOID LCAPI_SetVehicle(eVEHICLE_TYPE eVehicle)
{
    m_eVehicleType = eVehicle;
}

LC_VOID LCAPI_StartAdcVehicle()
{
    hadc2.Instance->SQR3 = 0x01;
    HAL_ADC_Start(&hadc2);
}

LC_VOID LCAPI_StopAdcVehicle()
{
    LC_DWORD dwValue;
    HAL_ADC_Stop(&hadc2);
    dwValue = HAL_ADC_GetValue(&hadc2);

#ifndef VEHICLE_SWAP
    if(dwValue < 0x0FF)
#else
    if(dwValue >= 0x0FF)
#endif
        m_eVehicleType = VEHICLE_SMART;
    else
        m_eVehicleType = VEHICLE_VITO;
}

//  ACC
LC_BOOL LCAPI_GetAcc()
{
    LC_BOOL bReturn;
    
    switch(m_eVehicleType)
    {
    case VEHICLE_NAVARA:
    case VEHICLE_SMART:
    case VEHICLE_VITO:
        {
#ifndef ACC_NO_CHECK            
            GPIO_PinState eState = HAL_GPIO_ReadPin(MCU_ACC_DETECT_GPIO_Port, MCU_ACC_DETECT_Pin);
            bReturn = (eState == GPIO_PIN_SET) ? LC_TRUE : LC_FALSE;
#else
            bReturn = LC_TRUE;
#endif
        }
        break;
    default:
        bReturn = m_bCanAcc;
        break;
    }
    
    return bReturn;
}

LC_VOID LCAPI_SetAcc(LC_BOOL bON)
{
    m_bCanAcc = bON;
}

//  IO ILL
LC_BOOL LCAPI_GetIoIll()
{
    GPIO_PinState eState = HAL_GPIO_ReadPin(MCU_ILL_DETECT_GPIO_Port, MCU_ILL_DETECT_Pin);
    return (eState == GPIO_PIN_SET) ? LC_TRUE : LC_FALSE;
}

//  IO RGEAR
LC_BOOL LCAPI_GetIoRgear()
{
    GPIO_PinState eState = HAL_GPIO_ReadPin(MCU_RGEAR_DETECT_GPIO_Port, MCU_RGEAR_DETECT_Pin);
    return (eState == GPIO_PIN_SET) ? LC_TRUE : LC_FALSE;
}

//  AMP
LC_VOID LCAPI_SetAmpOnoff(LC_BOOL bON)
{
    GPIO_PinState eState = (bON == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(MCU_AMP_EN_GPIO_Port, MCU_AMP_EN_Pin, eState);
}

LC_VOID LCAPI_SetAmpMute(LC_BOOL bMute)
{   
    GPIO_PinState eState = (bMute == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(MCU_AMP_MUTE_GPIO_Port, MCU_AMP_MUTE_Pin, eState);
}

LC_BOOL LCAPI_GetAmpDiag()
{
    GPIO_PinState eState = HAL_GPIO_ReadPin(MCU_AMP_DIAG_GPIO_Port, MCU_AMP_DIAG_Pin);
    //  Diag active low
    return (eState == GPIO_PIN_SET) ? LC_FALSE : LC_TRUE;
}

//  LCD Control
LC_VOID LCAPI_SetLcdOnoff(LC_BOOL bON)
{
    GPIO_PinState eState = (bON == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(MCU_LCD_PWRON_GPIO_Port, MCU_LCD_PWRON_Pin, eState);
}

LC_VOID LCAPI_SetBlOnoff(LC_BOOL bON)
{
    GPIO_PinState eState = (bON == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(MCU_BL_EN_GPIO_Port, MCU_BL_EN_Pin, eState);
}

//  Odd Parity Check
LC_DWORD LCAPI_GetLcdBrightData()
{
    int nParity;
    LC_WORD wBrightData1, wBrightData2;

    wBrightData1 = (LC_WORD)HAL_RTCEx_BKUPRead(&hrtc, RTC_POS_BRIGHTDATA1);
    nParity = 0;
    for(int i = 0; i < 16; i++)
    {
        if(wBrightData1 & (1 << i)) nParity++;
    }
    
    // if not Odd Parity
    if(!(nParity & 0x01))
        return 0;
    
    wBrightData2 = (LC_WORD)HAL_RTCEx_BKUPRead(&hrtc, RTC_POS_BRIGHTDATA2);
    nParity = 0;
    for(int i = 0; i < 16; i++)
    {
        if(wBrightData2 & (1 << i)) nParity++;
    }
    
    // if not Odd Parity
    if(!(nParity & 0x01))
        return 0;
    
    return (LC_DWORD)(wBrightData1) | (LC_DWORD)(wBrightData2 << 16);
}

LC_VOID LCAPI_SetLcdBrightData(LC_DWORD dwCurr, LC_DWORD dwNight, LC_DWORD dwDay, LC_BOOL bAuto)
{
    int nParity;
    LC_WORD wBrightData;
    
    wBrightData = (dwCurr & 0x7F) | ((dwNight & 0x7F) << 8);
    nParity = 0;
    for(int i = 0; i < 15; i++)
    {
        if(wBrightData & (1 << i)) nParity++;
    }
    
    //  Make Odd Parity
    if(!(nParity & 0x01))
    {
        wBrightData |= 0x8000;
    }
    HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_BRIGHTDATA1, wBrightData);
    
    wBrightData = (dwDay & 0x7F) | ((bAuto & 0x01) << 8);
    nParity = 0;
    for(int i = 0; i < 15; i++)
    {
        if(wBrightData & (1 << i)) nParity++;
    }
    
    //  Make Odd Parity
    if(!(nParity & 0x01))
    {
        wBrightData |= 0x8000;
    }
    HAL_RTCEx_BKUPWrite(&hrtc, RTC_POS_BRIGHTDATA2, wBrightData);
}

//  htim1.Init.Prescaler = 35;
//  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
//  htim1.Init.Period = 100;
//  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
//
//  72,000,000 / (35 + 1) =  2,000,000
//   2,000,000 / 100      =     20,000 Hz  <= Check Oscilator
LC_VOID LCAPI_SetLcdBrightness(LC_DWORD dwPercent, LC_BOOL bFirstInit)
{
    TIM_OC_InitTypeDef sConfigOC;
    
    if(bFirstInit == LC_TRUE)
    {
        HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_4);

        if(dwPercent > 0)
        {
            sConfigOC.OCMode = TIM_OCMODE_PWM1;
            sConfigOC.Pulse = dwPercent;            // (dwPercent * (100)) / 100;
            sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
            sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
            sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
            sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
         
            if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
            {
                LCAPI_Putc(0xF1);
                _Error_Handler(__FILE__, __LINE__);
            }

            HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
        }
    }
    else
    {
        sConfigOC.Pulse = dwPercent;            // (dwPercent * (100)) / 100;
        htim1.Instance->CCR4 = sConfigOC.Pulse;
    }
}

LC_VOID LCAPI_SetLedBrightness(LC_DWORD dwPercent, LC_BOOL bFirstInit)
{
    TIM_OC_InitTypeDef sConfigOC;
    
    if(bFirstInit == LC_TRUE)
    {
        HAL_TIM_PWM_Stop(&htim1, TIM_CHANNEL_1);

///        if(dwPercent > 0)
        {
            sConfigOC.OCMode = TIM_OCMODE_PWM1;
            sConfigOC.Pulse = dwPercent;
            sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
            sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
            sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
            sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
         
            if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
            {
                LCAPI_Putc(0xF2);
                _Error_Handler(__FILE__, __LINE__);
            }

            HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
        }
    }
    else
    {
        sConfigOC.Pulse = dwPercent;
        htim1.Instance->CCR1 = sConfigOC.Pulse;
    }
}

LC_VOID LCAPI_SetBuzzer(LC_BOOL bON)
{
    TIM_OC_InitTypeDef sConfigOC;
    
    HAL_TIM_PWM_Stop(&htim2, TIM_CHANNEL_1);

    sConfigOC.OCMode = TIM_OCMODE_PWM1;
    sConfigOC.Pulse = (bON == LC_TRUE) ? (htim2.Init.Period * 50 / 100) : 0;
    sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
    sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
    sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
    sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
 
    if (HAL_TIM_PWM_ConfigChannel(&htim2, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
    {
        LCAPI_Putc(0xF3);
        _Error_Handler(__FILE__, __LINE__);
    }

    HAL_TIM_PWM_Start(&htim2, TIM_CHANNEL_1);
}

LC_VOID LCAPI_SetRcamSignal(LC_BOOL bON)
{
    GPIO_PinState eState = (bON == LC_TRUE) ? GPIO_PIN_RESET : GPIO_PIN_SET;
    HAL_GPIO_WritePin(MCU_SETINT_AP_GPIO_Port, MCU_SETINT_AP_Pin, eState);
}

LC_VOID LCAPI_SetRcamOnoff(LC_BOOL bON)
{
    GPIO_PinState eState = (bON == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(MCU_RCAM_POWER_GPIO_Port, MCU_RCAM_POWER_Pin, eState);
}


//  CAN
LC_VOID LCAPI_SetPhyEn(LC_BOOL bHigh)
{
    GPIO_PinState eState = (bHigh == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(MCU_CANPHY_EN_GPIO_Port, MCU_CANPHY_EN_Pin, eState);
}

LC_VOID LCAPI_SetPhyStb(LC_BOOL bHigh)
{
    GPIO_PinState eState = (bHigh == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(MCU_CANPHY_STB_GPIO_Port, MCU_CANPHY_STB_Pin, eState);
}

LC_BOOL LCAPI_GetPhyError()
{
    GPIO_PinState eState = HAL_GPIO_ReadPin(MCU_CANPHY_ERR_GPIO_Port, MCU_CANPHY_ERR_Pin);
    return (eState == GPIO_PIN_SET) ? LC_TRUE : LC_FALSE;
}


//  LED
LC_VOID LCAPI_SetDebugLed(LC_S32 nSetElapse)
{
    m_nDebugLedElapse = nSetElapse;
    if(nSetElapse == 0)
    {
        HAL_GPIO_WritePin(MCU_LED2_GPIO_Port, MCU_LED2_Pin, GPIO_PIN_RESET);
    }
}

LC_VOID LCAPI_RunDebugLed()
{
    static LC_S32 nCount = 0;
    static LC_BOOL bToggle = LC_FALSE;
    
    if(m_nDebugLedElapse > 0)
    {
        if(++nCount >= m_nDebugLedElapse)
        {
            nCount = 0;
            bToggle ^= 1;
            
            HAL_GPIO_WritePin(MCU_LED2_GPIO_Port, MCU_LED2_Pin, (bToggle == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET);
        }
    }
}

LC_VOID LCAPI_SetErrorLed(LC_S32 nSetElapse)
{
    //  Set Error at No-Error State | More Important Error
    if(m_nErrorLedElapse == 0 || nSetElapse < m_nErrorLedElapse)
    {
        m_nErrorLedElapse = nSetElapse;
    }
    if(nSetElapse == 0)
    {
        HAL_GPIO_WritePin(MCU_LED1_GPIO_Port, MCU_LED1_Pin, GPIO_PIN_RESET);
    }
}

LC_VOID LCAPI_RunErrorLed()
{
    static LC_S32 nCount = 0;
    static LC_BOOL bToggle = LC_FALSE;
    
    if(m_nErrorLedElapse > 0)
    {
        if(++nCount >= m_nErrorLedElapse)
        {
            nCount = 0;
            bToggle ^= 1;
            
            HAL_GPIO_WritePin(MCU_LED1_GPIO_Port, MCU_LED1_Pin, (bToggle == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET);
        }
    }
}


//  POWER & RESET
LC_VOID LCAPI_PowerOnoff(LC_BOOL bON)
{
    GPIO_PinState ePinState = (bON == LC_TRUE) ? GPIO_PIN_SET : GPIO_PIN_RESET;
    HAL_GPIO_WritePin(MCU_SYS_POWER_GPIO_Port, MCU_SYS_POWER_Pin, ePinState);
}

LC_VOID LCAPI_ResetAP(LC_BOOL bON)
{
    GPIO_PinState ePinState = (bON == LC_TRUE) ? GPIO_PIN_RESET : GPIO_PIN_SET;
    HAL_GPIO_WritePin(MCU_AP_NRESET_GPIO_Port, MCU_AP_NRESET_Pin, ePinState);
}

LC_VOID LCAPI_ResetMcu(LC_DWORD dwElapse)
{
    if(dwElapse > 0)
    {
        LCAPI_Delaymsec(dwElapse * 1000);
    }
    
    HAL_NVIC_SystemReset();
}

//  0.025(40khz) x 256 = 6.4
//  dwSec = (dwReload X 6.4) / 1000
//  dwReload = (dwSec X 1000) / 6.4 = (dwSec X 10000) / 64
LC_VOID LCAPI_InitWdt(LC_DWORD dwSec)
{
    LC_DWORD dwReload = (dwSec * 10000) >> 6;
    
    hiwdg.Init.Reload = dwReload;

    if (HAL_IWDG_Init(&hiwdg) != HAL_OK)
    {
        LCAPI_Putc(0xF4);
        _Error_Handler(__FILE__, __LINE__);
    }
}

LC_VOID LCAPI_ClrWdt()
{
    HAL_IWDG_Refresh(&hiwdg);
}

//
LC_VOID LCAPI_SetStop()
{
    HAL_SuspendTick();
	CLEAR_BIT(SysTick->CTRL,SysTick_CTRL_ENABLE_Msk);

///	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFE);
  	HAL_PWR_EnterSTOPMode(PWR_LOWPOWERREGULATOR_ON, PWR_STOPENTRY_WFI);

	SET_BIT(SysTick->CTRL,SysTick_CTRL_ENABLE_Msk);
    HAL_ResumeTick();
}


//  HAL_Delay
#ifdef STM32F105xC
void HAL_Delay(__IO uint32_t Delay)
{
    int i;
    for(i = 0; i < (int)(Delay * 1000); i++)
    {
        ;
    }
}
#endif