#ifndef _CTRL_FLASH_H_
#define _CTRL_FLASH_H_

#include "defines.h"

extern LC_HANDLE ROM_Open  (eDEV eDevice, LC_PVOID pvProperty);
extern LC_VOID   ROM_Close (LC_HANDLE pvHandle);
extern LC_S32    ROM_Ioctl (LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData);

#endif