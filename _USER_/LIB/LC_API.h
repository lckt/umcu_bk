#ifndef _LC_API_H_
#define _LC_API_H_

#include "defines.h"


//  Task
extern LC_S32  LCAPI_CreateTask(LC_FUNCTION pvTaskFunc, LC_BYTE *pName, LC_PVOID pvStack, LC_WORD wStackSize, LC_PVOID pvArg, LC_DWORD dwPriority, LC_HANDLE pvTaskHandle);
extern LC_HANDLE LCAPI_GetTaskHandle(LC_DWORD dwID);
extern LC_VOID LCAPI_Delaymsec(LC_DWORD dwDelay);
extern LC_VOID LCAPI_ContextSW();

//  Event
extern LC_HANDLE LCAPI_CreateEvent();

extern LC_U32 LCAPI_WaitEvent(LC_HANDLE pvEventHandle, LC_DWORD dwEventBits, LC_DWORD dwTimeout);
extern LC_U32 LCAPI_GetEvent(LC_DWORD dwTo);
extern LC_U32 LCAPI_PostEvent(LC_DWORD dwTo, LC_DWORD dwEvent);
extern LC_S32 LCAPI_PostMessage(LC_DWORD dwTo, LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);

extern LC_U32 LCAPI_GetEventISR();
extern LC_U32 LCAPI_PostEventISR(LC_DWORD dwEvent);

//  Semaphore
extern LC_HANDLE LCAPI_CreateLock(eSHARE eResource, LC_DWORD dwArg);
extern LC_VOID LCAPI_DestroyLock();
extern LC_S32    LCAPI_Lock(LC_HANDLE hLock, LC_DWORD dwLockState);
extern LC_S32    LCAPI_Unlock(LC_HANDLE hLock, LC_DWORD dwLockState);

//  SW Timer
extern LC_S32 LCAPI_SetTimer(LC_WORD wTaskID, LC_WORD wTID, LC_DWORD dwElapse);
extern LC_S32 LCAPI_KillTimer(LC_WORD wTaskID, LC_WORD wTID);


//  HW Device APIs
extern LC_U32 LCAPI_GetSysTick(void);

extern LC_DWORD LCAPI_GetRtcTime();
extern LC_BOOL LCAPI_SetRtcTime(LC_DWORD dwTime);

extern LC_VOID LCAPI_RebootInit();

extern LC_VOID LCAPI_SetDownload();
extern LC_VOID LCAPI_ClrDownload();
extern LC_DWORD LCAPI_CheckDownload();
extern LC_VOID LCAPI_Jump2Addr(LC_DWORD dwAddr);

extern LC_DWORD LCAPI_GetJumpCount();
extern LC_VOID LCAPI_SetJumpCount(LC_DWORD dwCount);

extern LC_BYTE LCAPI_GetRecvLink();
extern LC_VOID LCAPI_SetRecvLink(LC_DWORD dwLink);
extern LC_BYTE LCAPI_GetRecvLinkSim();
extern LC_VOID LCAPI_SetRecvLinkSim(LC_DWORD dwLink);

extern LC_BYTE LCAPI_GetSendLink();
extern LC_VOID LCAPI_SetSendLink(LC_DWORD dwLink);
extern LC_BYTE LCAPI_GetSendLinkSim();
extern LC_VOID LCAPI_SetSendLinkSim(LC_DWORD dwLink);

extern eVEHICLE_TYPE LCAPI_GetVehicle();
extern LC_VOID LCAPI_SetVehicle(eVEHICLE_TYPE eVehicle);
extern LC_VOID LCAPI_StartAdcVehicle();
extern LC_VOID LCAPI_StopAdcVehicle();

extern LC_BOOL LCAPI_GetAcc();
extern LC_VOID LCAPI_SetAcc(LC_BOOL bON);

extern LC_BOOL LCAPI_GetIoIll();
extern LC_BOOL LCAPI_GetIoRgear();

extern LC_VOID LCAPI_SetAmpOnoff(LC_BOOL bON);
extern LC_VOID LCAPI_SetAmpMute(LC_BOOL bMute);
extern LC_BOOL LCAPI_GetAmpDiag();

extern LC_VOID LCAPI_SetLcdOnoff(LC_BOOL bON);
extern LC_VOID LCAPI_SetBlOnoff(LC_BOOL bON);
extern LC_DWORD LCAPI_GetLcdBrightData();        // Odd Parity
extern LC_VOID LCAPI_SetLcdBrightData(LC_DWORD dwCurr, LC_DWORD dwNight, LC_DWORD dwDay, LC_BOOL bAuto);
extern LC_VOID LCAPI_SetLcdBrightness(LC_DWORD dwPercent, LC_BOOL bFirstInit);
extern LC_VOID LCAPI_SetLedBrightness(LC_DWORD dwPercent, LC_BOOL bFirstInit);
extern LC_VOID LCAPI_SetBuzzer(LC_BOOL bON);
extern LC_VOID LCAPI_SetRcamSignal(LC_BOOL bON);
extern LC_VOID LCAPI_SetRcamOnoff(LC_BOOL bON);

extern LC_VOID LCAPI_SetPhyStb(LC_BOOL bHigh);
extern LC_VOID LCAPI_SetPhyEn(LC_BOOL bHigh);
extern LC_BOOL LCAPI_GetPhyError();

extern LC_VOID LCAPI_SetDebugLed(LC_S32 nSetElapse);
extern LC_VOID LCAPI_RunDebugLed();
extern LC_VOID LCAPI_SetErrorLed(LC_S32 nSetElapse);
extern LC_VOID LCAPI_RunErrorLed();

extern LC_VOID LCAPI_PowerOnoff(LC_BOOL bON);
extern LC_VOID LCAPI_ResetAP(LC_BOOL bON);
extern LC_VOID LCAPI_ResetMcu(LC_DWORD dwElapse);
extern LC_VOID LCAPI_InitWdt(LC_DWORD dwSec);
extern LC_VOID LCAPI_ClrWdt();
extern LC_VOID LCAPI_SetStop();

extern LC_VOID LCAPI_Putc(LC_BYTE chEcho);
extern LC_VOID LCAPI_Printf(char *pFormat, ...);

extern LC_VOID LCAPI_WaitTXE_Putc(LC_BYTE chEcho);
extern LC_VOID LCAPI_toAP_Putc(LC_BYTE chEcho);
extern LC_VOID LCAPI_AmpLcd_Putc(LC_BYTE chEcho);
extern LC_VOID LCAPI_CanRecv_Putc(LC_BYTE chEcho);
extern LC_VOID LCAPI_CanDiff_Putc(LC_BYTE chEcho);
#endif