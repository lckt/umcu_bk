//----------------------------------------------
//	Headers
//----------------------------------------------
#include "LC_Lib.h"


//----------------------------------------------
//	Defines
//----------------------------------------------
#ifdef APP_PROGRAM
const LC_BYTE m_cVer[16]   @   0x0801FFE0  = "00.74\0";
const LC_BYTE m_cUid[16]   @   0x0801FFF0  = "0123456789ABCDEF";

LC_BYTE *m_pcVer = (LC_BYTE *)m_cVer;
LC_BYTE *m_pcUid = (LC_BYTE *)m_cUid;

#else
const LC_BYTE *m_pcVer = (const LC_BYTE *)0x0801FFE0;
const LC_BYTE *m_pcUid = (const LC_BYTE *)0x0801FFF0;
#endif


//----------------------------------------------
//	Pre-Defined Variables
//----------------------------------------------
static LC_DWORD m_dwBootType;

static LC_WORD m_wAmpStatus;
static LC_WORD m_wLcdStatus;
static LC_WORD m_wRcamStatus;


//----------------------------------------------
//	Pre-Defined Functions
//----------------------------------------------



//----------------------------------------------
//	Functions
//----------------------------------------------
LC_DWORD LCLIB_ParseSrec(LC_BYTE *pDst, LC_DWORD *pdwDst, LC_BYTE *pSrc, LC_DWORD dwSrc)
{
    int i, j;
    
    //  Source Size too short
    if(dwSrc < 14)
        return 0xFF;
    
    //  1st Byte must 'S'
    if(pSrc[0] != 'S')
        return 0xFE;
    
    //  2nd Byte is '0' or '3' or '7'
    if(!(pSrc[1] == '0' || pSrc[1] == '3' || pSrc[1] == '7'))
        return 0xFD;
    
    //  Ascii '0'~'9', 'A'~'Z', 'a'~'z'
    for(i = 2; i < dwSrc; i++)
    {
        if(!((pSrc[i] >= '0' && pSrc[i] <= '9') || (pSrc[i] >= 'A' && pSrc[i] < 'Z') || (pSrc[i] >= 'a' && pSrc[i] <= 'z')))
            return 0xFC;
    }
    
    //  Make Data
    for(i = 2, j = 0; i < dwSrc; i+=2, j++)
    {
        pDst[j] = LCLIB_Ascii2Hex(pSrc[i], pSrc[i + 1]);
    }
    *pdwDst = pDst[0];

    LC_BYTE chSum = 0;
    for(i = 0; i < *pdwDst; i++)
    {
        chSum += pDst[i];
    }
    // Checksum error
    if((~chSum & 0xFF) != pDst[i])
        return 0xFB;
    
    return (pSrc[1] & 0x0F);
}

LC_BYTE LCLIB_Ascii2Hex(LC_BYTE chH, LC_BYTE chL)
{
    if(chH >= 'A') chH -= 7;    chH &= 0x0F;
    if(chL >= 'A') chL -= 7;    chL &= 0x0F;
    
    return ((chH << 4) | chL);
}

LC_BOOL LCLIB_CheckUniqueId()
{
    if(m_pcUid[0x0] == '0' && m_pcUid[0x1] == '1' && m_pcUid[0x2] == '2' && m_pcUid[0x3] == '3'
    && m_pcUid[0x4] == '4' && m_pcUid[0x5] == '5' && m_pcUid[0x6] == '6' && m_pcUid[0x7] == '7'
    && m_pcUid[0x8] == '8' && m_pcUid[0x9] == '9' && m_pcUid[0xA] == 'A' && m_pcUid[0xB] == 'B'
    && m_pcUid[0xC] == 'C' && m_pcUid[0xD] == 'D' && m_pcUid[0xE] == 'E' && m_pcUid[0xF] == 'F')
        return LC_TRUE;
    else
        return LC_FALSE;
}

LC_DWORD LCLIB_ReadVersion(LC_BYTE *pBuffer)
{
    int i;
    LC_BYTE Buffer[8];
    
    for(i = 0; i < 8 && m_pcVer[i] != 0; i++) Buffer[i] = m_pcVer[i];
    if(!(i == 5 && Buffer[2] == '.')) return 0;
    
    pBuffer[0] = ((Buffer[0] & 0x0F) * 10) + (Buffer[1] & 0x0F);
    pBuffer[1] = ((Buffer[3] & 0x0F) * 10) + (Buffer[4] & 0x0F);
    
#ifndef APP_PROGRAM
    pBuffer[1] -= 1;
#endif
    
    return 2;
}


LC_DWORD LCLIB_GetBootType()
{
    return m_dwBootType;
}

LC_VOID LCLIB_SetBootType(LC_DWORD dwBootType)
{
    m_dwBootType = dwBootType;
}

LC_WORD LCLIB_GetAmpStatus()
{
    return m_wAmpStatus;
}

LC_VOID LCLIB_SetAmpStatus(LC_WORD wStatus)
{
    m_wAmpStatus = wStatus;
}

LC_WORD LCLIB_GetLcdStatus()
{
    return m_wLcdStatus;
}

LC_VOID LCLIB_SetLcdStatus(LC_WORD wStatus)
{
    m_wLcdStatus = wStatus;
}

LC_WORD LCLIB_GetRcamStatus()
{
    return m_wRcamStatus;
}

LC_VOID LCLIB_SetRcamStatus(LC_WORD wStatus)
{
    m_wRcamStatus = wStatus;
}

