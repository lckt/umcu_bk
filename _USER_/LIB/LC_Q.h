#ifndef _LC_QUEUE_H_
#define _LC_QUEUE_H_

#include "defines.h"

//----------------------------------------------
//	Defines
//----------------------------------------------


//----------------------------------------------
//	Pre-Defined External Functions
//----------------------------------------------
extern void LCAPI_InitQ();
extern void LCAPI_DeinitQ();

extern int  LCAPI_CreateQ(xLC_Q *pQ);
extern void LCAPI_DeleteQ(LC_DWORD dwQ);

extern int  LCAPI_PutQMessage(LC_DWORD dwQ, LC_DWORD dwCmd, LC_BYTE *pBuffer, LC_DWORD dwSize);
extern int  LCAPI_GetQMessage(LC_DWORD dwQ, LC_DWORD *dwCmd, LC_BYTE *pBuffer, LC_DWORD dwSize);

extern int  LCAPI_PutQBuffer(LC_DWORD dwQ, LC_BYTE *pBuffer, LC_DWORD dwSize);
extern int  LCAPI_GetQBuffer(LC_DWORD dwQ, LC_BYTE *pBuffer, LC_DWORD dwSize, LC_BOOL bPeek);

extern int  LCAPI_GetQLength(LC_DWORD dwQ);
extern int  LCAPI_GetQAvailableSpace(LC_DWORD dwQ);

extern void LCAPI_EmptyQ(LC_DWORD dwQ);
extern int  LCAPI_IsEmptyQ(LC_DWORD dwQ);

#endif