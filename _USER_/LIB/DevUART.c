//----------------------------------------------
//  Headers
//----------------------------------------------
#include "DevUART.h"


//----------------------------------------------
//  Definitions
//----------------------------------------------
#define SEND_TERM_SIZE          128
#define RECV_TERM_SIZE          128

#define RECV_UART_SIZE          (520 + 12)      // (255 + 5) x 2 + 2(STX+ETX) + 8(Pos) + 2
#define SEND_UART_SIZE          (520 + 12)

enum
{
    eUART_1         = 0,
    eUART_4,
    eUART_MAX,
};


//----------------------------------------------
//  Variables
//----------------------------------------------
extern UART_HandleTypeDef huart1;
extern UART_HandleTypeDef huart4;

static LC_BOOL m_bPrintFromAP;
static LC_BOOL m_bPrintToAP;
static LC_BOOL m_bPrintAmpLcd;
static LC_BOOL m_bPrintCanRecv;
static LC_BOOL m_bPrintCanDiff;

static xUartPort m_xPort[eUART_MAX];

static LC_BYTE m_TdmaTERM[SEND_TERM_SIZE];

static LC_BYTE m_RecvTERM[RECV_TERM_SIZE];
static LC_BYTE m_SendTERM[SEND_TERM_SIZE];
static const xLC_Q xQueueTERM[2] = 
{
    {   .wHead = 0, .wTail = 0, .pBuffer = m_RecvTERM,    .dwSize = sizeof(m_RecvTERM)    },
    {   .wHead = 0, .wTail = 0, .pBuffer = m_SendTERM,    .dwSize = sizeof(m_SendTERM)    },
};

static LC_BYTE m_TdmaUART[SEND_UART_SIZE];

static LC_BYTE m_RecvUART[RECV_UART_SIZE];
static LC_BYTE m_SendUART[SEND_UART_SIZE];
static const xLC_Q xQueueUART[2] = 
{
    {   .wHead = 0, .wTail = 0, .pBuffer = m_RecvUART,    .dwSize = sizeof(m_RecvUART)    },
    {   .wHead = 0, .wTail = 0, .pBuffer = m_SendUART,    .dwSize = sizeof(m_SendUART)    },
};


//----------------------------------------------
//  Private Functions
//----------------------------------------------
//  ISR
static void OnUartReceiveIT(LC_DWORD dwPort, UART_HandleTypeDef *huart);
static void OnUartReceiveIdleIT(LC_DWORD dwPort, UART_HandleTypeDef *huart);
static void OnUartTransmitIT(LC_DWORD dwPort, UART_HandleTypeDef *huart);
static void OnUartEndTrasmitIT(LC_DWORD dwPort, UART_HandleTypeDef *huart);

static void OnUartISR(LC_DWORD dwPort, UART_HandleTypeDef *huart);



//----------------------------------------------
//  Functions
//----------------------------------------------
LC_HANDLE COM_Open(eDEV eDevice, LC_PVOID pvProperty)
{
    switch(eDevice)
    {
    case DEV_UART1:
        m_xPort[eUART_1].phPort = &huart1;
        m_xPort[eUART_1].pTxDma = m_TdmaTERM;
        m_xPort[eUART_1].nRecvQ = LCAPI_CreateQ((xLC_Q *)&xQueueTERM[0]);
        m_xPort[eUART_1].nSendQ = LCAPI_CreateQ((xLC_Q *)&xQueueTERM[1]);

		if((m_xPort[eUART_1].nRecvQ < 0) || (m_xPort[eUART_1].nSendQ < 0))
		{
			return (LC_HANDLE)LCERR_DRIVER_OPEN;
		}
        
        __HAL_UART_ENABLE_IT(m_xPort[eUART_1].phPort, UART_IT_RXNE | UART_IT_IDLE);
        HAL_NVIC_EnableIRQ(USART1_IRQn);
        HAL_NVIC_EnableIRQ(DMA1_Channel4_IRQn);
        break;
        
    case DEV_UART4:
        m_xPort[eUART_4].phPort = &huart4;
        m_xPort[eUART_4].pTxDma  = m_TdmaUART;
        m_xPort[eUART_4].nRecvQ = LCAPI_CreateQ((xLC_Q *)&xQueueUART[0]);
        m_xPort[eUART_4].nSendQ = LCAPI_CreateQ((xLC_Q *)&xQueueUART[1]);
        
        if((m_xPort[eUART_4].nRecvQ < 0) || (m_xPort[eUART_4].nSendQ < 0))
        {
            return (LC_HANDLE)LCERR_DRIVER_OPEN;
        }
        
        __HAL_UART_ENABLE_IT(m_xPort[eUART_4].phPort, UART_IT_RXNE | UART_IT_IDLE);
        HAL_NVIC_EnableIRQ(UART4_IRQn);
        HAL_NVIC_EnableIRQ(DMA2_Channel5_IRQn);
        break;
        
    default:
        return (LC_HANDLE)LCERR_DRIVER_OPEN;
    }

    return (LC_HANDLE)eDevice;
}

LC_VOID COM_Close(LC_HANDLE pvHandle)
{
    int nPort = (int)pvHandle;
    
    do
    {
        if(m_xPort[nPort].phPort->gState == HAL_UART_STATE_READY)
            break;
        LCAPI_Delaymsec(100);
    }
    while(LC_TRUE);

    switch(nPort)
    {
    case DEV_UART1:
        __HAL_UART_DISABLE_IT(m_xPort[eUART_1].phPort, UART_IT_RXNE | UART_IT_IDLE);
        HAL_NVIC_DisableIRQ(USART1_IRQn);
        HAL_NVIC_DisableIRQ(DMA1_Channel4_IRQn);
        
        LCAPI_DeleteQ(m_xPort[eUART_1].nRecvQ);
        LCAPI_DeleteQ(m_xPort[eUART_1].nSendQ);
        break;
        
    case DEV_UART4:
        __HAL_UART_DISABLE_IT(m_xPort[eUART_4].phPort, UART_IT_RXNE | UART_IT_IDLE);
        HAL_NVIC_DisableIRQ(UART4_IRQn);
        HAL_NVIC_DisableIRQ(DMA2_Channel5_IRQn);
        
        LCAPI_DeleteQ(m_xPort[eUART_4].nRecvQ);
        LCAPI_DeleteQ(m_xPort[eUART_4].nSendQ);
        break;
        
    default:
        return;
    }
    
    pvHandle = (LC_HANDLE)LC_DEINIT;
}

LC_S32 COM_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData)
{
    if((int)pvHandle < 0) return LCERR_DRIVER_WRONG;

    LC_S32 nReturn = 0;

    switch(dwCmd)
    {
    case IOCTL_PRINT_FROM_AP:
        {
            LC_BYTE *pBuffer = (LC_BYTE *)pvOut;
            m_bPrintFromAP = (LC_BOOL)pBuffer[0];
        }
        nReturn = 1;
        break;
    case IOCTL_PRINT_TO_AP:
        {
            LC_BYTE *pBuffer = (LC_BYTE *)pvOut;
            m_bPrintToAP = (LC_BOOL)pBuffer[0];
        }
        nReturn = 1;
        break;
    case IOCTL_PRINT_AMP_LCD:
        {
            LC_BYTE *pBuffer = (LC_BYTE *)pvOut;
            m_bPrintAmpLcd = (LC_BOOL)pBuffer[0];
        }
        nReturn = 1;
        break;
    case IOCTL_PRINT_CAN_RECV:
        {
            LC_BYTE *pBuffer = (LC_BYTE *)pvOut;
            m_bPrintCanRecv = (LC_BOOL)pBuffer[0];
        }
        nReturn = 1;
        break;
    case IOCTL_PRINT_CAN_DIFF:
        {
            LC_BYTE *pBuffer = (LC_BYTE *)pvOut;
            m_bPrintCanDiff = (LC_BOOL)pBuffer[0];
        }
        nReturn = 1;
        break;
    }

    return nReturn;
}

LC_S32 COM_Read(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData)
{
    int nLength;
    int nPort = (int)pvHandle;

	if(nPort >= 0)
	{
	    // ISR sync
	    nLength = LCAPI_GetQLength(m_xPort[nPort].nRecvQ);
	    nLength = ((int)cbData <= nLength) ? (int)cbData : nLength;
	    nLength = LCAPI_GetQBuffer(m_xPort[nPort].nRecvQ, (LC_BYTE *)pvData, nLength, LC_FALSE);
	    //

	    return nLength;
	}
	else
	{
		return nPort;
	}
}

LC_S32 COM_Write(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData)
{
    int nAvailable;
    int nReturn;
    int nPort = (int)pvHandle;

	if(nPort >= 0)
	{
	    //  ISR Sync
        nAvailable = LCAPI_GetQAvailableSpace(m_xPort[nPort].nSendQ);
        if(nAvailable < (int)cbData) return 0;

        nReturn = LCAPI_PutQBuffer(m_xPort[nPort].nSendQ, (LC_BYTE *)pvData, cbData);
            
	    if(m_xPort[nPort].phPort->gState == HAL_UART_STATE_READY)
	    {
	        int nLength = LCAPI_GetQLength(m_xPort[nPort].nSendQ);
	        if(nLength > 0)
	        {
	            nLength = LCAPI_GetQBuffer(m_xPort[nPort].nSendQ, m_xPort[nPort].pTxDma, nLength, LC_FALSE);
	            HAL_UART_Transmit_DMA(m_xPort[nPort].phPort, m_xPort[nPort].pTxDma, nLength);
	        }
	    }
        
	    return nReturn;
	}
	else
	{
		return nPort;
	}
}


//----------------------------------------------
//  ISR
//----------------------------------------------
void USART1_IRQHandler(void)
{
    OnUartISR((LC_DWORD)eUART_1, m_xPort[eUART_1].phPort);
}

void UART4_IRQHandler(void)
{
    OnUartISR((LC_DWORD)eUART_4, m_xPort[eUART_4].phPort);
}

static void OnUartReceiveIT(LC_DWORD dwPort, UART_HandleTypeDef *huart)
{
    LC_BYTE chData = huart->Instance->DR & 0xFF;

    int nSize = LCAPI_GetQAvailableSpace(m_xPort[dwPort].nRecvQ);
    if(nSize > 0)
        LCAPI_PutQBuffer(m_xPort[dwPort].nRecvQ, &chData, 1);
    
    if(dwPort == eUART_4 && m_bPrintFromAP == LC_TRUE)
    {
        if(chData != COMM_EOF)
            huart1.Instance->DR = chData;
        else
            huart1.Instance->DR = 0x7F;
    }
}

static void OnUartReceiveIdleIT(LC_DWORD dwPort, UART_HandleTypeDef *huart)
{
    LC_BYTE chData = huart->Instance->DR;

    switch(dwPort)
    {
    case eUART_1:
        LCAPI_PostEventISR(EVENT_TERM_RC);
        break;
    case eUART_4:
        LCAPI_PostEventISR(EVENT_UART_RC);
        break;
    }
}

static void OnUartTransmitIT(LC_DWORD dwPort, UART_HandleTypeDef *huart)
{
}

static void OnUartEndTrasmitIT(LC_DWORD dwPort, UART_HandleTypeDef *huart)
{
    __HAL_UART_CLEAR_FLAG(huart, UART_FLAG_TC);

    // Disable the UART Transmit Complete Interrupt
    __HAL_UART_DISABLE_IT(huart, UART_IT_TC);

    // Check if a receive process is ongoing or not
    huart->gState = HAL_UART_STATE_READY;
        
    int nLength = LCAPI_GetQLength(m_xPort[dwPort].nSendQ);
    if(nLength > 0)
    {
        nLength = LCAPI_GetQBuffer(m_xPort[dwPort].nSendQ, m_xPort[dwPort].pTxDma, nLength, LC_FALSE);
        HAL_UART_Transmit_DMA(huart, m_xPort[dwPort].pTxDma, nLength);
    }
}

static void OnUartISR(LC_DWORD dwPort, UART_HandleTypeDef *huart)
{
    uint32_t tmp_flag = 0, tmp_it_source = 0;

    tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_PE);
    tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_PE);  
    //  UART parity error interrupt occurred
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    { 
        huart->ErrorCode |= HAL_UART_ERROR_PE;
    }
      
    tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_FE);
    tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_ERR);
    //  UART frame error interrupt occurred
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    { 
        huart->ErrorCode |= HAL_UART_ERROR_FE;
    }
      
    tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_NE);
    //  UART noise error interrupt occurred
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    { 
        huart->ErrorCode |= HAL_UART_ERROR_NE;
    }
      
    tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_ORE);
    //  UART Over-Run interrupt occurred
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    { 
        huart->ErrorCode |= HAL_UART_ERROR_ORE;
    }
      
    tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_RXNE);
    tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_RXNE);
    //  UART in mode Receiver
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    { 
        OnUartReceiveIT(dwPort, huart);
    }
    
    tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_IDLE);
    tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_IDLE);
    //  UART in mode Receive IDLE
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    {
        OnUartReceiveIdleIT(dwPort, huart);
    }
      
    tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_TXE);
    tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_TXE);
    //  UART in mode Transmitter
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    {
        OnUartTransmitIT(dwPort, huart);
    }

    tmp_flag = __HAL_UART_GET_FLAG(huart, UART_FLAG_TC);
    tmp_it_source = __HAL_UART_GET_IT_SOURCE(huart, UART_IT_TC);
    //  UART in mode Transmitter end
    if((tmp_flag != RESET) && (tmp_it_source != RESET))
    {
        OnUartEndTrasmitIT(dwPort, huart);
    }  

    if(huart->ErrorCode != HAL_UART_ERROR_NONE)
    {
        //  Clear all the error flag at once
        __HAL_UART_CLEAR_PEFLAG(huart);
        
        //  Set the UART state ready to be able to start again the process
        huart->gState = HAL_UART_STATE_READY;
        
        HAL_UART_ErrorCallback(huart);
    }  
}


//----------------------------------------------
//----------------------------------------------
LC_VOID inline LCAPI_Putc(LC_BYTE chEcho)
{
    huart1.Instance->DR = chEcho;
}

LC_VOID LCAPI_WaitTXE_Putc(LC_BYTE chEcho)
{
    
    do
    {
        if(huart1.Instance->SR & USART_SR_TXE)
        {
            huart1.Instance->DR = chEcho;
            break;
        }
    }
    while(LC_TRUE);
}

LC_VOID LCAPI_toAP_Putc(LC_BYTE chEcho)
{
    if(m_bPrintToAP == LC_TRUE)
    {
        LCAPI_Putc(chEcho);
    }
}

LC_VOID LCAPI_AmpLcd_Putc(LC_BYTE chEcho)
{
    if(m_bPrintAmpLcd == LC_TRUE)
    {
        LCAPI_Putc(chEcho);
    }
}

LC_VOID LCAPI_CanRecv_Putc(LC_BYTE chEcho)
{
    if(m_bPrintCanRecv == LC_TRUE)
    {
        LCAPI_Putc(chEcho);
    }
}

LC_VOID LCAPI_CanDiff_Putc(LC_BYTE chEcho)
{
    if(m_bPrintCanDiff == LC_TRUE)
    {
        LCAPI_Putc(chEcho);
    }
}
