//----------------------------------------------
//  Headers
//----------------------------------------------
#include "DevFlash.h"


#ifdef STM32F105xC
//----------------------------------------------
//  Defines
//----------------------------------------------


//----------------------------------------------
//  Variables
//----------------------------------------------


//----------------------------------------------
//  Private Functions
//----------------------------------------------
static LC_DWORD EraseFlash(LC_DWORD dwAddr, LC_DWORD dwSize);
static LC_DWORD WriteFlash(LC_BYTE *pBuffer, LC_WORD wSize);


//----------------------------------------------
//  Functions
//----------------------------------------------
LC_HANDLE ROM_Open(eDEV eDevice, LC_PVOID pvProperty)
{
    switch((int)eDevice)
    {
    case DEV_FLASH:
        break;
    default:
	    return (LC_HANDLE)LCERR_DRIVER_OPEN;
    }

    return (LC_HANDLE)eDevice;
}

LC_VOID ROM_Close(LC_HANDLE pvHandle)
{
    int nPort = (int)pvHandle;
    
    switch(nPort)
    {
    case DEV_FLASH:
        break;
    default:
        return;
    }
    
    pvHandle = (LC_HANDLE)LC_DEINIT;
}

LC_S32 ROM_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData)
{
    if((int)pvHandle < 0) return LCERR_DRIVER_WRONG;
    
    LC_S32 nReturn = 0;
    
    switch(dwCmd)
    {
    case IOCTL_ERASE_FLASH:
        {
            LC_DWORD *pdwOut = (LC_DWORD *)pvOut;
            LC_DWORD *pdwInp = (LC_DWORD *)pvInp;
            
            pdwInp[0] = EraseFlash(pdwOut[0], pdwOut[1]);
        }
        
        nReturn = 1;
        break;
        
    case IOCTL_WRITE_FLASH:
        {
            LC_BYTE  *pOut   = (LC_BYTE  *)pvOut;
            LC_DWORD *pdwInp = (LC_DWORD *)pvInp;
            
            pdwInp[0] = WriteFlash(pOut, (LC_WORD)cbOutData);
        }
        
        nReturn = 1;
        break;
    }
    
    return nReturn;
}


static LC_DWORD EraseFlash(LC_DWORD dwAddr, LC_DWORD dwSize)
{
    HAL_StatusTypeDef xFlashResult = HAL_OK;
    FLASH_EraseInitTypeDef xFlashEraseDef;
    LC_DWORD dwPageError;

    xFlashResult += HAL_FLASH_Unlock();
    
    xFlashEraseDef.TypeErase = FLASH_TYPEERASE_PAGES;
    xFlashEraseDef.PageAddress = dwAddr + (dwSize - 2048);
    xFlashEraseDef.NbPages = 1;
    xFlashResult += HAL_FLASHEx_Erase(&xFlashEraseDef, &dwPageError);
    
    xFlashEraseDef.TypeErase = FLASH_TYPEERASE_PAGES;
    xFlashEraseDef.PageAddress = dwAddr;
    xFlashEraseDef.NbPages = ((dwSize - 2048) / 2048);          // ((64 - 2) / 2) KByte
    xFlashResult += HAL_FLASHEx_Erase(&xFlashEraseDef, &dwPageError);
    
    xFlashResult += HAL_FLASH_Lock();
    
    return (xFlashResult == HAL_OK) ? 0 : 0xF0;
}

static LC_DWORD WriteFlash(LC_BYTE *pBuffer, LC_WORD wSize)
{
    int i;
    int nOffset;
    HAL_StatusTypeDef xFlashResult = HAL_OK;
    LC_DWORD dwAddr, dwSize, dwRemainder;
    
    dwAddr = (pBuffer[2] << 24) | (pBuffer[3] << 16) | (pBuffer[4] << 8) | (pBuffer[5]);
    dwSize = (LC_DWORD)(pBuffer[1] - 5) >> 2;      // calc dword size
    dwRemainder = (LC_DWORD)(pBuffer[1] - 5) & 0x03;
    
    xFlashResult += HAL_FLASH_Unlock();
    for(i = 0; i < dwSize; i++)
    {
        nOffset = (i * 4) + 6;
        LC_DWORD dwData = (pBuffer[nOffset + 3] << 24)
                        | (pBuffer[nOffset + 2] << 16)
                        | (pBuffer[nOffset + 1] << 8)
                        | (pBuffer[nOffset + 0]);
        xFlashResult += HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dwAddr + i * 4, dwData);
    }
    
    if(dwRemainder)
    {
        nOffset = (i * 4) + 6;
        LC_DWORD dwData;
        
        switch(dwRemainder)
        {
        case 1:
            dwData = (pBuffer[nOffset + 0]);
            break;
        case 2:
            dwData = (pBuffer[nOffset + 1] << 8)
                   | (pBuffer[nOffset + 0]);
            break;
        case 3:
            dwData = (pBuffer[nOffset + 2] << 16)
                   | (pBuffer[nOffset + 1] << 8)
                   | (pBuffer[nOffset + 0]);
            break;
        }
            
        xFlashResult += HAL_FLASH_Program(FLASH_TYPEPROGRAM_WORD, dwAddr + i * 4, dwData);
    }
    
    xFlashResult += HAL_FLASH_Lock();
    
    return (xFlashResult == HAL_OK) ? pBuffer[0] : 0xF1;
}


#elif defined STM32F103xE
LC_HANDLE ROM_Open(eDEV eDevice, LC_PVOID pvProperty)
{
    return (LC_HANDLE)LCERR_DRIVER_OPEN;
}

LC_VOID ROM_Close(LC_HANDLE pvHandle)
{
}

LC_S32 ROM_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData)
{
    return 0;
}


#else

LC_HANDLE ROM_Open(eDEV eDevice, LC_PVOID pvProperty)
{
    return (LC_HANDLE)LCERR_DRIVER_OPEN;
}

LC_VOID ROM_Close(LC_HANDLE pvHandle)
{
}

LC_S32 ROM_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData)
{
    return 0;
}
#endif