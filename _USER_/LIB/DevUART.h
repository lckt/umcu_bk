#ifndef _IST_UART_H_
#define _IST_UART_H_

#include "defines.h"

extern LC_HANDLE COM_Open  (eDEV eDevice, LC_PVOID pvProperty);
extern LC_VOID   COM_Close (LC_HANDLE pvHandle);
extern LC_S32    COM_Ioctl (LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData);
extern LC_S32    COM_Read  (LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData);
extern LC_S32    COM_Write (LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData);

#endif
