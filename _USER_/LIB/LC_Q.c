//----------------------------------------------
//	Headers
//----------------------------------------------
#include "LC_Q.h"


//----------------------------------------------
//	Defines
//----------------------------------------------



//----------------------------------------------
//	Pre-Defined Variables
//----------------------------------------------
static xLC_Q     m_xLC_Q[LC_Q_MAX];


//----------------------------------------------
//	Pre-Defined Functions
//----------------------------------------------



//----------------------------------------------
//	Functions
//----------------------------------------------
void LCAPI_InitQ()
{
    for(int i = 0; i < LC_Q_MAX; i++)
    {
        m_xLC_Q[i].bOpen = LC_FALSE;
    }
}

void LCAPI_DeinitQ()
{
}

int LCAPI_CreateQ(xLC_Q *pQ)
{
    int i;

    for(i = 0; i < LC_Q_MAX; i++)
    {
        if(m_xLC_Q[i].bOpen == LC_FALSE)
            break;
    }

    if(i == LC_Q_MAX)
        return (-1);
  

    m_xLC_Q[i].bOpen = LC_TRUE;

    m_xLC_Q[i].hLock   = NULL;
    m_xLC_Q[i].wHead   = pQ->wHead;
    m_xLC_Q[i].wTail   = pQ->wTail;
    m_xLC_Q[i].pBuffer = pQ->pBuffer;
    m_xLC_Q[i].dwSize  = pQ->dwSize;

    return i;
}

void LCAPI_DeleteQ(LC_DWORD dwQ)
{
    if(dwQ < LC_Q_MAX)
        m_xLC_Q[dwQ].bOpen = LC_FALSE;
}

int LCAPI_PutQMessage(LC_DWORD dwQ, LC_DWORD dwCmd, LC_BYTE *pBuffer, LC_DWORD dwSize)
{
    int nReturn;
	LC_BYTE Buffer[QMESSAGE_HEADER_SIZE];
    
	// Check Free Size
	if(LCAPI_GetQAvailableSpace(dwQ) < (int)(dwSize + QMESSAGE_HEADER_SIZE))
    {
        return -1;
    }
	
    memcpy(Buffer, (void *)&dwCmd, sizeof(LC_DWORD));
    memcpy(Buffer + 4, (void *)&dwSize, sizeof(LC_DWORD));
    
	LCAPI_PutQBuffer(dwQ, Buffer, QMESSAGE_HEADER_SIZE);

	if(dwSize > 0)
	{
		nReturn = LCAPI_PutQBuffer(dwQ, pBuffer, dwSize);
	}
	else
	{
		nReturn = 0;
	}
    
    return nReturn;
}

int LCAPI_GetQMessage(LC_DWORD dwQ, LC_DWORD *pdwCmd, LC_BYTE *pBuffer, LC_DWORD dwSize)
{
	int nSize;
    int nReturn;
    LC_DWORD *pdwSize;
	LC_BYTE Buffer[QMESSAGE_HEADER_SIZE];
    
    nSize = LCAPI_GetQLength(dwQ);
	if(nSize < (int)(dwSize + QMESSAGE_HEADER_SIZE))
    {
        return -1;
    }

	LCAPI_GetQBuffer(dwQ, Buffer, QMESSAGE_HEADER_SIZE, LC_FALSE);
	memcpy(pdwCmd, (void *)Buffer, sizeof(LC_DWORD));
    pdwSize = (LC_DWORD *)(Buffer + 4);
    nSize = (int)*pdwSize;

	if(nSize > 0)
	{
		if(nSize > (int)dwSize)
		{
			nReturn = 0;
		}
		else
		{
			nReturn = LCAPI_GetQBuffer(dwQ, pBuffer, nSize, LC_FALSE);
		}
	}
	else
	{
		nReturn = 0;
	}
    
    return nReturn;
}

int LCAPI_PutQBuffer(LC_DWORD dwQ, LC_BYTE *pBuffer, LC_DWORD dwSize)
{
	LC_WORD wHead;
    LC_WORD wTail;
	int nSize;
	int nWrite;
    int nReturn;
    
	wHead = m_xLC_Q[dwQ].wHead;
	wTail = m_xLC_Q[dwQ].wTail;
	
	nSize = wHead - wTail;  					// Use Size
	if(nSize < 0) nSize += m_xLC_Q[dwQ].dwSize;
	
	// BSWedit TEST needed
//	nSize = m_xLC_Q[dwQ].dwSize - nSize - 1;	// Free Size
	nSize = m_xLC_Q[dwQ].dwSize - nSize;		// Free Size
	
	if(nSize < 0)
    {
        return 0;
    }

	nWrite = 0;
	
	while(dwSize)
	{
		nSize = (wHead < wTail) ? (wTail - wHead) : (m_xLC_Q[dwQ].dwSize - wHead);
		if(nSize > (int)dwSize) nSize = (int)dwSize;

		memcpy(&m_xLC_Q[dwQ].pBuffer[wHead], (char *)(pBuffer + nWrite), nSize);

		dwSize  -= nSize;
		nWrite	+= nSize;
		wHead	+= nSize;
		
		if(wHead >= m_xLC_Q[dwQ].dwSize) wHead = 0;
	}
	
	m_xLC_Q[dwQ].wHead = wHead;
    
    nReturn = nWrite;
        
	return nReturn;
}

int LCAPI_GetQBuffer(LC_DWORD dwQ, LC_BYTE *pBuffer, LC_DWORD dwSize, LC_BOOL bPeek)
{
	LC_WORD wHead;
	LC_WORD wTail;
	int nSize;
	int nRead;
    int nReturn;

	wHead = m_xLC_Q[dwQ].wHead;
	wTail = m_xLC_Q[dwQ].wTail;
	
	nSize = wHead - wTail;					    // Use Size
	if(nSize < 0) nSize += m_xLC_Q[dwQ].dwSize;
	
	if(nSize < (int)dwSize) dwSize = nSize;
	nRead = 0;
	
	while(dwSize)
	{
		nSize = (wHead > wTail) ? (wHead - wTail) : (m_xLC_Q[dwQ].dwSize - wTail);
		if(nSize > dwSize) nSize = dwSize;

		memcpy((char *)pBuffer + nRead, &m_xLC_Q[dwQ].pBuffer[wTail], nSize);

		dwSize  -= nSize;
		nRead	+= nSize;
		wTail	+= nSize;
		
		if(wTail >= m_xLC_Q[dwQ].dwSize) wTail = 0;
	}

	if(bPeek == LC_FALSE)
	{
		m_xLC_Q[dwQ].wTail = wTail;
	}
	
    nReturn = nRead;
        
	return nReturn;
}

int LCAPI_GetQLength(LC_DWORD dwQ)
{
	LC_WORD wHead;
	LC_WORD wTail;
	int nLength;
    int nReturn;
	
	wTail = m_xLC_Q[dwQ].wTail;
	wHead = m_xLC_Q[dwQ].wHead;
	
	nLength = wHead - wTail;				// Use Size
	if(nLength < 0) nLength += m_xLC_Q[dwQ].dwSize;
    
    nReturn = nLength;

	return nReturn;
}

int LCAPI_GetQAvailableSpace(LC_DWORD dwQ)
{
	LC_WORD wHead;
	LC_WORD wTail;
	int nLength;
    int nReturn;

	wTail = m_xLC_Q[dwQ].wTail;
	wHead = m_xLC_Q[dwQ].wHead;

	nLength = wHead - wTail;				// Use Size
	if(nLength < 0) nLength += m_xLC_Q[dwQ].dwSize;

	nReturn = (m_xLC_Q[dwQ].dwSize - nLength - 1);
    
    return nReturn;
}

void LCAPI_EmptyQ(LC_DWORD dwQ)
{
	m_xLC_Q[dwQ].wHead = m_xLC_Q[dwQ].wTail;
}

int LCAPI_IsEmptyQ(LC_DWORD dwQ)
{
	LC_WORD wHead;
	LC_WORD wTail;
    int nReturn;
	
	wTail = m_xLC_Q[dwQ].wTail;
	wHead = m_xLC_Q[dwQ].wHead;

	nReturn = (wHead == wTail) ? 1 : 0;
    
    return nReturn;
}

