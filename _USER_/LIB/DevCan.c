//----------------------------------------------
//  Headers
//----------------------------------------------
#include "DevCan.h"


//----------------------------------------------
//  Definitions
//----------------------------------------------
enum
{
    eCAN_1          = 0,
    eCAN_MAX,
};

enum
{
    eCANID_PAS      = 0,
    eCANID_OPS,
    eCANID_GEAR,
    eCANID_KEY,
    eCANID_ILL,
    eCANID_RMC,
    eCANID_HVAC,
    eCANID_DOOR,
    eCANID_MAX,
};

typedef struct
{
    LC_BOOL bCompared;
    LC_BYTE Buffer[8];
}   xCompare;

#define CAN_FIFO_INIT           0xFF

#define CAN_RECV_BUFFER_SIZE    288             //  (4 + 4 + (4 + 4 + 4 + 4 + 4 + 8)) * 8
#define CAN_SEND_BUFFER_SIZE	36
#define CAN_TEMP_BUFFER_SIZE    CAN_SEND_BUFFER_SIZE

//----------------------------------------------
//  Variables
//----------------------------------------------
extern CAN_HandleTypeDef hcan1;

static xCanPort m_xCanPort[eCAN_MAX];

static CanRxMsgTypeDef Can_Rx_Buff[eCAN_MAX];
static CanRxMsgTypeDef Can_Rx1Buff[eCAN_MAX];
static CanTxMsgTypeDef Can_Tx_Buff[eCAN_MAX];

static LC_BYTE m_RecvCAN[eCAN_MAX][CAN_RECV_BUFFER_SIZE];
static LC_BYTE m_SendCAN[eCAN_MAX][CAN_SEND_BUFFER_SIZE];

static const xLC_Q xQueueCan[eCAN_MAX][3] = 
{
    {
        {   .wHead = 0, .wTail = 0, .pBuffer = m_RecvCAN[eCAN_1],   .dwSize = sizeof(m_RecvCAN[eCAN_1]) },
        {   .wHead = 0, .wTail = 0, .pBuffer = m_SendCAN[eCAN_1],   .dwSize = sizeof(m_SendCAN[eCAN_1]) },
    }
};

//  Vehicle Type
static eVEHICLE_TYPE m_eVehicleType;

static xCompare m_Compare[eCANID_MAX];

static LC_DWORD m_dwCanIntCount = 0;


//----------------------------------------------
//  Private Functions
//----------------------------------------------
//  ISR
static void FilterInit(int nPort, xFilterBankInfoType* pInfo);


//----------------------------------------------
//  Functions
//----------------------------------------------
LC_HANDLE CAN_Open(eDEV eDevice, LC_PVOID pvProperty)
{
    switch((int)eDevice)
    {
    case DEV_CAN1:
        m_xCanPort[eCAN_1].phPort = &hcan1;

        m_xCanPort[eCAN_1].nRecvQ = LCAPI_CreateQ((xLC_Q *)&xQueueCan[eCAN_1][0]);
        m_xCanPort[eCAN_1].nSendQ = LCAPI_CreateQ((xLC_Q *)&xQueueCan[eCAN_1][1]);

        m_xCanPort[eCAN_1].phPort->pRxMsg = &Can_Rx_Buff[eCAN_1];
        m_xCanPort[eCAN_1].phPort->pRxMsg->FIFONumber = CAN_FIFO_INIT;  //  for Callback Function
        m_xCanPort[eCAN_1].phPort->pRx1Msg= &Can_Rx1Buff[eCAN_1];
        m_xCanPort[eCAN_1].phPort->pRx1Msg->FIFONumber= CAN_FIFO_INIT;  //  for Callback Function
        m_xCanPort[eCAN_1].phPort->pTxMsg = &Can_Tx_Buff[eCAN_1];

        for(int j = 0; j < eCANID_MAX; j++)
            m_Compare[j].bCompared = LC_FALSE;

        HAL_CAN_Init(m_xCanPort[eCAN_1].phPort);

        FilterInit(eCAN_1, (xFilterBankInfoType *)pvProperty);

        if (HAL_CAN_Receive_IT(m_xCanPort[eCAN_1].phPort, CAN_FIFO0) != HAL_OK)
        {
///            PRINTF_ERR("CAN 1 FIFO_0 OPEN ERROR!!!\r\n");
            return (LC_HANDLE)LCERR_DRIVER_OPEN;
        }
        if (HAL_CAN_Receive_IT(m_xCanPort[eCAN_1].phPort, CAN_FIFO1) != HAL_OK)
        {
///            PRINTF_ERR("CAN 1 FIFO_1 OPEN ERROR!!!\r\n");
            return (LC_HANDLE)LCERR_DRIVER_OPEN;
        }

///        HAL_NVIC_EnableIRQ(CAN1_TX_IRQn);
///        HAL_NVIC_EnableIRQ(CAN1_RX0_IRQn);
///        HAL_NVIC_EnableIRQ(CAN1_RX1_IRQn);
///        HAL_NVIC_EnableIRQ(CAN1_SCE_IRQn);
        break;

    default:
	    return (LC_HANDLE)LCERR_DRIVER_OPEN;
    }

    m_eVehicleType = LCAPI_GetVehicle();

    return (LC_HANDLE)eDevice;
}

LC_VOID CAN_Close(LC_HANDLE pvHandle)
{
   int nDevice = (int)pvHandle;
   
    switch(nDevice)
    {
    case DEV_CAN1:
//?        HAL_GPIO_WritePin(GPIOA, GPIO_PIN_8, GPIO_PIN_SET);
        
        __HAL_CAN_DISABLE_IT(m_xCanPort[eCAN_1].phPort,
                                    CAN_IT_EWG |
                                    CAN_IT_EPV |
                                    CAN_IT_BOF |
                                    CAN_IT_LEC |
                                    CAN_IT_ERR |
                                    CAN_IT_FMP0|
                                    CAN_IT_FOV0|
                                    CAN_IT_FMP1|
                                    CAN_IT_FOV1|
                                    CAN_IT_TME  );
            
        HAL_CAN_DeInit(m_xCanPort[eCAN_1].phPort);
///        HAL_NVIC_DisableIRQ(CAN1_TX_IRQn);
///        HAL_NVIC_DisableIRQ(CAN1_RX0_IRQn);
///        HAL_NVIC_DisableIRQ(CAN1_RX1_IRQn);
///        HAL_NVIC_DisableIRQ(CAN1_SCE_IRQn);

        m_xCanPort[eCAN_1].phPort->pRxMsg = NULL;
        m_xCanPort[eCAN_1].phPort->pRx1Msg= NULL;
        m_xCanPort[eCAN_1].phPort->pTxMsg = NULL;
        
        LCAPI_DeleteQ(m_xCanPort[eCAN_1].nRecvQ);
        LCAPI_DeleteQ(m_xCanPort[eCAN_1].nSendQ);
        break;
        
    default:
        return;
    }
    
    pvHandle = (LC_HANDLE)LC_DEINIT;
}

LC_S32 CAN_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData)
{
    if((int)pvHandle < 0) return LCERR_DRIVER_WRONG;

    LC_S32 nReturn = 0;

	switch(dwCmd)
	{
    case IOCTL_READ_CANINT:
        {
            LC_DWORD *pdwInp = (LC_DWORD *)pvInp;
            pdwInp[0] = m_dwCanIntCount;
        }
        nReturn = 1;
        break;
	}
    
    return nReturn;
}

LC_S32 CAN_Write(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData)
{
    int nDevice = (int)pvHandle;

	if(nDevice >= 0)
	{
	    return 0;
	}
	else
	{
		return LCERR_DRIVER_WRONG;
	}
}

LC_S32 CAN_Read(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData)
{
	int nLength;
    int nDevice = (int)pvHandle;
    int nPort;
	LC_DWORD dwCmd;

    switch(nDevice)
    {
    case DEV_CAN1:
        nPort = eCAN_1;
        break;
    default:
        return LCERR_DRIVER_WRONG;
    }
    
    nLength = LCAPI_GetQLength(m_xCanPort[nPort].nRecvQ);
	if(nLength >= QMESSAGE_HEADER_SIZE)
	{
		nLength = LCAPI_GetQMessage(m_xCanPort[nPort].nRecvQ, &dwCmd, (LC_BYTE *)pvData, cbData);
	}
	else
	{
		return (LC_S32)LCERR_QUEUE_EMPTY;
	}
    
    return nLength;
}


//----------------------------------------------
//  Interrupt
//----------------------------------------------
void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef* hcan)
{
	CanTxMsgTypeDef xMsg;

	UNUSED(hcan);
    
    m_dwCanIntCount++;
    
///    LCAPI_Putc(m_dwCanIntCount & 0xFF);
    
    if(hcan->Instance == CAN1)
    {
        if(hcan->pRxMsg->FIFONumber == CAN_FIFO0)
        {
            xMsg.StdId = hcan->pRxMsg->StdId;
            xMsg.ExtId = hcan->pRxMsg->ExtId;
            xMsg.IDE = hcan->pRxMsg->IDE;
            xMsg.RTR = hcan->pRxMsg->RTR;
            xMsg.DLC = hcan->pRxMsg->DLC;
            memcpy(xMsg.Data, hcan->pRxMsg->Data, xMsg.DLC);
        
            int i;
            int nPos;
            LC_BOOL bUseData = LC_TRUE;
            
            switch(m_eVehicleType)
            {
            case VEHICLE_SMART:
                switch(xMsg.StdId)
                {
                case CANID_SMART_PAS:
                    nPos = eCANID_PAS;
                    break;
                case CANID_SMART_GEAR:
                    nPos = eCANID_GEAR;
                    break;
                case CANID_SMART_KEY:
                    nPos = eCANID_KEY;
                    break;
                case CANID_SMART_ILL:
                    nPos = eCANID_ILL;
                    break;
                case CANID_SMART_HVAC:
                    nPos = eCANID_HVAC;
                    //
                    xMsg.Data[2] = xMsg.Data[2] & 0xF8;
                    //
                    break;
                case CANID_SMART_DOOR:
                    nPos = eCANID_DOOR;
                    break;
                default:
                    bUseData = LC_FALSE;
                    break;
                }
                break;
                
            case VEHICLE_VITO:
                 switch(xMsg.StdId)
                {
                case CANID_VITO_PAS:
                    nPos = eCANID_PAS;
                    break;
                case CANID_VITO_OPS:
                    nPos = eCANID_OPS;
                    break;
                case CANID_VITO_GEAR:
                    nPos = eCANID_GEAR;
                    break;
                case CANID_VITO_ILL:
                    nPos = eCANID_ILL;
                    break;
                case CANID_VITO_RMC:
                    nPos = eCANID_RMC;
                    break;
                default:
                    bUseData = LC_FALSE;
                    break;
                }
                break;
            }           

            if(bUseData == LC_TRUE)
            {
                LC_BOOL bDifferent = LC_FALSE;

                if(m_Compare[nPos].bCompared == LC_FALSE)
                {
                    m_Compare[nPos].bCompared = LC_TRUE;
                    bDifferent = LC_TRUE;
                }

                for(i = 0; i < 8; i++)
                {
                    if(m_Compare[nPos].Buffer[i] != xMsg.Data[i])
                    {
                        m_Compare[nPos].Buffer[i] = xMsg.Data[i];
                        bDifferent = LC_TRUE;
                    }
                }
                        
                if(bDifferent == LC_TRUE)
                {
                    int nReturn;
                    nReturn = LCAPI_PutQMessage(m_xCanPort[eCAN_1].nRecvQ, (LC_DWORD)eCAN_1, (LC_BYTE *)&xMsg, sizeof(xMsg));
                    if(nReturn >= 0)
                    {
                        LCAPI_PostEventISR(EVENT_CAN1_RC);
                    }
                }
            }
       
            hcan->pRxMsg->FIFONumber = CAN_FIFO_INIT;
            if (HAL_CAN_Receive_IT(hcan, CAN_FIFO0) != HAL_OK)
            {
                /* Reception Error */
///             PRINTF_ERR("HAL_CAN_RxCpltCallback IT ERROR!!!\r\n");
            }
            
            static int nPrint = 0;
            if(++nPrint >= 100)
            {
                nPrint = 0;
                LCAPI_CanRecv_Putc(0xEA);
            }
        }
        //
        else if(hcan->pRx1Msg->FIFONumber == CAN_FIFO1)
        {
            hcan->pRx1Msg->FIFONumber = CAN_FIFO_INIT;
            
            if (HAL_CAN_Receive_IT(hcan, CAN_FIFO1) != HAL_OK)
            {
                /* Reception Error */
///            PRINTF_ERR("HAL_CAN_RxCpltCallback IT ERROR!!!\r\n");
            }

            static int nPrint = 0;
            if(++nPrint >= 500)
            {
                nPrint = 0;
                LCAPI_CanRecv_Putc(0xEC);
            }
        }
    }
}

void HAL_CAN_ErrorCallback(CAN_HandleTypeDef* hcan)
{
	if(hcan->Instance == CAN1)
	{
        LCAPI_SetPhyStb(LC_TRUE);
        
        LCAPI_Putc(0xED);
        
        hcan->pRxMsg->FIFONumber = CAN_FIFO_INIT;
        if (HAL_CAN_Receive_IT(hcan, CAN_FIFO0) != HAL_OK)
        {
            /* Reception Error */
///            PRINTF_ERR("HAL_CAN_RxCpltCallback IT ERROR!!!\r\n");
            LCAPI_Putc(0xEE);
        }
        
        hcan->pRx1Msg->FIFONumber = CAN_FIFO_INIT;
        if (HAL_CAN_Receive_IT(hcan, CAN_FIFO1) != HAL_OK)
        {
            /* Reception Error */
///            PRINTF_ERR("HAL_CAN_RxCpltCallback IT ERROR!!!\r\n");
            LCAPI_Putc(0xEF);
        }
    }
}

//----------------------------------------------
//----------------------------------------------
static void FilterInit(int nPort, xFilterBankInfoType* pInfo)
{
    CAN_FilterConfTypeDef  CAN_FilterInitStructure;
    CAN_FilterInitStructure.FilterActivation = ENABLE;
    
    for(int i = 0; ; i++)
    {
        switch(pInfo[i].xFilterConfig)
        {
        case MASKID32BIT:
            CAN_FilterInitStructure.FilterMode=CAN_FILTERMODE_IDMASK;
            CAN_FilterInitStructure.FilterScale=CAN_FILTERSCALE_32BIT;
            break;
        case MASKID16BIT:
            CAN_FilterInitStructure.FilterMode=CAN_FILTERMODE_IDMASK;
            CAN_FilterInitStructure.FilterScale=CAN_FILTERSCALE_16BIT;
            break;
        case IDLIST32BIT:
            CAN_FilterInitStructure.FilterMode=CAN_FILTERMODE_IDLIST;
            CAN_FilterInitStructure.FilterScale=CAN_FILTERSCALE_32BIT;
            break;
        case IDLIST16BIT:
            CAN_FilterInitStructure.FilterMode=CAN_FILTERMODE_IDLIST;
            CAN_FilterInitStructure.FilterScale=CAN_FILTERSCALE_16BIT;
            break;
        default:
            return;
        }

        CAN_FilterInitStructure.FilterFIFOAssignment       = pInfo[i].chFifo;
        CAN_FilterInitStructure.FilterNumber               = pInfo[i].chBank;
        CAN_FilterInitStructure.FilterIdHigh     = (uint16_t)pInfo[i].wFR1H;
        CAN_FilterInitStructure.FilterIdLow      = (uint16_t)pInfo[i].wFR1L;
        CAN_FilterInitStructure.FilterMaskIdHigh = (uint16_t)pInfo[i].wFR2H;
        CAN_FilterInitStructure.FilterMaskIdLow  = (uint16_t)pInfo[i].wFR2L;
        
        CAN_FilterInitStructure.BankNumber                  = 14;
//        CAN_FilterInitStructure.BankNumber                  = 0x1C;

        if(HAL_CAN_ConfigFilter(m_xCanPort[nPort].phPort, &CAN_FilterInitStructure) != HAL_OK)
        {
///            PRINTF_ERR("HAL_CAN_ConfigFilter ERROR!!!\r\n");
        }
    }
}
