//----------------------------------------------
//  Headers
//----------------------------------------------
#include "DevSPI.h"


//----------------------------------------------
//  Defines
//----------------------------------------------
#define PARKINFO_MAX            4

//----------------------------------------------
//  Variables
//----------------------------------------------
extern SPI_HandleTypeDef hspi1;

static LC_BOOL m_bLoadPas = LC_FALSE;
static LC_BOOL m_bLoadOps = LC_FALSE;

//static xCarInfo m_CarInfo;
int m_nCounter = 0;
int m_aRxCount[PARKINFO_MAX];
int m_aTxCount[PARKINFO_MAX];
static LC_WORD m_awTxBuffer[PARKINFO_MAX] = { 0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF };
LC_WORD m_awTempBuffer[PARKINFO_MAX];

//----------------------------------------------
//  Private Functions
//----------------------------------------------
static LC_VOID OnSpiReceiveIT(SPI_HandleTypeDef *hspi);
static LC_VOID OnSpiTransmitIT(SPI_HandleTypeDef *hspi);


//----------------------------------------------
//  Functions
//----------------------------------------------
LC_HANDLE SPI_Open(eDEV eDevice, LC_PVOID pvProperty)
{
    switch((int)eDevice)
    {
    case DEV_SPI:
        if(hspi1.RxISR == NULL)
            hspi1.RxISR = OnSpiReceiveIT;
        if(hspi1.TxISR == NULL)
            hspi1.TxISR = OnSpiTransmitIT;
        
        hspi1.Instance->DR = m_awTxBuffer[m_nCounter];
        m_aTxCount[m_nCounter]++;
        m_nCounter++;
        
        __HAL_SPI_ENABLE(&hspi1);
        __HAL_SPI_ENABLE_IT(&hspi1, SPI_IT_TXE | SPI_IT_RXNE | SPI_IT_ERR);
        break;
    default:
	    return (LC_HANDLE)LCERR_DRIVER_OPEN;
    }
    
    return (LC_HANDLE)eDevice;
}

LC_VOID SPI_Close(LC_HANDLE pvHandle)
{
    int nPort = (int)pvHandle;
    
    switch(nPort)
    {
    case DEV_SPI:
        __HAL_SPI_DISABLE_IT(&hspi1, SPI_IT_TXE | SPI_IT_RXNE | SPI_IT_ERR);
        __HAL_SPI_DISABLE(&hspi1);
        break;
        
    default:
        return;
    }
    
    pvHandle = (LC_HANDLE)LC_DEINIT;
}

LC_S32 SPI_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData)
{
    if((int)pvHandle < 0) return LCERR_DRIVER_WRONG;
    
    LC_S32 nReturn = 0;
    
    switch(dwCmd)
    {
    case IOCTL_PARKINFO_INIT:
        {
            LC_WORD *pwBuffer = (LC_WORD *)pvOut;

            m_awTempBuffer[0] = pwBuffer[0];
            if(m_bLoadPas == LC_FALSE)
            {
                m_awTempBuffer[1] = pwBuffer[1];
            }
            if(m_bLoadOps == LC_FALSE)
            {
                m_awTempBuffer[2] = pwBuffer[2];
                m_awTempBuffer[3] = pwBuffer[3];
            }
        }
        nReturn = 1;
        break;
        
    case IOCTL_LOAD_PAS:
        {
            LC_DWORD *pdwBuffer = (LC_DWORD *)pvOut;
            
            m_awTempBuffer[1] = (LC_WORD)pdwBuffer[0];
            
            if(m_bLoadPas == LC_FALSE) m_bLoadPas = LC_TRUE;
        }
        nReturn = 1;
        break;

    case IOCTL_LOAD_OPS:
        {
            LC_BYTE *pBuffer = (LC_BYTE *)pvOut;
            
            m_awTempBuffer[2] = (pBuffer[0] << 8) | (pBuffer[1]);
            m_awTempBuffer[3] = (pBuffer[2] << 8) | (pBuffer[3]);
            
            if(m_bLoadOps == LC_FALSE) m_bLoadOps = LC_TRUE;
        }
        nReturn = 1;
        break;
    }
    
    return nReturn;
}

LC_S32 SPI_Write(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData)
{
    return 0;
}

LC_S32 SPI_Read(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData)
{
    return 0;
}


static LC_VOID OnSpiReceiveIT(SPI_HandleTypeDef *hspi)
{
    int nCount;

    nCount = hspi->Instance->DR;
    if((nCount & 0xFF00) != 0xAA00)
    {
        LCAPI_Putc((nCount >> 8) & 0xFF);
        LCAPI_WaitTXE_Putc(nCount & 0xFF); 
    }
    if((nCount & 0x00FF) >= 4)
    {
        LCAPI_WaitTXE_Putc(nCount & 0xFF);
    }
    nCount = nCount & 0x03;
    
    m_aRxCount[nCount]++;
    
    if(nCount == (PARKINFO_MAX - 1))
    {
        if(!(hspi->Instance->SR & SPI_FLAG_TXE))
        {
            if(m_nCounter != 1)
            {
                LCAPI_WaitTXE_Putc(0xF0 + (m_nCounter & 0x03));
                m_nCounter = 1;
            }
        }
        else
        {
            LCAPI_WaitTXE_Putc(0xF4 + (m_nCounter & 0x03));
        }
    }
}

static LC_VOID OnSpiTransmitIT(SPI_HandleTypeDef *hspi)
{
    if(m_nCounter < PARKINFO_MAX)
    {
        hspi->Instance->DR = m_awTxBuffer[m_nCounter];
        m_aTxCount[m_nCounter]++;
        
        if(++m_nCounter >= PARKINFO_MAX)
        {
            m_nCounter = 0;
            
            for(int i = 0; i < PARKINFO_MAX; i++)
            {
                if(m_awTxBuffer[i] != m_awTempBuffer[i])
                {
                    m_awTxBuffer[i] = m_awTempBuffer[i];
                }
            }
        }
    }
    else
    {
        LCAPI_Putc(0xF8 + m_nCounter & 0x07);
    }
}


//  BSWedit modify SPI Error
void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi)
{
//    LCAPI_Putc(0xF6);
    LCAPI_Putc(hspi->ErrorCode & 0xFF);
    
    __HAL_SPI_ENABLE_IT(&hspi1, SPI_IT_TXE | SPI_IT_RXNE | SPI_IT_ERR);
}
