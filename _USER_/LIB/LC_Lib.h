#ifndef _LC_LIB_H_
#define _LC_LIB_H_

#include "defines.h"

extern LC_DWORD LCLIB_ParseSrec(LC_BYTE *pDst, LC_DWORD *pdwDst, LC_BYTE *pSrc, LC_DWORD dwSrc);

extern LC_BYTE LCLIB_Ascii2Hex(LC_BYTE chH, LC_BYTE chL);

extern LC_BOOL LCLIB_CheckUniqueId();
extern LC_DWORD LCLIB_ReadVersion(LC_BYTE *pBuffer);

extern LC_DWORD LCLIB_GetBootType();
extern LC_VOID LCLIB_SetBootType(LC_DWORD dwBootType);

extern LC_WORD LCLIB_GetAmpStatus();
extern LC_VOID LCLIB_SetAmpStatus(LC_WORD wStatus);

extern LC_WORD LCLIB_GetLcdStatus();
extern LC_VOID LCLIB_SetLcdStatus(LC_WORD wStatus);

extern LC_WORD LCLIB_GetRcamStatus();
extern LC_VOID LCLIB_SetRcamStatus(LC_WORD wStatus);

#endif