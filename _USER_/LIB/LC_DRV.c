//----------------------------------------------
//	Headers
//----------------------------------------------
#include "LC_DRV.h"

#include "LC_Q.h"

#include "DevUART.h"
#include "DevFlash.h"
#include "DevCan.h"
#include "DevSPI.h"
#include "DevAdc.h"


//----------------------------------------------
//	Defines
//----------------------------------------------



//----------------------------------------------
//  Variables
//----------------------------------------------
extern LC_HANDLE *g_phDevice;

xDriverTable xDriverList[DEV_MAX] =
{
    {   .open = COM_Open,       .close = COM_Close,     .ioctl = COM_Ioctl,     .read = COM_Read,       	.write = COM_Write      },
    {   .open = COM_Open,       .close = COM_Close,     .ioctl = COM_Ioctl,     .read = COM_Read,       	.write = COM_Write      },
    {   .open = ROM_Open,       .close = ROM_Close,     .ioctl = ROM_Ioctl,     .read = NULL,           	.write = NULL           },
	{   .open = CAN_Open,       .close = CAN_Close,     .ioctl = CAN_Ioctl,     .read = CAN_Read,        	.write = NULL           },
    {   .open = SPI_Open,       .close = SPI_Close,     .ioctl = SPI_Ioctl,     .read = SPI_Read,           .write = SPI_Write      },
    {   .open = ADC_Open,       .close = ADC_Close,     .ioctl = ADC_Ioctl,     .read = NULL,           	.write = NULL           },
};


//----------------------------------------------
//	Pre-Defined Functions
//----------------------------------------------



//----------------------------------------------
//	Functions : FreeRTOS
//----------------------------------------------
LC_HANDLE LCDRV_Open(eDEV eDevice, LC_PVOID pvProperty)
{
    if((int)*(g_phDevice + eDevice) < 0)
    {
        *(g_phDevice + eDevice) = (LC_HANDLE)eDevice;
        
        xDriverList[eDevice].open(eDevice, pvProperty);
    }
	else
	{
		return (LC_HANDLE)LCERR_ALREADY_OPENED;
	}
    return (LC_HANDLE)eDevice;
}

LC_VOID LCDRV_Close(LC_HANDLE pvHandle)
{
    int nHandle = (int)pvHandle;
    
    if(nHandle >= 0)
    {
        xDriverList[nHandle].close(pvHandle);
        
        *(g_phDevice + nHandle) = (LC_HANDLE)LCERR_NOT_INITIALIZED;
    }
}

LC_S32 LCDRV_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData)
{
    int nHandle = (int)pvHandle;
    
    if(nHandle >= 0)
    {
        return xDriverList[nHandle].ioctl(pvHandle, dwCmd, pvOut, cbOutData, pvInp, cbInpData);
    }
    else
        return (LC_S32)LCERR_NOT_INITIALIZED;
}

LC_S32 LCDRV_Read(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData)
{
    int nHandle = (int)pvHandle;
    
    if(nHandle >= 0)
    {
        return xDriverList[nHandle].read(pvHandle, pvData, cbData);
    }
    else
        return (LC_S32)-1;
}

LC_S32 LCDRV_Write(LC_HANDLE pvHandle, LC_PVOID pvData, LC_DWORD cbData)
{
    int nHandle = (int)pvHandle;
    
    if(nHandle >= 0)
    {
        return xDriverList[nHandle].write(pvHandle, pvData, cbData);
    }
    else
        return (LC_S32)-1;
}
