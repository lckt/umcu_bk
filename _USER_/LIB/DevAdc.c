//----------------------------------------------
//  Headers
//----------------------------------------------
#include "DevAdc.h"


//----------------------------------------------
//  Defines
//----------------------------------------------


//----------------------------------------------
//  Variables
//----------------------------------------------
extern ADC_HandleTypeDef hadc1;
extern ADC_HandleTypeDef hadc2;


//----------------------------------------------
//  Private Functions
//----------------------------------------------


//----------------------------------------------
//  Functions
//----------------------------------------------
LC_HANDLE ADC_Open(eDEV eDevice, LC_PVOID pvProperty)
{
    switch((int)eDevice)
    {
    case DEV_ADC:
        hadc1.Instance->SQR3 = 0x0B;
        HAL_ADC_Start(&hadc1);
        hadc2.Instance->SQR3 = 0x0D;
        HAL_ADC_Start(&hadc2);
        break;
    default:
        return (LC_HANDLE)LCERR_DRIVER_OPEN;
    }

    return (LC_HANDLE)eDevice;
}

LC_VOID ADC_Close(LC_HANDLE pvHandle)
{
    int nPort = (int)pvHandle;
    
    switch(nPort)
    {
    case DEV_ADC:
        HAL_ADC_Stop(&hadc1);
        HAL_ADC_Stop(&hadc2);
        break;
    default:
        return;
    }
    
    pvHandle = (LC_HANDLE)LC_DEINIT;
}

LC_S32 ADC_Ioctl(LC_HANDLE pvHandle, LC_DWORD dwCmd, LC_PVOID pvOut, LC_DWORD cbOutData, LC_PVOID pvInp, LC_DWORD cbInpData)
{
    if((int)pvHandle < 0) return LCERR_DRIVER_WRONG;
    
    LC_S32 nReturn = 0;
    
    switch(dwCmd)
    {
    case IOCTL_READ_BUTTON:
        {
            LC_DWORD *pdwOut = (LC_DWORD *)pvOut;
            LC_WORD  *pwInp  = (LC_WORD  *)pvInp;
            
            switch(pdwOut[0])
            {
            case 2:
                HAL_ADC_Stop(&hadc1);
                pwInp[0] = (LC_WORD)HAL_ADC_GetValue(&hadc1);
                hadc1.Instance->SQR3 = 0x0C;
                HAL_ADC_Start(&hadc1);
                break;
            case 4:
                HAL_ADC_Stop(&hadc1);
                pwInp[0] = (LC_WORD)HAL_ADC_GetValue(&hadc1);
                hadc1.Instance->SQR3 = 0x0B;
                HAL_ADC_Start(&hadc1);
                break;
            }
        }
        
        nReturn = 1;
        break;
        
    case IOCTL_READ_BATTERY:
        {
            LC_DWORD *pdwOut = (LC_DWORD *)pvOut;
            LC_DWORD *pdwInp = (LC_DWORD *)pvInp;
            
            switch(pdwOut[0])
            {
            case 4:
                HAL_ADC_Stop(&hadc2);
                pdwInp[0] = HAL_ADC_GetValue(&hadc2);
                HAL_ADC_Start(&hadc2);
                break;
            }
        }
        
        nReturn = 1;
        break;
    }
    
    return nReturn;
}
