//----------------------------------------------
//  Headers
//----------------------------------------------
#include "TaskMC.h"

#include "TaskCanV.h"
#include "TaskCommAP.h"
#include "TaskPcSim.h"
#include "TaskDload.h"
#include "TaskSystem.h"


//----------------------------------------------
//  Definitions
//----------------------------------------------
#define EVENT_MASK              ( EVENT_MESSAGE         \
                                | EVENT_JUMP_BOOT       \
                                )


//----------------------------------------------
//  Variables
//----------------------------------------------
const xTaskTable m_xTaskList[] = 
{
    {   .pvTask = TaskCanV,     .pName = "CanV",        .wID = ID_CANV,     .wStackSize = 128,  .xPriority = osPriorityNormal   },
    {   .pvTask = TaskCommAP,   .pName = "CommAP",      .wID = ID_COMMAP,   .wStackSize = 1024, .xPriority = osPriorityNormal   },
    {   .pvTask = TaskPcSim,    .pName = "PcSim",       .wID = ID_PCSIM,    .wStackSize = 256,  .xPriority = osPriorityNormal   },
    {   .pvTask = TaskUpdate,   .pName = "Update",      .wID = ID_UPDATE,   .wStackSize = 128,  .xPriority = osPriorityNormal   },
    {   .pvTask = TaskSystem,   .pName = "System",      .wID = ID_SYSTEM,   .wStackSize = 256,  .xPriority = osPriorityNormal   },
};
xTaskTable *g_pTaskList = (xTaskTable *)m_xTaskList;
LC_DWORD g_dwTaskListSize = sizeof(m_xTaskList) / sizeof(xTaskTable);

LC_HANDLE  g_ahTasks[ID_MAX];
LC_HANDLE *g_phTasks = (LC_HANDLE *)g_ahTasks;

LC_HANDLE  g_ahEvent[ID_MAX];
LC_HANDLE *g_phEvent = (LC_HANDLE *)g_ahEvent;

LC_HANDLE  g_ahQueue[EVQ_MAX];
LC_HANDLE *g_phQueue = (LC_HANDLE *)g_ahQueue;

LC_HANDLE  g_ahDevice[DEV_MAX];
LC_HANDLE *g_phDevice = (LC_HANDLE *)g_ahDevice;

LC_DWORD g_dwEvent = 0;

static LC_DWORD m_dwID;

xTimerTable m_xTimerList[TIMER_MAX];

//  Task Queue
static LC_BYTE m_MsgQ[128];

static const xLC_Q xQueueList[1] = {
    {   .wHead = 0, .wTail = 0, .pBuffer = m_MsgQ,  .dwSize = sizeof(m_MsgQ), },
};

#ifdef APP_PROGRAM
static LC_BYTE m_Version[4];
#endif

static LC_BOOL m_bJumpReserved = LC_FALSE;

static LC_DWORD m_dwTaskListCount;
static LC_DWORD m_dwTaskListID;


//----------------------------------------------
//  Private Functions
//----------------------------------------------
//  Initial
static LC_BOOL Init();

//  Event & Queue
static LC_BOOL OnEventNotify(LC_DWORD dwEvent);
static LC_BOOL OnTickNotify();

static LC_VOID CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);

//  Receive Commands
static LC_VOID DoJumpBoot(LC_DWORD dwCmd, LC_PVOID lpData, LC_DWORD dwSize);
static LC_VOID DoNotifyWakeup(LC_DWORD dwCmd);
static LC_S32  DoProcessSuspend(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_S32  DoProcessResume(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoSetTimer(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoKillTimer(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);

//  Send

//  Process
static void ProcWakeup();
static void ProcJumpBoot();

//  Task APIs
static LC_BOOL UserTasksCreate(const xTaskTable *pTaskList, LC_DWORD dwSize);


//----------------------------------------------
//  Functions
//----------------------------------------------
void TaskMC()
{
    LC_DWORD dwEvent;

    if(Init() == LC_FALSE)
    {
        // LED ON
        
        return;
    }

    do
    {
        dwEvent = LCAPI_WaitEvent(*(g_phEvent + m_dwID), EVENT_MASK, 1);
        
        if(dwEvent != 0)
            OnEventNotify(dwEvent);
        else
            OnTickNotify();
    }
    while(LC_TRUE);
}

//  Initial
static LC_BOOL Init()
{
    //  Own ID
    m_dwID = ID_MC;
    
    //  ALL Reset Global Tables
    for(int i = 0; i < ID_MAX; i++)
    {
        *(g_phTasks + i) = (LC_HANDLE)LC_DEINIT;
        *(g_phEvent + i) = (LC_HANDLE)LC_DEINIT;
    }
    for(int i = 0; i < EVQ_MAX; i++)
    {
        *(g_phQueue + i) = (LC_HANDLE)LC_DEINIT;
    }
    for(int i = 0; i < DEV_MAX; i++)
    {
        *(g_phDevice + i) = (LC_HANDLE)LC_DEINIT;
    }
    
    //  Own Task ID
    *(g_phTasks + m_dwID) = LCAPI_GetTaskHandle(m_dwID);
    if(*(g_phTasks + m_dwID) == NULL) return LC_FALSE;

    //  Own Event
    *(g_phEvent + m_dwID) = LCAPI_CreateEvent();
    if(*(g_phEvent + m_dwID) == NULL) return LC_FALSE;

    //  Queue Initial & Own Queue
    LCAPI_InitQ();
    
    *(g_phQueue + m_dwID) = (void *)LCAPI_CreateQ((xLC_Q *)&xQueueList[0]);
    if(*(g_phQueue + m_dwID) == (void *)LC_DEINIT) return LC_FALSE;
    
    //  ALL Timer Initial
    for(int i = 0; i < TIMER_MAX; i++)
    {
        m_xTimerList[i].bActive = LC_FALSE;
        m_xTimerList[i].dwID = (LC_DWORD)-1;
    }

    //  Version
#ifdef APP_PROGRAM
    LCLIB_ReadVersion(m_Version);
#endif

    //  Task Create
    return UserTasksCreate(m_xTaskList, g_dwTaskListSize);
}

static LC_BOOL OnEventNotify(LC_DWORD dwEvent)
{
    LC_DWORD dwCmd;
    LC_DWORD dwSize;
    LC_BYTE Buffer[128];

    if(dwEvent & EVENT_JUMP_BOOT)
    {
        CommandProcess(IDM_JUMP_BOOT, NULL, 0);
    }
    if(dwEvent & EVENT_MESSAGE)
    {
        do
        {
            if(*(g_phQueue + m_dwID) == (void *)LC_DEINIT) break;;
            
            int nLength;
            if((nLength = LCAPI_GetQLength((LC_DWORD)*(g_phQueue + m_dwID))) >= QMESSAGE_HEADER_SIZE)
            {
                dwSize = LCAPI_GetQMessage((LC_DWORD)*(g_phQueue + m_dwID), &dwCmd, Buffer, nLength - QMESSAGE_HEADER_SIZE);
                CommandProcess(dwCmd, (void *)Buffer, dwSize);
            }
            else
                break;
        }   while(LC_TRUE);
    }

    return LC_TRUE;
}

static LC_BOOL OnTickNotify()
{
    LCAPI_ClrWdt();

    static LC_BOOL bFirstTick = LC_TRUE;
    if(bFirstTick == LC_TRUE)
    {
        bFirstTick = LC_FALSE;
        CommandProcess(IDM_NOTIFY_WAKEUP, NULL, 0);
    }
    
    int i;
    LC_DWORD dwEvent;

    //  Event from ISR
    dwEvent = LCAPI_GetEventISR();

    if(dwEvent != 0)
    {
        if(dwEvent & EVENT_CAN1_RC)
        {
            LCAPI_PostEvent(ID_CANV, EVENT_CAN1_RC);
        }
        if(dwEvent & EVENT_UART_RC)
        {
            LCAPI_PostEvent(ID_COMMAP, EVENT_UART_RC);
        }
        if(dwEvent & EVENT_TERM_RC)
        {
            LCAPI_PostEvent(ID_PCSIM, EVENT_TERM_RC);
        }
    }

    //  SW Timer
    for(i = 0; i < TIMER_MAX; i++)
    {
        if(m_xTimerList[i].bActive == LC_TRUE)
        {
            m_xTimerList[i].dwCount++;
            if(m_xTimerList[i].dwElapse <= m_xTimerList[i].dwCount)
            {
                m_xTimerList[i].dwCount = 0;
             
                LC_DWORD dwID  = (m_xTimerList[i].dwID >> 16) & 0xFFFF;
                LC_DWORD dwTID = (m_xTimerList[i].dwID      ) & 0xFFFF;
                
                LCAPI_PostMessage(dwID, IDM_ON_TIMER, &dwTID, sizeof(LC_DWORD));
            }
        }
    }
    
    return LC_TRUE;
}

static LC_VOID CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    switch(dwCmd & 0xFFFF)
    {
    case IDM_JUMP_BOOT:
        DoJumpBoot(dwCmd, pData, dwSize);
        break;
    case IDM_NOTIFY_WAKEUP:
        DoNotifyWakeup(dwCmd);
        break;
    case IDM_PROCESS_SUSPEND:
        if(DoProcessSuspend(dwCmd, pData, dwSize) == 0)
        {
            if(m_bJumpReserved == LC_TRUE)
            {
                ProcJumpBoot();
            }
        }
        break;

    case IDM_PROCESS_RESUME:
        if(DoProcessResume(dwCmd, pData, dwSize) == 0)
        {
            ProcWakeup();
        }
        break;
        
    case IDM_SET_TIMER:
        DoSetTimer(dwCmd, pData, dwSize);
        break;
    case IDM_KILL_TIMER:
        DoKillTimer(dwCmd, pData, dwSize);
        break;
    default:
        LCAPI_Printf("(0x%X) Unknown Command 0x%X", m_dwID, dwCmd);
        break;
    }
}


//----------------------------------------------
static LC_VOID DoJumpBoot(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    //  1. Task Kill & Write 'DN' & Jump to 0x08000000
    LCAPI_SetDownload(pdwBuffer[0]);
    
    m_bJumpReserved = LC_TRUE;
    
    if(g_dwTaskListSize > 0)
    {
        m_dwTaskListCount = 0;
        m_dwTaskListID = m_xTaskList[m_dwTaskListCount].wID;
        
        LC_S32 nReturn;
        nReturn =  LCAPI_PostMessage(m_dwTaskListID, IDM_PROCESS_SUSPEND, NULL, 0);
        if(nReturn != EVENT_MESSAGE)
        {
            LCAPI_Printf("ERROR DoJumpBoot 0x%X", nReturn);
        }
    }
}

static LC_VOID DoNotifyWakeup(LC_DWORD dwCmd)
{
    if(g_dwTaskListSize > 0)
    {
        m_dwTaskListCount = 0;
        m_dwTaskListID = m_xTaskList[m_dwTaskListCount].wID;

        LC_S32 nReturn;
        nReturn =  LCAPI_PostMessage(m_dwTaskListID, IDM_PROCESS_RESUME, NULL, 0);
        if(nReturn != EVENT_MESSAGE)
        {
            LCAPI_Printf("ERROR DoNotifyWakeup 0x%X", nReturn);
        }
    }
}

static LC_S32 DoProcessSuspend(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    if(pdwBuffer[0] == m_dwTaskListID)
    {
        if(++m_dwTaskListCount < g_dwTaskListSize)
        {
            m_dwTaskListID = m_xTaskList[m_dwTaskListCount].wID;
            
            LC_S32 nReturn;
            nReturn =  LCAPI_PostMessage(m_dwTaskListID, IDM_PROCESS_SUSPEND, NULL, 0);
            if(nReturn != EVENT_MESSAGE)
            {
                LCAPI_Printf("ERROR DoProcessSuspend %d 0x%X",m_dwTaskListCount, nReturn);
                return LCERR_POST_MESSAGE;
            }
        }
        return (LC_S32)(g_dwTaskListSize - m_dwTaskListCount);
    }
    else
    {
        LCAPI_Printf("ERROR DoProcessSuspend %d : %d %d", m_dwTaskListCount, m_dwTaskListID, pdwBuffer[0]);
        return LCERR_WRONG_ID;
    }
}

static LC_S32 DoProcessResume(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    if(pdwBuffer[0] == m_dwTaskListID)
    {
        LCAPI_Putc((LC_BYTE)pdwBuffer[0]);
        
        if(++m_dwTaskListCount < g_dwTaskListSize)
        {
            m_dwTaskListID = m_xTaskList[m_dwTaskListCount].wID;
            
            LC_S32 nReturn;
            nReturn =  LCAPI_PostMessage(m_dwTaskListID, IDM_PROCESS_RESUME, NULL, 0);
            if(nReturn != EVENT_MESSAGE)
            {
//                LCAPI_Printf("ERROR DoProcessResume %d 0x%X", m_dwTaskListCount, nReturn);
                return LCERR_POST_MESSAGE;
            }
        }
        return (LC_S32)(g_dwTaskListSize - m_dwTaskListCount);
    }
    else
    {
        LCAPI_Printf("ERROR DoProcessResume %d : %d %d", m_dwTaskListCount, m_dwTaskListID, pdwBuffer[0]);
        return LCERR_WRONG_ID;
    }
}

static LC_VOID DoSetTimer(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    int i;
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    for(i = 0; i < TIMER_MAX; i++)
    {
        if(m_xTimerList[i].dwID == pdwBuffer[0])
            break;
    }
    
    //  Find same ID
    if(i < TIMER_MAX)
    {
        m_xTimerList[i].dwElapse = pdwBuffer[1];
        m_xTimerList[i].dwCount  = 0;
        m_xTimerList[i].bActive  = LC_TRUE;
    }
    else
    {
        for(i = 0; i < TIMER_MAX; i++)
        {
            if(m_xTimerList[i].bActive == LC_FALSE)
                break;
        }

        //  Find Timer ID
        if(i < TIMER_MAX)
        {
            m_xTimerList[i].dwID     = pdwBuffer[0];
            m_xTimerList[i].dwElapse = pdwBuffer[1];
            m_xTimerList[i].dwCount  = 0;
            m_xTimerList[i].bActive  = LC_TRUE;
        }
        //  Timer ID Over
        else
        {
            LCAPI_Printf("SWTimer Over");
        }
    }
}

static LC_VOID DoKillTimer(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    int i;
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    for(i = 0; i < TIMER_MAX; i++)
    {
        if(m_xTimerList[i].dwID == pdwBuffer[0])
            break;
    }
    
    //  Find same ID
    if(i < TIMER_MAX)
    {
        m_xTimerList[i].bActive  = LC_FALSE;
    }
    else
    {
///        LCAPI_Printf("KillTimer not Found %d", i);
    }
}


//----------------------------------------------
static void ProcWakeup()
{
#ifdef APP_PROGRAM
    LCAPI_SetJumpCount(0);

#else
    LC_DWORD dwJumpRetry = LCAPI_GetJumpCount();
    if(dwJumpRetry == JUMP_RETRY_MAX)
        LCAPI_SetJumpCount(JUMP_RETRY_MAX - 1);
    else
        LCAPI_SetJumpCount(0);
#endif
}

static void ProcJumpBoot()
{
///    LCAPI_Delaymsec(500);
    HAL_RCC_DeInit();
    __disable_irq();

    HAL_NVIC_DisableIRQ(TIM6_IRQn);

    LCAPI_Jump2Addr(BOOT_START_ADDR);
  
    //  2. Erase whole Update Area
  
    //  3. Send Response to AP
}


//----------------------------------------------
static LC_BOOL UserTasksCreate(const xTaskTable *pTaskList, LC_DWORD dwSize)
{
    LC_BOOL bReturn = LC_TRUE;
    LC_S32 result;

    // Buzzer Test
#if 0
    for(int i = 0; i < 5; i++)
    {
        LCAPI_SetBuzzer(LC_TRUE);
        LCAPI_Delaymsec(150);
        LCAPI_SetBuzzer(LC_FALSE);
        LCAPI_Delaymsec(600);

        LCAPI_ClrWdt();
    }

    for(int i = 0; i < 5; i++)
    {
        LCAPI_SetBuzzer(LC_TRUE);
        LCAPI_Delaymsec(150);
        LCAPI_SetBuzzer(LC_FALSE);
        LCAPI_Delaymsec(350);

        LCAPI_ClrWdt();
    }

    for(int i = 0; i < 5; i++)
    {
        LCAPI_SetBuzzer(LC_TRUE);
        LCAPI_Delaymsec(150);
        LCAPI_SetBuzzer(LC_FALSE);
        LCAPI_Delaymsec(100);

        LCAPI_ClrWdt();
    }

    for(int i = 0; i < 5; i++)
    {
        LCAPI_SetBuzzer(LC_TRUE);
        LCAPI_Delaymsec(300);

        LCAPI_ClrWdt();
    }

    LCAPI_SetBuzzer(LC_FALSE);
#endif

    for(int i = 0; i < (int)dwSize; i++)
    {
        LCAPI_ClrWdt();

        result = LCAPI_CreateTask(pTaskList[i].pvTask,
                                  pTaskList[i].pName,
                                  NULL,
                                  pTaskList[i].wStackSize,
                                  (void *)&pTaskList[i].wID, pTaskList[i].xPriority - osPriorityIdle, (g_ahTasks + i));
        
        if(result == LC_TRUE)
        {
            LCAPI_WaitEvent(*(g_phEvent + m_dwID), EVENT_INIT, 0xFFFFFFFF);
        }
        else
        {
            bReturn = LC_FALSE;
        }
    }

    return bReturn;
}
