//----------------------------------------------
//  Headers
//----------------------------------------------
#include "TaskPcSim.h"


//----------------------------------------------
//  Definitions
//----------------------------------------------
#define EVENT_MASK              (EVENT_MESSAGE | EVENT_TERM_RC)

#define TID_DLOAD_RESPONSE      1000
#define TID_REQ_OSTIME          1001
#define TID_TEST_SYNC           1002

#define REQ_OSTIME_ELAPSE       60000           // 1 minute
#define TEST_SYNC_ELAPSE        200

#define SEND_BUFFER_SIZE        128
#define RECV_BUFFER_SIZE        128


//----------------------------------------------
//  Variables
//----------------------------------------------
extern LC_HANDLE *g_phEvent;
extern LC_HANDLE *g_phQueue;
extern LC_HANDLE *g_phDevice;

static LC_DWORD m_dwID;

static volatile LC_BOOL m_bSuspend = LC_FALSE;

static LC_BYTE m_RecvBuffer[RECV_BUFFER_SIZE];

static LC_DWORD m_dwRecvSize;

static LC_DWORD m_dwRecvLink;
static LC_DWORD m_dwSendLink;

static LC_BOOL m_bError;

//  Task Queue
static LC_BYTE m_MsgQ[128];

static const xLC_Q xQueueList[1] =
{
    {   .wHead = 0, .wTail = 0, .pBuffer = m_MsgQ,  .dwSize = sizeof(m_MsgQ), },
};


//----------------------------------------------
//  Private Functions
//----------------------------------------------
//  Initial
static LC_BOOL Init(LC_PVOID arg);

static void OnResume();
static void OnSuspend();

//  Event & Queue
static LC_BOOL OnEventNotify(LC_DWORD dwEvent);
static void CommandProcess(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);

//  Receive Data
static void DoSuspend(LC_DWORD dwCmd);
static void DoResume(LC_DWORD dwCmd);
static void DoOnTimer(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoRecvTermNotify(LC_DWORD dwCmd);
static void DoEraseResponse(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoWriteResponse(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);

//  Process
static void PostSuspendDone(LC_DWORD dwID);
static void PostResumeDone(LC_DWORD dwID);

//  Recv Serial
static void RecvDataRequest(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvOsTime(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvAmpMute(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvWdtElapse(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvWdtClear();
static void RecvLcdOnoff(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvLcdBrightness(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvLedBrightness(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvDownRequest();
static void RecvDownSendData(LC_BYTE *pBuffer, LC_DWORD cbData);

static void RecvTestVehicleType(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestPrintFromAP(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestPrintToAP(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestPrintAmpLcd(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestPrintAdc(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestPrintCanRecv(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestPrintCanDiff(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestRear(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestPasAutoMove(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestPasManualMove();
static void RecvTestOpsAutoMove(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestOpsManualMove(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvTestBuzzerMute(LC_BYTE *pBuffer, LC_DWORD cbData);

//  Serial Send
static void SendVersion();
static void SendMcuTime();
static void SendReqOsTime();
#   ifndef APP_PROGRAM
static void SendDownResponse(LC_DWORD dwRespnse);
#   endif
static void SendDownSendDataRsp(LC_DWORD dwResponse);
static void SendTestSync();

//  Serial
static int RecvMessage(LC_BYTE *pBuffer, LC_DWORD cbData);
static int StripPacket(LC_BYTE *pDest, LC_BYTE *pSrc, LC_DWORD cbData);
static void StripMessage(LC_BYTE *pBuffer, LC_DWORD cbData);
static void OnRecvData(LC_BYTE *pBuffer, LC_DWORD cbData);
static void OnAcknowledge(LC_BYTE *pBuffer, LC_DWORD cbData);
static void SendAcknowledge(LC_BYTE *pBuffer, LC_DWORD cbData);

static int BuildPacket(LC_BYTE *pDest, LC_BYTE *pSrc, LC_DWORD cbData);
static int BuildRetry(LC_BYTE *pBuffer, LC_DWORD cbData);
static int BuildData(LC_BYTE *pBuffer, LC_DWORD cbData, LC_BOOL bAckRequest);



//----------------------------------------------
//  Functions
//----------------------------------------------
void TaskPcSim(LC_PVOID arg)
{
    LC_DWORD dwEvent;

    if(Init(arg) == LC_FALSE)
        return;

    do
    {
        dwEvent = LCAPI_WaitEvent(*(g_phEvent + m_dwID), EVENT_MASK, WAIT_INFINITE);

        if(dwEvent)
            OnEventNotify(dwEvent);
    }
    while(LC_TRUE);
}

static LC_BOOL Init(void *arg)
{
    LC_WORD *pwBuffer = (LC_WORD *)arg;

    m_dwID = pwBuffer[0];

    *(g_phEvent + m_dwID) = LCAPI_CreateEvent();
    if(*(g_phEvent + m_dwID) == NULL) return LC_FALSE;

    *(g_phQueue + m_dwID) = (void *)LCAPI_CreateQ((xLC_Q *)&xQueueList[0]);
    if(*(g_phQueue + m_dwID) == (void *)LC_DEINIT) return LC_FALSE;
  
///    OnResume();

    LCAPI_PostEvent(ID_MC, EVENT_INIT);

    return LC_TRUE;
}

static void OnResume()
{
    // Timer
#ifdef APP_PROGRAM
    LCAPI_SetTimer(m_dwID, TID_TEST_SYNC, TEST_SYNC_ELAPSE);
#else
    LC_DWORD dwBootType = LCLIB_GetBootType();
    if(dwBootType == RUN_DLOAD_PC)
    {
        LCAPI_SetTimer(m_dwID, TID_DLOAD_RESPONSE, 100);
    }
#endif
    
    // Initial Variable
    m_dwRecvLink = LCAPI_GetRecvLinkSim();
    m_dwSendLink = LCAPI_GetSendLinkSim();

    //  Device Driver Open
    LCDRV_Open(DEV_UART1, NULL);
}

static void OnSuspend()
{
    //  Set for Power Saving
    
    //  Device Driver Close
    LCDRV_Close(*(g_phDevice + DEV_UART1));

    //  Timer Kill
    LCAPI_KillTimer(m_dwID, TID_DLOAD_RESPONSE);
    LCAPI_KillTimer(m_dwID, TID_REQ_OSTIME);
}


//----------------------------------------------
static LC_BOOL OnEventNotify(LC_DWORD dwEvent)
{
    LC_DWORD dwCmd;
    LC_DWORD dwSize;
    LC_BYTE Buffer[128];

    if(dwEvent & EVENT_TERM_RC)
    {
        CommandProcess(IDM_TERM_RECV_NOTIFY, NULL, 0);
    }
    if(dwEvent & EVENT_MESSAGE)
    {
        do
        {
            if(*(g_phQueue + m_dwID) == (void *)LC_DEINIT) break;;
            
            int nLength;
            if((nLength = LCAPI_GetQLength((LC_DWORD)*(g_phQueue + m_dwID))) >= QMESSAGE_HEADER_SIZE)
            {
                dwSize = LCAPI_GetQMessage((LC_DWORD)*(g_phQueue + m_dwID), &dwCmd, Buffer, nLength - QMESSAGE_HEADER_SIZE);
                CommandProcess(dwCmd, Buffer, dwSize);
            }
            else
                break;
        }
        while(LC_TRUE);
    }

    return LC_TRUE;
}

static void CommandProcess(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    switch(dwCmd & 0xFFFF)
    {
    case IDM_PROCESS_SUSPEND:
        DoSuspend(dwCmd);
        break;
    case IDM_PROCESS_RESUME:
        DoResume(dwCmd);
        break;
    case IDM_ON_TIMER:
        DoOnTimer(dwCmd, pData, dwSize);
        break;
    case IDM_TERM_RECV_NOTIFY:
        DoRecvTermNotify(dwCmd);
        break;
    case IDM_ERASE_RESPONSE:
        DoEraseResponse(dwCmd, pData, dwSize);
        break;
    case IDM_WRITE_RESPONSE:
        DoWriteResponse(dwCmd, pData, dwSize);
        break;
    default:
        LCAPI_Printf("(0x%X) Unknown Command 0x%X", m_dwID, dwCmd);
        break;
    }
}

//----------------------------------------------
//----------------------------------------------
//  Process
static void DoSuspend(LC_DWORD dwCmd)
{
    m_bSuspend = LC_TRUE;
    
    OnSuspend();
    
    PostSuspendDone(ID_MC);
}

static void DoResume(LC_DWORD dwCmd)
{
    m_bSuspend = LC_FALSE;

    OnResume();
    
    PostResumeDone(ID_MC);
    
///    LCAPI_Printf("Printf Avaiable");
}

static void DoOnTimer(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    switch(pdwBuffer[0])
    {
    case TID_DLOAD_RESPONSE:
#ifndef APP_PROGRAM
        LCAPI_KillTimer(m_dwID, TID_DLOAD_RESPONSE);

        SendDownResponse(0);
#endif
        break;
    case TID_REQ_OSTIME:
        LCAPI_KillTimer(m_dwID, TID_REQ_OSTIME);
        SendReqOsTime();
        break;
    case TID_TEST_SYNC:
        LCAPI_KillTimer(m_dwID, TID_TEST_SYNC);
        SendTestSync();
        break;
    }
}

static void DoRecvTermNotify(LC_DWORD dwCmd)
{
    int nLength;
    LC_BYTE Buffer[128];

    while((nLength = LCDRV_Read(*(g_phDevice + DEV_UART1), Buffer, 128)) > 0)
    {
        RecvMessage(Buffer, (LC_DWORD)nLength);
    }
}

static void DoEraseResponse(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    SendDownSendDataRsp(pdwBuffer[0]);
}

static void DoWriteResponse(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    SendDownSendDataRsp(pdwBuffer[0]);
}


//  Send
static void PostSuspendDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_SUSPEND, &m_dwID, sizeof(LC_DWORD));
}

static void PostResumeDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_RESUME, &m_dwID, sizeof(LC_DWORD));
}


//----------------------------------------------
//----------------------------------------------
//  Serial Recv
static void RecvDataRequest(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[0] & 0x7F)
    {
    case FMT_CONTROL:
        switch(pBuffer[3])
        {
        case CMD_MCU_VERSION:
            SendVersion();
            break;
        case CMD_MCU_TIME:
            SendMcuTime();
            break;
        }
        break;
    }        
}

static void RecvOsTime(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pBuffer;
    
    if(LCAPI_SetRtcTime(pdwBuffer[0]) == LC_FALSE)
    {
        LCAPI_SetTimer(m_dwID, TID_REQ_OSTIME, REQ_OSTIME_ELAPSE);
    }
}

static void RecvAmpMute(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_APSET_MUTE, pBuffer, 1);

    LCAPI_SetAmpMute((LC_BOOL)pBuffer[0]);
}

static void RecvWdtElapse(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_WDT_ELAPSE, pBuffer, 1);
}

static void RecvWdtClear()
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_WDT_CLEAR, NULL, 0);
}

static void RecvLcdOnoff(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    if(pBuffer[0] != 0)
        LCAPI_PostMessage(ID_SYSTEM, IDM_LCD_ON, NULL, 0);
    else
        LCAPI_PostMessage(ID_SYSTEM, IDM_LCD_OFF, NULL, 0);
}

static void RecvLcdBrightness(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_LCD_BRIGHTNESS, pBuffer, cbData);
}

static void RecvLedBrightness(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_FLED_BRIGHTNESS, pBuffer, 1);
}

static void RecvDownRequest()
{
#ifdef APP_PROGRAM
    LCAPI_SetRecvLinkSim(m_dwRecvLink);
    LCAPI_SetSendLinkSim(m_dwSendLink);

    LC_DWORD dwDload = RUN_DLOAD_PC;
    LCAPI_PostMessage(ID_MC, IDM_JUMP_BOOT, &dwDload, sizeof(LC_DWORD));
#else
    SendDownResponse(0);
#endif
}

static void RecvDownSendData(LC_BYTE *pBuffer, LC_DWORD cbData)
{
#ifndef APP_PROGRAM
    LC_DWORD dwDst;
    LC_BYTE Buffer[32];
    LC_DWORD dwResponse;

    //  1.  Parsing Data
    dwResponse = LCLIB_ParseSrec(Buffer + 1, &dwDst, pBuffer + 3, cbData - 3);
    //  2.  Make Write Data and Write Flash
    if(dwResponse > 0x00 && dwResponse < 0x80)
    {
        Buffer[0] = (LC_BYTE)dwResponse;
        dwDst += 1;
        LCAPI_PostMessage(ID_UPDATE, IDM_WRITE_FLASH_PCSIM, (void *)Buffer, dwDst);
    }
    else if(dwResponse == 0)
    {
        LC_DWORD dwBuffer[2];
        dwBuffer[0] = 0x08010000;
        dwBuffer[1] = 0x00010000;

        LCAPI_PostMessage(ID_UPDATE, IDM_ERASE_FLASH_PCSIM, (void *)dwBuffer, sizeof(dwBuffer));
    }
    else
        SendDownSendDataRsp(dwResponse);
    //  3.  Send Response to AP

#endif
}

static void RecvTestVehicleType(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_SetVehicle((eVEHICLE_TYPE)pBuffer[0]);
}

static void RecvTestPrintFromAP(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[0])
    {
    case 0:
    case 1:
        LCDRV_Ioctl(*(g_phDevice + DEV_UART1), IOCTL_PRINT_FROM_AP, pBuffer, 1, NULL, 0);
        break;
    }
}

static void RecvTestPrintToAP(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[0])
    {
    case 0:
    case 1:
        LCDRV_Ioctl(*(g_phDevice + DEV_UART1), IOCTL_PRINT_TO_AP, pBuffer, 1, NULL, 0);
        break;
    }
}

static void RecvTestPrintAmpLcd(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[0])
    {
    case 0:
    case 1:
        LCDRV_Ioctl(*(g_phDevice + DEV_UART1), IOCTL_PRINT_AMP_LCD, pBuffer, 1, NULL, 0);
        break;
    }
}

static void RecvTestPrintAdc(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_TEST_ADC, pBuffer, 1);
}

static void RecvTestPrintCanRecv(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[0])
    {
    case 0:
    case 1:
        LCDRV_Ioctl(*(g_phDevice + DEV_UART1), IOCTL_PRINT_CAN_RECV, pBuffer, 1, NULL, 0);
        break;
    }
}

static void RecvTestPrintCanDiff(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[0])
    {
    case 0:
    case 1:
        LCDRV_Ioctl(*(g_phDevice + DEV_UART1), IOCTL_PRINT_CAN_DIFF, pBuffer, 1, NULL, 0);
        break;
    }
}

static void RecvTestRear(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, (pBuffer[0] == 1) ? IDM_RGEAR_ON : IDM_RGEAR_OFF, NULL, 0);
}

static void RecvTestPasAutoMove(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_TEST_PAS_AUTOMOVE, pBuffer, 1);
}

static void RecvTestPasManualMove()
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_TEST_PAS_MANUALMOVE, NULL, 0);
}

static void RecvTestOpsAutoMove(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_TEST_OPS_AUTOMOVE, pBuffer, 1);
}

static void RecvTestOpsManualMove(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_TEST_OPS_MANUALMOVE, pBuffer, 1);
}

static void RecvTestBuzzerMute(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_TEST_BUZZER_MUTE, pBuffer, 1);
}


//  Serial Send
static void SendVersion()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[16];
    
    LC_DWORD dwVersionSize;
    LC_BYTE Version[8];
    
    dwVersionSize = LCLIB_ReadVersion(Version);

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_MCU_VERSION;
    Buffer[dwSize++] = dwVersionSize;          //  Size
    for(int i = 0; i < dwVersionSize; i++)
        Buffer[dwSize++] = Version[i];
  
    BuildData(Buffer, dwSize, LC_TRUE);
}

static void SendMcuTime()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[16];
    
    LC_DWORD dwMcuTime;

    dwMcuTime = LCAPI_GetRtcTime();
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_MCU_TIME;
    Buffer[dwSize++] = 4;
    Buffer[dwSize++] = (dwMcuTime      ) & 0xFF;
    Buffer[dwSize++] = (dwMcuTime >>  8) & 0xFF;
    Buffer[dwSize++] = (dwMcuTime >> 16) & 0xFF;
    Buffer[dwSize++] = (dwMcuTime >> 24) & 0xFF;
    
    BuildData(Buffer, dwSize, LC_TRUE);
}

static void SendReqOsTime()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_REQ_OS_TIME;
    Buffer[dwSize++] = 0;
    
    BuildData(Buffer, dwSize, LC_TRUE);
}

#ifndef APP_PROGRAM
static void SendDownResponse(LC_DWORD dwResponse)
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_DLOAD_RESPONSE;
    Buffer[dwSize++] = 1;                   // Size
    Buffer[dwSize++] = (LC_BYTE)dwResponse;
  
    BuildData(Buffer, dwSize, LC_TRUE);
}
#endif

static void SendDownSendDataRsp(LC_DWORD dwResponse)
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_DLOAD_DATA_RESPONSE;
    Buffer[dwSize++] = 1;
    Buffer[dwSize++] = (LC_BYTE)dwResponse;
    
    BuildData(Buffer, dwSize, LC_TRUE);
}

static void SendTestSync()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL;
    Buffer[dwSize++] = CMD_TEST_SYNC;
    Buffer[dwSize++] = 0;
    
    BuildData(Buffer, dwSize, LC_TRUE);
}


//  Serial RX
static int RecvMessage(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    int nSize;
    LC_BYTE Buffer[RECV_BUFFER_SIZE];

    for(int i = 0; i < (int)cbData; i++)
    {
        if(pBuffer[i] == COMM_EOF)
        {
            if(m_dwRecvSize >= 5)
            {
                nSize = StripPacket(Buffer, m_RecvBuffer, m_dwRecvSize);
                if(nSize >= 5)
                {
                    StripMessage(Buffer, nSize - 1);
                }
            }

            m_dwRecvSize = 0;
            continue;
        }

        m_RecvBuffer[m_dwRecvSize++] = pBuffer[i];
    }

    return 0;
}

static int StripPacket(LC_BYTE *pDest, LC_BYTE *pSrc, LC_DWORD cbData)
{
    int nSize = 0;
    LC_BYTE chSum = 0;

    for(int i = 0; i < (int)cbData; i++, nSize++)
    {
        if(pSrc[i] == COMM_IDF)
        {
            i++;
            pDest[nSize] = pSrc[i] | 0x20;
        }
        else
        {
            pDest[nSize] = pSrc[i];
        }

        chSum += pDest[nSize];
    }

    // Parity Error
    if(chSum != 0)
    {
        return 0;
    }

    return nSize;
}

static void StripMessage(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LC_BYTE chLink;
    LC_BYTE chFmt;
    LC_BYTE chCmd;

    chLink = pBuffer[0];
    chFmt  = pBuffer[1];
    chCmd  = pBuffer[2];

    // check command later

    // clear error
    if(m_bError == LC_TRUE)
    {
        m_bError = LC_FALSE;
    }

    // Receive ACK
    if(chFmt == FMT_BASIC && chCmd == COMM_ACK)
    {
        OnAcknowledge(pBuffer, 3);
        return;
    }

    // Send Ack
    if(chFmt & ACK_REQUEST)
    {
        SendAcknowledge(pBuffer, 3);
    }
  
    // Prev RxLink ....
    if(chLink == m_dwRecvLink) return;
    m_dwRecvLink = chLink;

    OnRecvData(pBuffer, (int)cbData);
}

static void OnRecvData(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[1] & 0x7F)
    {
    case FMT_CONTROL:
        switch(pBuffer[2])
        {
        case CMD_AP_REBOOT:
            LCAPI_ResetMcu((LC_DWORD)pBuffer[4]);
            break;
        case CMD_NOP:
            break;
        case CMD_DATA_REQUEST:
            RecvDataRequest(pBuffer + 1, cbData - 1);
            break;
        case CMD_OS_TIME:
            RecvOsTime(pBuffer + 4, cbData - 4);
            break;
        case CMD_REQ_OS_TIME:
            LCAPI_PostMessage(ID_COMMAP, IDM_UART_SEND_SIM, pBuffer, cbData);
            break;
        case CMD_AMP_MUTE:
            RecvAmpMute(pBuffer + 4, cbData - 4);
            break;
        case CMD_WDT_ELAPSE:
            RecvWdtElapse(pBuffer + 4, cbData - 4);
            break;
        case CMD_WDT_CLEAR:
            RecvWdtClear();
            break;
        case CMD_LCD_ONOFF:
            RecvLcdOnoff(pBuffer + 4, cbData - 4);
            break;
        case CMD_LCD_BRIGHTNESS:
            RecvLcdBrightness(pBuffer + 4, cbData - 4);
            break;
        case CMD_LED_BRIGHTNESS:
            RecvLedBrightness(pBuffer + 4, cbData - 4);
            break;

        case CMD_DLOAD_REQUEST:
            RecvDownRequest();
            break;
            
        case CMD_DLOAD_DATA_SEND:
            RecvDownSendData(pBuffer + 1, cbData - 1);
            break;

        case CMD_TEST_VEHICLE_TYPE:
            RecvTestVehicleType(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_PRINT_FROM_AP:
            RecvTestPrintFromAP(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_PRINT_TO_AP:
            RecvTestPrintToAP(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_PRINT_AMP_LCD:
            RecvTestPrintAmpLcd(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_PRINT_ADC:
            RecvTestPrintAdc(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_PRINT_CAN_RECV:
            RecvTestPrintCanRecv(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_PRINT_CAN_DIFF:
            RecvTestPrintCanDiff(pBuffer + 4, cbData - 4);
            break;
        }
        break;
        
    case FMT_CARINFO:
        switch(pBuffer[2])
        {
        case CMD_CAR_STATUS:
        case CMD_BRAKE:
        case CMD_HVAC:
        case CMD_HANDLE_REMOCON:
        case CMD_HANDLE_REMOCON_VITO:
        case CMD_DOOR_OPEN:
            LCAPI_PostMessage(ID_COMMAP, IDM_UART_SEND_SIM, pBuffer, cbData);
            break;

        case CMD_ILLUMINATION:
            LCAPI_PostMessage(ID_COMMAP, IDM_UART_SEND_SIM, pBuffer, cbData);
            LCAPI_PostMessage(ID_SYSTEM, IDM_ILL, pBuffer + 4, cbData - 4);
            break;
            
        case CMD_GEAR_POSITION:
            LCAPI_PostMessage(ID_COMMAP, IDM_UART_SEND_SIM, pBuffer, cbData);
            RecvTestRear(pBuffer + 4, cbData - 4);
            break;
            
        case CMD_TEST_RGEAR:
            RecvTestRear(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_PAS_AUTOMOVE:
            RecvTestPasAutoMove(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_PAS_MANUALMOVE:
            RecvTestPasManualMove();
            break;
        case CMD_TEST_OPS_AUTOMOVE:
            RecvTestOpsAutoMove(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_OPS_MANUALMOVE:
            RecvTestOpsManualMove(pBuffer + 4, cbData - 4);
            break;
        case CMD_TEST_BUZZER_MUTE:
            RecvTestBuzzerMute(pBuffer + 4, cbData - 4);
            break;
        }
        break;
            
    }
}

static void OnAcknowledge(LC_BYTE *pBuffer, LC_DWORD cbData)
{

}

static void SendAcknowledge(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = pBuffer[0];
    Buffer[dwSize++] = FMT_BASIC;
    Buffer[dwSize++] = COMM_ACK;
    Buffer[dwSize++] = 0;                   // Size

    BuildData(Buffer, dwSize, LC_FALSE);
}


//----------------------------------------------
//----------------------------------------------
static int BuildPacket(LC_BYTE *pDest, LC_BYTE *pSrc, LC_DWORD cbData)
{
    int i;
    int nSize;
    unsigned char nSum = 0;
    
    nSize = 0;
    pDest[nSize++] = COMM_EOF;
    
    for(i = 0; i < (int)cbData; i++)
    {
        if(pSrc[i] == COMM_EOF || pSrc[i] == COMM_IDF)
        {
            pDest[nSize++] = COMM_IDF;
            pDest[nSize++] = pSrc[i] & (~0x20);
        }
        else
        {
            pDest[nSize++] = pSrc[i];
        }
        
        nSum += pSrc[i];
    }
    
    nSum = ((~(nSum & 0xFF)) + 1) & 0xFF;
    if(nSum == COMM_EOF || nSum == COMM_IDF)
    {
        pDest[nSize++] = COMM_IDF;
        pDest[nSize++] = nSum & (~0x20);
    }
    else
    {
        pDest[nSize++] = nSum;
    }
    
    pDest[nSize++] = COMM_EOF;
    
    return nSize;
}

static int BuildRetry(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    return 0;
}

static int BuildData(LC_BYTE *pBuffer, LC_DWORD cbData, LC_BOOL bAckRequest)
{
    LC_DWORD dwDstSize;
    LC_BYTE DstBuffer[SEND_BUFFER_SIZE * 2];

    m_dwSendLink = (m_dwSendLink + 1) & 0xFF;
    pBuffer[0] = (LC_BYTE)m_dwSendLink;
    
    if(bAckRequest == LC_TRUE)
    {
        BuildRetry(pBuffer, cbData);
    }

    dwDstSize = BuildPacket(DstBuffer, pBuffer, cbData);

    return LCDRV_Write(*(g_phDevice + DEV_UART1), DstBuffer, dwDstSize);
}



//----------------------------------------------
//  PRINTF
//----------------------------------------------
#include "stdarg.h"

LC_VOID LCAPI_Printf(char *pFormat, ...)
{
    va_list arglist;
    LC_BYTE Buffer[64 + 8] = { 0, };
    
    va_start(arglist, pFormat);
    vsprintf((char *)(Buffer + 4), pFormat, arglist);
    va_end(arglist);
    
    LC_DWORD dwPrintf = strlen((char *)(Buffer + 4));

    LC_DWORD dwSize;
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_BASIC;
    Buffer[dwSize++] = CMD_PRINTF;
    Buffer[dwSize++] = dwPrintf;
    
    BuildData(Buffer, dwSize + dwPrintf, LC_FALSE);
}