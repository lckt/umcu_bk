//----------------------------------------------
//  Headers
//----------------------------------------------
#include "TaskSystem.h"


//----------------------------------------------
//  Definitions
//----------------------------------------------
#define EVENT_MASK              (EVENT_MESSAGE | EVENT_APBOOT)

#define TID_POLL                1000
#define TID_SETOFF              1001
#define TID_CHKOFF              1002
#define TID_AP                  1003
#define TID_PSTATE              1004
#define TID_AMP                 1005
#define TID_LCD                 1006
#define TID_FLED                1007
#define TID_RCAM                1008

#define POLL_ELAPSE             10
#define SETOFF_ELAPSE           3000
#define CHKOFF_ELAPSE           3000            //  3 sec
#define AP_ELAPSE               500             //  1000 => 5 => 500

#define ACC_ON_CONFIRM          20              //  (10 x 50 => 10 x 20) msec
#define ACC_OFF_CONFIRM         20              //  (10 x 50 => 10 x 20) msec
#define ACC_OFF_REQ_OSTIME      1000            //  (10 x 1000) msec => 10 sec
#define ACC_OFF_POWEROFF        3000            //  (10 x 3000) msec => 30 sec

#define LED_CAN_ELAPSE          60
#define LED_CANLESS_ELAPSE      100
#define LED_FACTORY_ELAPSE      150
#define LED_ACCOFF_ELAPSE       30
#define LED_POWEROFF_ELAPSE     10

#define LED_ERROR_NONE          0
#define LED_ERROR_CAN_TIMEOUT   100
#define LED_ERROR_CAN_CLOSED    50

#define IO_ILL_LOOP_MAX         5
#define IO_ILL_SET_CONFIRM      4               //  > (10 x 5 x 2 => 10 x 5 x 4) msec
#define IO_ILL_REL_CONFIRM      4               //  > (10 x 5 x 2 => 10 x 5 x 4) msec

#define IO_RGEAR_LOOP_MAX       2
#define IO_RGEAR_SET_CONFIRM    25              //  > (10 x 2 x 25) msec
#define IO_RGEAR_REL_CONFIRM    3               //  > (10 x 2 x  3) msec

#define RGEAR_ON_CONFIRM        50              //  > (10 x 50) msec
#define RGEAR_OFF_CONFIRM       10              //  > (10 x 50 => 10 x 10) msec

#define DIAG_SET_CONFIRM        10              //  > 100 msec
#define DIAG_REL_CONFIRM        2               //  >  20 msec

#define FLED_ILL_ON_NAVARA      50
#define FLED_ILL_OFF_NAVARA     10

#define FLED_ILL_ON_SMART       10
#define FLED_ILL_OFF_SMART      FLED_MIN_BRIGHT

typedef enum
{
    STATE_POWER_FUZZY = 0,
    STATE_POWER_0,
    STATE_POWER_1,
    STATE_POWER_2,
}   eSTATE_POWER;

typedef enum
{
    STATE_CAN_FUZZY = 0,
    STATE_CAN_NONE,
    STATE_CAN_RECEIVED,
    STATE_CAN_TIMEOUT,
    STATE_CAN_CLOSED,
}   eSTATE_CAN;

enum
{
    eSTATE_AMP_OFF  = 0,
    eSTATE_MUTE_ENABLE,
    eSTATE_AMP_ENABLE,
    eSTATE_MUTESET,
    eSTATE_AMP_ON,
    eSTATE_MUTE_DISABLE,
    eSTATE_AMP_DISABLE,
    eSTATE_MUTE_DIAG,
    eSTATE_AMP_DIAG,
    eSTATE_DIAG_OFF,
    eSTATE_DIAG_OFF2,
};

enum
{
    eSTATE_LCD_INIT = 0,
    eSTATE_LCD_OFF,
    eSTATE_LCD_PON,
    eSTATE_BL_EN,
    eSTATE_PWM_EN,
    eSTATE_BRIGHTNESS_SET,
    eSTATE_LCD_ON,
    eSTATE_BRIGHTNESS_FADEOUT,
    eSTATE_PWM_DIS,
    eSTATE_BL_DIS,
    eSTATE_LCD_POFF,
};

enum
{
    eSTATE_RCAM_OFF = 0,
    eSTATE_RGEAR_CONFIRM,
    eSTATE_RGEAR_APSIG,
    eSTATE_RGEAR_APDISP,
    eSTATE_RCAM_ON,
    eSTATE_XGEAR_CONFIRM,
    eSTATE_XGEAR_APSIG,
    eSTATE_XGEAR_APDISP,
};

//  MACRO
#define IF_NEQ_SET_EQ(x,y)      {   if(x != y) x = y;   }


//----------------------------------------------
//  Variables
//----------------------------------------------
extern LC_HANDLE *g_phEvent;
extern LC_HANDLE *g_phQueue;
extern LC_HANDLE *g_phDevice;

static LC_DWORD m_dwID;

static volatile LC_BOOL m_bSuspend = LC_FALSE;

//  Task Queue
static LC_BYTE m_MsgQ[128];

static const xLC_Q xQueueList[1] = {
    {   .wHead = 0, .wTail = 0, .pBuffer = m_MsgQ,  .dwSize = sizeof(m_MsgQ), },
};


//  Vehicle Type
static LC_DWORD m_eVehicleType;

//  Power Var
static eSTATE_POWER m_ePowerState = STATE_POWER_FUZZY;

//  AP Stae
static LC_BOOL m_bApBoot = LC_FALSE;

//  Can Timeout
static eSTATE_CAN m_eCanState = STATE_CAN_FUZZY;

//  ILL
static LC_BOOL m_bCanIll;
static LC_BOOL m_bIoIll;

//  AMP Var
static LC_S32 m_nAmpState;
static LC_S32 m_nAmpEvent;
static LC_S32 m_nDiagEvent;
static LC_BOOL m_bApsetMute;

//  LCD Var
static LC_S32 m_nLcdState;
static LC_S32 m_nLcdEvent;

//  Front LED Var
static LC_S32 m_nFrontLedState;
static LC_S32 m_nFrontLedEvent;

//static LC_DWORD m_dwCurrButton = 0, m_dwPrevButton = 0;
static LC_S32 m_nCurrPwm, m_nGoalPwm, m_nSetPwm;

static LC_DWORD m_dwCurrLcdSet, m_dwNightLcdSet, m_dwDayLcdSet;
static LC_BOOL m_bAutoLcdSet;

static LC_S32 m_nGoalFrontLed = FLED_MIN_BRIGHT;

static LC_S32 m_nCurrWdt = 0, m_nGoalWdt = 0, m_nSetWdt = 0;

//  RCAM Var
static LC_S32 m_nRcamState;
static LC_S32 m_nRcamEvent;

static LC_BOOL m_bCamRgear = LC_FALSE;

//  ADC
static LC_BOOL m_bTestAdc = LC_FALSE;

//  PAS /  OPS Simulation
static LC_BOOL  m_bPasAuto = LC_FALSE;
static LC_DWORD m_dwPasDir = ePAS_DIR_LEFT;
static LC_S32 m_nPasVal;

static LC_BOOL m_abOpsAuto[4] = { LC_FALSE, LC_FALSE, LC_FALSE, LC_FALSE };
static LC_BYTE m_aOpsDir[4];
static LC_BYTE m_aOpsVal[4];

static LC_BOOL m_bMuteBuzzer = LC_FALSE;
static LC_S32 m_nStepBuzzer = -1;
static LC_S32 m_nCurrBuzzer = -1, m_nClrBuzzerPoint = 0, m_nLoopBuzzerPoint = 0;


///static LC_S32 m_nTestTick1, m_nTestTick2, m_nTestTick3;


//----------------------------------------------
//  Private Functions
//----------------------------------------------
//  Initial
static LC_BOOL Init(void *arg);
static LC_VOID OnResume();
static LC_VOID OnSuspend();

//  Event & Queue
static LC_BOOL OnEventNotify(LC_DWORD dwEvent);
static LC_VOID CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);

//  Process
static LC_VOID DoSuspend(LC_DWORD dwCmd);
static LC_VOID DoResume(LC_DWORD dwCmd);
static LC_VOID DoPoweroff(LC_DWORD dwCmd);
static LC_VOID DoOnTimer(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoCandataReceived(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoCandataTimeout(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoCandataClosed(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoIll(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoAmpEvent(LC_DWORD dwCmd);
static LC_VOID DoDiagEvent(LC_DWORD dwCmd);
static LC_VOID DoApsetMute(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoLcdOn(LC_DWORD dwCmd);
static LC_VOID DoLcdOff(LC_DWORD dwCmd);
static LC_VOID DoLcdCutoff(LC_DWORD dwCmd);
static LC_VOID DoLcdBrightness(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoFrontLedBrightness(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoFrontLedDim(LC_DWORD dwCmd);
static LC_VOID DoRcamEvent(LC_DWORD dwCmd);
static LC_VOID DoRgearOnoff(LC_DWORD dwCmd);
static LC_VOID DoWdtElapse(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoWdtClear(LC_DWORD dwCmd);
static LC_VOID DoTestAdc(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoStepOps(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoTestPasAutoMove(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoTestPasManualMove(LC_DWORD dwCmd);
static LC_VOID DoTestOpsAutoMove(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoTestOpsManualMove(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static LC_VOID DoTestBuzzerMute(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);

//  Send
static LC_VOID PostSuspendDone(LC_DWORD dwID);
static LC_VOID PostResumeDone(LC_DWORD dwID);
static LC_VOID PostOffReady(LC_DWORD dwID, LC_BOOL bReady);
static LC_VOID PostReqOsTime(LC_DWORD dwID);
static LC_VOID PostReqOff(LC_DWORD dwID);

//  Process
static LC_VOID ProcSetOff();
static LC_VOID ProcChkOff();
static LC_VOID ProcButton();
static LC_VOID ProcAcc();
static LC_VOID ProcDiag();
static LC_VOID ProcWdt();
static LC_VOID ProcIoIll();
static LC_VOID ProcIoRgear();
static LC_VOID ProcDebugLed();
static LC_VOID ProcErrorLed();
static LC_VOID ProcRgear();
static LC_VOID ProcBuzzer();
static LC_VOID ProcPasSim();
static LC_VOID ProcOpsSim();

static LC_VOID ProcPstate();
static LC_VOID ProcRcam();
static LC_VOID ProcAP();
static LC_VOID ProcAmp();
static LC_VOID ProcLcd();
static LC_VOID Proc1stLcdData();
static LC_VOID Proc1stLcdSet(LC_BOOL bIll);
static LC_BOOL ProcBright();
static LC_VOID ProcFrontLed();

//  Utils
static LC_VOID UtilReloadOpsPasData();
//  Macro Functions
static LC_VOID PROC_AMP_STATE(LC_S32 nState, LC_S32 nElapse, LC_S32 nPrint);
static LC_VOID PROC_LCD_STATE(LC_S32 nState, LC_S32 nElapse, LC_S32 nPrint);
static LC_VOID PROC_FLED_STATE(LC_S32 nState, LC_S32 nElapse, LC_S32 nPrint);
static LC_VOID PROC_RCAM_STATE(LC_S32 nState, LC_S32 nElapse, LC_S32 nPrint);



//----------------------------------------------
//  Functions
//----------------------------------------------
void TaskSystem(void *arg)
{
    LC_DWORD dwEvent;

    if(Init(arg) == LC_FALSE)
        return;

    do
    {
        dwEvent = LCAPI_WaitEvent(*(g_phEvent + m_dwID), EVENT_MASK, WAIT_INFINITE);

        if(dwEvent != 0)
            OnEventNotify(dwEvent);
    }
    while(LC_TRUE);
}

// Initial
static LC_BOOL Init(void *arg)
{
    LC_WORD *pwBuffer = (LC_WORD *)arg;

    m_dwID = pwBuffer[0];
  
    *(g_phEvent + m_dwID) = LCAPI_CreateEvent();
    if(*(g_phEvent + m_dwID) == NULL) return LC_FALSE;
  
    *(g_phQueue + m_dwID) = (LC_HANDLE)LCAPI_CreateQ((xLC_Q *)&xQueueList[0]);
    if(*(g_phQueue + m_dwID) == (LC_HANDLE)-1) return LC_FALSE;

///    OnResume();
    
    m_eVehicleType = LCAPI_GetVehicle();

    LCAPI_PostEvent(ID_MC, EVENT_INIT);

    return LC_TRUE;
}

static void OnResume()
{
    //  Initial Variable
    m_bCanIll = LC_FALSE;
    m_bIoIll = LC_FALSE;
    
    m_nAmpState = eSTATE_AMP_OFF;
    m_nAmpEvent = 0;
    m_nDiagEvent = 0;
    m_bApsetMute = LC_TRUE;

    m_nLcdState = eSTATE_LCD_INIT;
    m_nLcdEvent = 0;
    m_nCurrPwm = LCD_MIN_BRIGHT;
    m_nGoalPwm = LCD_1ST_BRIGHT;
    m_nSetPwm  = LCD_1ST_BRIGHT;
    m_dwCurrLcdSet  = LCD_1ST_BRIGHT;
    m_dwNightLcdSet = LCD_1ST_BRIGHT;
    m_dwDayLcdSet   = LCD_1ST_BRIGHT;
    m_bAutoLcdSet   = LC_FALSE;
    
    m_nFrontLedState = eSTATE_LCD_OFF;
    m_nFrontLedEvent = 0;
    
    m_nRcamState = eSTATE_RCAM_OFF;
    m_nRcamEvent = 0;
    
    //  Start Timer
    LCAPI_SetTimer(m_dwID, TID_POLL, POLL_ELAPSE);
    LCAPI_SetTimer(m_dwID, TID_AP, AP_ELAPSE);
    LCAPI_SetTimer(m_dwID, TID_AMP, 1);
//    LCAPI_SetTimer(m_dwID, TID_LCD, 1);
//    LCAPI_SetTimer(m_dwID, TID_RCAM, 10);

    //  Initial Lib
    *(g_phDevice + DEV_ADC) = LCDRV_Open(DEV_ADC, NULL);
}

static void OnSuspend()
{
    //  Set for Energy Saving
    
    //  Deint Lib Function ... & etc
    LCDRV_Close(*(g_phDevice + DEV_ADC));
    
    //  Timer Kill
    LCAPI_KillTimer(m_dwID, TID_POLL);
    LCAPI_KillTimer(m_dwID, TID_SETOFF);
    LCAPI_KillTimer(m_dwID, TID_CHKOFF);
    LCAPI_KillTimer(m_dwID, TID_AP);
    LCAPI_KillTimer(m_dwID, TID_AMP);
    LCAPI_KillTimer(m_dwID, TID_LCD);
    LCAPI_KillTimer(m_dwID, TID_RCAM);
}

//  Timer Callback

//  Event & Queue
static LC_BOOL OnEventNotify(LC_DWORD dwEvent)
{
    LC_DWORD dwCmd;
    LC_DWORD dwSize;
    LC_BYTE Buffer[128];
    
    if(dwEvent & EVENT_APBOOT)
    {
        m_bApBoot = LC_TRUE;
        
        // AP Reset at LCD off
        if(m_ePowerState == STATE_POWER_0 && m_nLcdState == eSTATE_LCD_OFF)
        {
            CommandProcess(IDM_LCD_ON, NULL, 0);
        }
    }
    if(dwEvent & EVENT_MESSAGE)
    {
        do
        {
            if(*(g_phQueue + m_dwID) == (void *)LC_DEINIT) break;;

            int nLength;
            if((nLength = LCAPI_GetQLength((LC_DWORD)*(g_phQueue + m_dwID))) >= QMESSAGE_HEADER_SIZE)
            {
                dwSize = LCAPI_GetQMessage((LC_DWORD)*(g_phQueue + m_dwID), &dwCmd, Buffer, nLength - QMESSAGE_HEADER_SIZE);
                CommandProcess(dwCmd, (void *)Buffer, dwSize);
            }
            else
                break;
        }
        while(LC_TRUE);
    }

    return LC_TRUE;
}

static void CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    switch(dwCmd)
    {
    case IDM_PROCESS_SUSPEND:
        DoSuspend(dwCmd);
        break;
    case IDM_PROCESS_RESUME:
        DoResume(dwCmd);
        break;
    case IDM_POWEROFF:
        DoPoweroff(dwCmd);
        break;
    case IDM_ON_TIMER:
        DoOnTimer(dwCmd, pData, dwSize);
        break;
    case IDM_CANDATA_RECEIVED:
        DoCandataReceived(dwCmd, pData, dwSize);
        break;
    case IDM_CANDATA_TIMEOUT:
        DoCandataTimeout(dwCmd, pData, dwSize);
        break;
    case IDM_CANDATA_CLOSED:
        DoCandataClosed(dwCmd, pData, dwSize);
        break;
    case IDM_ILL:
        DoIll(dwCmd, pData, dwSize);
        break;

    case IDM_AMP_ON:
    case IDM_AMP_OFF:
    case IDM_AMP_SET:
        DoAmpEvent(dwCmd);
        break;
    case IDM_DIAG_ON:
    case IDM_DIAG_OFF:
        DoDiagEvent(dwCmd);
        break;
    case IDM_APSET_MUTE:
        DoApsetMute(dwCmd, pData, dwSize);
        break;
        
    case IDM_LCD_ON:
        DoLcdOn(dwCmd);
        break;
    case IDM_LCD_OFF:
        DoLcdOff(dwCmd);
        break;
    case IDM_LCD_CUTOFF:
        DoLcdCutoff(dwCmd);
        break;
    case IDM_LCD_BRIGHTNESS:
        DoLcdBrightness(dwCmd, pData, dwSize);
        break;
    case IDM_FLED_BRIGHTNESS:
        DoFrontLedBrightness(dwCmd, pData, dwSize);
        break;
    case IDM_FLED_DIM:
        DoFrontLedDim(dwCmd);
        break;
        
    case IDM_EVENT_RCAM_ON:
    case IDM_EVENT_RCAM_OFF:
        DoRcamEvent(dwCmd);
        break;
        
    case IDM_RGEAR_ON:
    case IDM_RGEAR_OFF:
        DoRgearOnoff(dwCmd);
        break;
        
    case IDM_WDT_ELAPSE:
        DoWdtElapse(dwCmd, pData, dwSize);
        break;
    case IDM_WDT_CLEAR:
        DoWdtClear(dwCmd);
        break;

    case IDM_TEST_ADC:
        DoTestAdc(dwCmd, pData, dwSize);
        break;
        
    case IDM_STEP_BUZZER:
        DoStepOps(dwCmd, pData, dwSize);
        break;
    case IDM_TEST_PAS_AUTOMOVE:
        DoTestPasAutoMove(dwCmd, pData, dwSize);
        break;
    case IDM_TEST_PAS_MANUALMOVE:
        if(m_nRcamState == eSTATE_RCAM_ON)
        {
            DoTestPasManualMove(dwCmd);
        }
        break;
    case IDM_TEST_OPS_AUTOMOVE:
        DoTestOpsAutoMove(dwCmd, pData, dwSize);
        break;
    case IDM_TEST_OPS_MANUALMOVE:
        if(m_nRcamState == eSTATE_RCAM_ON)
        {
            DoTestOpsManualMove(dwCmd, pData, dwSize);
        }
        break;
    case IDM_TEST_BUZZER_MUTE:
        DoTestBuzzerMute(dwCmd, pData, dwSize);
        break;
    default:
        LCAPI_Printf("(0x%X) Unknown Command 0x%X", m_dwID, dwCmd);
        break;
    }
}


//  Process
static void DoSuspend(LC_DWORD dwCmd)
{
    m_bSuspend = LC_TRUE;
    
    OnSuspend();
    
    PostSuspendDone(ID_MC);
}

static void DoResume(LC_DWORD dwCmd)
{
    m_bSuspend = LC_FALSE;
    
    OnResume();

    PostResumeDone(ID_MC);
}

static void DoPoweroff(LC_DWORD dwCmd)
{
    ProcSetOff();
}

static void DoOnTimer(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;

    switch(pdwBuffer[0])
    {
    case TID_POLL:
        ProcButton();
        ProcAcc();
        ProcDiag();
        ProcWdt();
        ProcIoIll();
        ProcIoRgear();
        ProcDebugLed();
        ProcErrorLed();
        ProcRgear();
        ProcBuzzer();
        ProcPasSim();
        ProcOpsSim();
        break;
        
    case TID_SETOFF:
        ProcSetOff();
        break;
        
    case TID_CHKOFF:
        ProcChkOff();
        break;
        
    case TID_AP:
        ProcAP();
        break;
        
    case TID_PSTATE:
        ProcPstate();
        break;
        
    case TID_AMP:
        ProcAmp();
        break;
        
    case TID_LCD:
        ProcLcd();
        break;
        
    case TID_FLED:
        ProcFrontLed();
        break;
        
    case TID_RCAM:
        ProcRcam();
        break;
    }
}

static LC_VOID DoCandataReceived(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
//        LCAPI_Putc(0x30);

    m_eCanState = STATE_CAN_RECEIVED;
}

static LC_VOID DoCandataTimeout(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    switch(m_eCanState)
    {
    case STATE_CAN_FUZZY:
        m_eCanState = STATE_CAN_NONE;
        break;
    case STATE_CAN_RECEIVED:
        m_eCanState = STATE_CAN_TIMEOUT;
        break;
    }
}

static LC_VOID DoCandataClosed(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    switch(m_eCanState)
    {
    case STATE_CAN_FUZZY:
        m_eCanState = STATE_CAN_NONE;
        break;
    case STATE_CAN_RECEIVED:
    case STATE_CAN_TIMEOUT:
        m_eCanState = STATE_CAN_CLOSED;
        break;
    }
}

static LC_VOID DoIll(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    
//    LCAPI_Putc(0x31);
    
    m_bCanIll = (LC_BOOL)(pBuffer[0] & 0x01);

    //  LED Value Set
    //  ILL ON
    if(m_bCanIll == LC_TRUE)
    {
//        m_nGoalFrontLed = FLED_ILL_ON_NAVARA;
        m_nGoalFrontLed = FLED_ILL_ON_SMART;
    }
    else
    {
//        m_nGoalFrontLed = FLED_ILL_OFF_NAVARA;
        m_nGoalFrontLed = FLED_ILL_OFF_SMART;
    }

    //  LED PWM
    LCAPI_SetLedBrightness(m_nGoalFrontLed, LC_FALSE);
    

    //  LCD
    if(m_bApBoot == LC_FALSE)
    {
        Proc1stLcdSet(m_bCanIll);
        
        if(m_nLcdState == eSTATE_LCD_ON)
            m_nLcdEvent = IDM_LCD_BRIGHTNESS;
    }
}

static void DoAmpEvent(LC_DWORD dwCmd)
{
    m_nAmpEvent = (LC_S32)dwCmd;
}

static void DoDiagEvent(LC_DWORD dwCmd)
{
    m_nDiagEvent = (LC_S32)dwCmd;
}

static void DoApsetMute(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;

    m_bApsetMute = (pBuffer[0] == 1) ? LC_TRUE : LC_FALSE;
        
    m_nAmpEvent = IDM_AMP_SET;
}

static void DoLcdBrightness(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    
    m_nSetPwm = (pBuffer[0] >= LCD_MIN_BRIGHT) ? (LC_S32)pBuffer[0] : LCD_MIN_BRIGHT;
    m_nGoalPwm = m_nSetPwm;
    
    m_nLcdEvent = dwCmd;
    
    // Save LCD Brightdata Value
    if(dwSize == 4)
    {
        m_dwCurrLcdSet = (LC_DWORD)m_nSetPwm;
        m_dwNightLcdSet = (LC_DWORD)pBuffer[1];
        m_dwDayLcdSet = (LC_DWORD)pBuffer[2];
        m_bAutoLcdSet = (LC_BOOL)pBuffer[3];

        LCAPI_SetLcdBrightData(m_dwCurrLcdSet, m_dwNightLcdSet, m_dwDayLcdSet, m_bAutoLcdSet);
    }
}

static void DoLcdOn(LC_DWORD dwCmd)
{
    m_nGoalPwm = m_nSetPwm;
    
    m_nLcdEvent = dwCmd;
}

static void DoLcdOff(LC_DWORD dwCmd)
{
    m_nGoalPwm = LCD_OFF_BRIGHT;
    
    m_nLcdEvent = dwCmd;
}

static void DoLcdCutoff(LC_DWORD dwCmd)
{
    m_nGoalPwm = LCD_OFF_BRIGHT;
    
    m_nLcdEvent = dwCmd;
}

static void DoFrontLedBrightness(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    if(pData != NULL)
    {
        LC_BYTE *pBuffer = (LC_BYTE *)pData;
//        m_nGoalFrontLed = (pBuffer[0] >= FLED_MIN_BRIGHT) ? (LC_S32)pBuffer[0] : FLED_MIN_BRIGHT;
        m_nGoalFrontLed = (LC_S32)pBuffer[0];
    }

    m_nFrontLedEvent = dwCmd;
}

static void DoFrontLedDim(LC_DWORD dwCmd)
{
    m_nFrontLedEvent = dwCmd;
}

static void DoRcamEvent(LC_DWORD dwCmd)
{
    m_nRcamEvent = (LC_S32)dwCmd;
}

static void DoRgearOnoff(LC_DWORD dwCmd)
{
    m_bCamRgear = (dwCmd == IDM_RGEAR_ON) ? LC_TRUE : LC_FALSE;
}

static void DoWdtElapse(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;

    m_nSetWdt = pdwBuffer[0] * 1000;
    m_nGoalWdt = m_nSetWdt;
    m_nCurrWdt = 0;
}

static void DoWdtClear(LC_DWORD dwCmd)
{
    m_nCurrWdt = 0;
}

static void DoTestAdc(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    
    m_bTestAdc = (LC_BOOL)pBuffer[0];
}

static void DoStepOps(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    LC_S32 nOpsVal;
    LC_S32 nStepBuzzer = m_nStepBuzzer;
    
    switch(m_eVehicleType)
    {
    case VEHICLE_VITO:
        nOpsVal = (pBuffer[0] < pBuffer[1]) ? pBuffer[0] : pBuffer[1];
        nOpsVal = (nOpsVal    < pBuffer[2]) ? nOpsVal    : pBuffer[2];
        nOpsVal = (nOpsVal    < pBuffer[3]) ? nOpsVal    : pBuffer[3];
            
        switch(nOpsVal)
        {
        case 0:
        case 1:
            nStepBuzzer = 0;
            break;
        case 2:
        case 3:
            nStepBuzzer = 1;
            break;
        case 4:
        case 5:
            nStepBuzzer = 2;
            break;
        case 6:
            nStepBuzzer = 3;
            break;
        default:
            nStepBuzzer = 4;
            break;
        }
        break;
        
    default:
        return;
    }


    if(m_nStepBuzzer != nStepBuzzer)
    {
        m_nStepBuzzer = nStepBuzzer;

        switch(m_nStepBuzzer)
        {
        case 0:
            m_nClrBuzzerPoint = 15;
            m_nLoopBuzzerPoint = 15;
            
            m_nCurrBuzzer = -1;
            break;
        case 1:
            m_nClrBuzzerPoint = 15;
            m_nLoopBuzzerPoint = 20;
            break;
        case 2:
            m_nClrBuzzerPoint = 15;
            m_nLoopBuzzerPoint = 40;
            break;
        case 3:
            m_nClrBuzzerPoint = 15;
            m_nLoopBuzzerPoint = 60;
            break;
        default:
            m_nLoopBuzzerPoint = 0;
            LCAPI_SetBuzzer(LC_FALSE);
            break;
        }
    }
}

static void DoTestPasAutoMove(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    
    m_bPasAuto = (LC_BOOL)pBuffer[0];
}

static void DoTestPasManualMove(LC_DWORD dwCmd)
{
    switch(m_eVehicleType)
    {
    case VEHICLE_SMART:
    case VEHICLE_VITO:
        if(m_dwPasDir == ePAS_DIR_LEFT)
        {
            if(--m_nPasVal <= 0)
            {
                m_nPasVal = 0;
                m_dwPasDir = ePAS_DIR_RIGHT;
            }
            LCAPI_PostMessage(ID_COMMAP, IDM_LOAD_PAS, &m_nPasVal, sizeof(LC_S32));
        }
        else if(m_dwPasDir == ePAS_DIR_RIGHT)
        {
            if(++m_nPasVal >= (PAS_PICTURE_MAX - 1))
            {
                m_nPasVal = PAS_PICTURE_MAX - 1;
                m_dwPasDir = ePAS_DIR_LEFT;
            }
            LCAPI_PostMessage(ID_COMMAP, IDM_LOAD_PAS, &m_nPasVal, sizeof(LC_S32));
        }
        break;
    }
}

static void DoTestOpsAutoMove(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    
    for(int i = 0; i < 3; i++)
    {
        m_abOpsAuto[i] = (LC_BOOL)((pBuffer[0] >> i) & 0x01);
    }
}

static void DoTestOpsManualMove(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    
    for(int i = 0; i < 4; i++)
    {
        if((pBuffer[0] >> i) & 0x01)
        {
            if(m_aOpsDir[i] == eOPS_DIR_FAR)
            {
                m_aOpsVal[i]++;
                if(m_aOpsVal[i] > OPS_FAR_MAX)
                {
                    m_aOpsVal[i] = OPS_OFF_VAL;
                    m_aOpsDir[i] = eOPS_DIR_NEAR;
                }
                LCAPI_PostMessage(ID_COMMAP, IDM_LOAD_OPS, m_aOpsVal, 4);
            }
            else if(m_aOpsDir[i] == eOPS_DIR_NEAR)
            {
                if(m_aOpsVal[i] == OPS_OFF_VAL)
                {
                    m_aOpsVal[i] = OPS_FAR_MAX;
                }
                else if(m_aOpsVal[i] == 0)
                {
                    m_aOpsVal[i] = 1;
                    m_aOpsDir[i] = eOPS_DIR_FAR;
                }
                else
                {
                    m_aOpsVal[i]--;
                }
                
                LCAPI_PostMessage(ID_COMMAP, IDM_LOAD_OPS, m_aOpsVal, 4);
            }
        }
    }

    //  Check Buzzer Value
    DoStepOps(IDM_STEP_BUZZER, m_aOpsVal, 4);
}

static void DoTestBuzzerMute(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;

    m_bMuteBuzzer = (LC_BOOL)pBuffer[0];
    if(m_bMuteBuzzer == LC_TRUE)
    {
        LCAPI_SetBuzzer(LC_FALSE);
    }
    else
    {
        if(m_nStepBuzzer == 0)
            LCAPI_SetBuzzer(LC_TRUE);
    }
}


//  Send
static void PostSuspendDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_SUSPEND, &m_dwID, sizeof(LC_DWORD));
}

static void PostResumeDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_RESUME, &m_dwID, sizeof(LC_DWORD));
}

static LC_VOID PostOffReady(LC_DWORD dwID, LC_BOOL bReady)
{
    LCAPI_PostMessage(dwID, IDM_OFFREADY, &bReady, sizeof(LC_BOOL));
}

static void PostReqOsTime(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_REQ_OS_TIME, NULL, 0);
}

static LC_VOID PostReqOff(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_REQOFF, NULL, 0);
}


//
static void ProcSetOff()
{
    LCAPI_KillTimer(m_dwID, TID_SETOFF);
    
    LCAPI_PowerOnoff(LC_FALSE);
    
    LCAPI_SetTimer(m_dwID, TID_CHKOFF, CHKOFF_ELAPSE);
}

static void ProcChkOff()
{
    LCAPI_ResetMcu(0);
}

//
static void ProcButton()
{
    if(m_bTestAdc == LC_TRUE)
    {
        static LC_DWORD dwStep = 0;
        static LC_DWORD dwPrevBtn = 0, dwCurrBtn = 0;
        LC_WORD wAdValue;
        
        if((++dwStep) & 0x01) return;
        
        if(LCDRV_Ioctl(*(g_phDevice + DEV_ADC), IOCTL_READ_BUTTON, &dwStep, sizeof(LC_DWORD), &wAdValue, sizeof(LC_WORD)) <= 0)
        {
            wAdValue = 0xFFFF;
        }

        switch(dwStep)
        {
        case 2:
            if(wAdValue <= 0xC0)
            {
                dwCurrBtn |= (1 << 5);
            }
    //        return;
            break;
            
        case 4:
            //  0x000
            if(wAdValue <= 0xC0)
            {
                dwCurrBtn |= (1 << 0);
            }
            //  0x305
            else if(wAdValue >= 0x245 && wAdValue <= 0x3C5)
            {
                dwCurrBtn |= (1 << 1);
            }
            //  0x724
            else if(wAdValue >= 0x664 && wAdValue <= 0x7E4)
            {
                dwCurrBtn |= (1 << 2);
            }
            //  0xB61
            else if(wAdValue >= 0xAA1 && wAdValue <= 0xC21)
            {
                dwCurrBtn |= (1 << 3);
            }
            //  0xDE8
            else if(wAdValue >= 0xD28 && wAdValue <= 0xEA8)
            {
                dwCurrBtn |= (1 << 4);
            }
            break;
            
        default:
            return;
        }

        //  for Mr. YOO
        LC_DWORD dwAdValue = wAdValue;
        static LC_DWORD dwPrev2 = 0xFFFFFFFF, dwPrev4 = 0xFFFFFFFF;
        LC_DWORD dwLimitL;

        switch(dwStep)
        {
        case 2:
            if(dwPrev2 == 0xFFFFFFFF)
            {
                dwPrev2 = dwAdValue;
            }
            else
            {
                dwLimitL = (dwPrev2 > 64) ? dwPrev2 - 64 : 0;
                if((dwAdValue < dwLimitL) || (dwAdValue > dwPrev2 + 64))
                {
                    int nVoltage = (dwAdValue * 330) / 0xFFF;
                    LCAPI_Printf("BTN-1 : %d.%02d V", nVoltage / 100, nVoltage % 100);
                    dwPrev2 = dwAdValue;
                }
            }
            return;
            
        case 4:
            if(dwPrev4 == 0xFFFFFFFF)
            {
                dwPrev4 = dwAdValue;
            }
            else
            {
                dwLimitL = (dwPrev4 > 64) ? dwPrev4 - 64 : 0;
                if((dwAdValue < dwLimitL) != (dwAdValue > dwPrev4 + 64))
                {
                    int nVoltage = (dwAdValue * 330) / 0xFFF;
                    LCAPI_Printf("BTN-2 : %d.%02d V", nVoltage / 100, nVoltage % 100);
                    dwPrev4 = dwAdValue;
                }
            }
            break;
        }

        // Send
        if(dwPrevBtn != dwCurrBtn)
        {
            LCAPI_PostMessage(ID_COMMAP, IDM_SEND_BUTTON, &dwCurrBtn, sizeof(LC_DWORD));
            dwPrevBtn = dwCurrBtn;
        }
        dwStep = 0;
        dwCurrBtn = 0;
    }
}

//  Run 1st time this Functions : act ACC OFF
static void ProcAcc()
{
    LC_BOOL bAcc;
    static LC_S32 nOnCount = 0, nOffCount = 0;
   
    bAcc = LCAPI_GetAcc();
    
    if(bAcc == LC_TRUE)
    {
        if(nOnCount < ACC_ON_CONFIRM)
        {
            if(++nOnCount >= ACC_ON_CONFIRM)
            {
//                LCAPI_Putc(0x8F);
                
                m_ePowerState = STATE_POWER_0;
                
                CommandProcess(IDM_AMP_ON, NULL, 0);
                CommandProcess(IDM_LCD_ON, NULL, 0);
                
                PostOffReady(ID_COMMAP, LC_FALSE);
            }
        }
        
        IF_NEQ_SET_EQ(nOffCount, 0);
    }
    else
    {
        static LC_BOOL bPrevPoff = LC_FALSE;
        LC_BOOL bCurrPoff = LC_FALSE;

        if(nOffCount < ACC_OFF_POWEROFF)
        {
            nOffCount++;
            if(nOffCount == ACC_OFF_CONFIRM)
            {
//                LCAPI_Putc(0x8E);

                m_ePowerState = STATE_POWER_1;
                
                CommandProcess(IDM_AMP_OFF, NULL, 0);
                CommandProcess(IDM_LCD_OFF, NULL, 0);

                PostOffReady(ID_COMMAP, LC_TRUE);
            }
            else if(nOffCount == ACC_OFF_REQ_OSTIME)
            {
                PostReqOsTime(ID_COMMAP);
            }
        }
        else
        {
            switch(m_eCanState)
            {
            case STATE_CAN_FUZZY:
            case STATE_CAN_NONE:
            case STATE_CAN_CLOSED:
                bCurrPoff = LC_TRUE;
                break;
            }
        }

        IF_NEQ_SET_EQ(nOnCount, 0);
        
        //  Proc Power off
        if(bPrevPoff != bCurrPoff)
        {
            bPrevPoff = bCurrPoff;
            if(bCurrPoff == LC_TRUE)
            {
                m_ePowerState = STATE_POWER_2;

                LCAPI_SetPhyStb(LC_FALSE);
                
                LCAPI_KillTimer(m_dwID, TID_AP);
                LCAPI_KillTimer(m_dwID, TID_AMP);
                LCAPI_KillTimer(m_dwID, TID_LCD);
                LCAPI_SetTimer(m_dwID, TID_SETOFF, SETOFF_ELAPSE);
            
                PostReqOff(ID_COMMAP);
            }
        }
    }
    
    //  Battery Value
    static LC_DWORD dwStep = 0;
    LC_DWORD dwBattery;

    if(++dwStep & 0x01) return;
    
    if(LCDRV_Ioctl(*(g_phDevice + DEV_ADC), IOCTL_READ_BATTERY, &dwStep, sizeof(LC_DWORD), &dwBattery, sizeof(LC_DWORD)) <= 0)
    {
        dwBattery = 0xFFFF;
    }

    if(dwStep >= 4)
    {
        dwStep = 0;

        //  for Mr. YOO
        if(m_bTestAdc == LC_TRUE)
        {
            static LC_DWORD dwPrev = 0xFFFFFFFF;
            
            if(dwPrev == 0xFFFFFFFF)
            {
                dwPrev = dwBattery;
            }
            else
            {
                LC_DWORD dwLimitL = (dwPrev > 64) ? dwPrev - 64 : 0;
                
                if((dwBattery < dwLimitL) || (dwBattery > dwPrev + 64))
                {
                    int nVoltage = (dwBattery * 330) / 0xFFF;
                    LCAPI_Printf("BAT-0 : %d.%02d V", nVoltage / 100, nVoltage % 100);
                    dwPrev = dwBattery;
                }
            }
        }
    }
}

static void ProcDiag()
{
    LC_BOOL bDiag;
    static LC_S32 nSetCount = 0, nRelCount = 0;
    
    bDiag = LCAPI_GetAmpDiag();
    if(bDiag == LC_TRUE)
    {
        if(nSetCount < DIAG_SET_CONFIRM)
        {
            if(++nSetCount >= DIAG_SET_CONFIRM)
            {
                CommandProcess(IDM_DIAG_ON, NULL, 0);
            }
        }
        
        IF_NEQ_SET_EQ(nRelCount, 0);
    }
    else
    {
        if(nRelCount < DIAG_REL_CONFIRM)
        {
            if(++nRelCount >= DIAG_REL_CONFIRM)
            {
                CommandProcess(IDM_DIAG_OFF, NULL, 0);
            }
        }
        
        IF_NEQ_SET_EQ(nSetCount, 0);
    }
}    

static void ProcWdt()
{
    if(m_nGoalWdt > 0)
    {
        m_nCurrWdt += POLL_ELAPSE;
        if(m_nCurrWdt >= m_nGoalWdt)
        {
            LCAPI_ResetMcu(1);
        }
    }
}

static LC_VOID ProcIoIll()
{
    LC_BOOL bIll;
    LC_BOOL bIoLcdSet = LC_FALSE;
    static LC_S32 nLoop = 0;
    static LC_S32 nSetCount = 0, nRelCount = 0;
    
    if(++nLoop < IO_ILL_LOOP_MAX) return;
    nLoop = 0;

    bIll = LCAPI_GetIoIll();
    m_bIoIll = bIll;
    
    if(m_eCanState == STATE_CAN_FUZZY) return;
    
    if(bIll == LC_TRUE)
    {
        if(nSetCount < IO_ILL_SET_CONFIRM)
        {
            if(++nSetCount >= IO_ILL_SET_CONFIRM)
            {
                if(m_eCanState == STATE_CAN_NONE)
                {
                    bIoLcdSet = LC_TRUE;
                    LCAPI_PostMessage(ID_COMMAP, IDM_CAN_ILL, &bIll, sizeof(LC_DWORD));
                    CommandProcess(IDM_ILL, &bIll, sizeof(LC_BYTE));
                }
            }
        }
        
        IF_NEQ_SET_EQ(nRelCount, 0);
    }
    else
    {
        if(nRelCount < IO_ILL_REL_CONFIRM)
        {
            if(++nRelCount >= IO_ILL_REL_CONFIRM)
            {
                if(m_eCanState == STATE_CAN_NONE)
                {
                    bIoLcdSet = LC_TRUE;
                    LCAPI_PostMessage(ID_COMMAP, IDM_CAN_ILL, &bIll, sizeof(LC_DWORD));
                    CommandProcess(IDM_ILL, &bIll, sizeof(LC_BYTE));
                }
            }
        }
        
        IF_NEQ_SET_EQ(nSetCount, 0);
    }
    
    
    //  LCD
    if(bIoLcdSet == LC_TRUE && m_bApBoot == LC_FALSE)
    {
        Proc1stLcdSet(m_bIoIll);

        if(m_nLcdState == eSTATE_LCD_ON)
            m_nLcdEvent = IDM_LCD_BRIGHTNESS;
    }
}

static LC_VOID ProcIoRgear()
{
    LC_BOOL bRgear;
    static LC_S32 nLoop = 0;
    static LC_S32 nSetCount = 0, nRelCount = IO_RGEAR_REL_CONFIRM;

    static LC_S32 nApDelay = 300;               // 600 => 300
    if(nApDelay > 0)
    {
        nApDelay--;
        return;
    }
    
    if(m_eCanState == STATE_CAN_FUZZY) return;
    if(++nLoop < IO_RGEAR_LOOP_MAX) return;
    nLoop = 0;
    
    bRgear = LCAPI_GetIoRgear();
    if(bRgear == LC_TRUE)
    {
        if(nSetCount < IO_RGEAR_SET_CONFIRM)
        {
            if(++nSetCount >= IO_RGEAR_SET_CONFIRM)
            {
                if(m_eCanState == STATE_CAN_NONE)
                {
                    LCAPI_PostMessage(ID_COMMAP, IDM_CAN_GEAR, &bRgear, sizeof(LC_DWORD));
                    CommandProcess(IDM_EVENT_RCAM_ON, NULL, 0);
                }
            }
        }
        
        IF_NEQ_SET_EQ(nRelCount, 0);
    }
    else
    {
        if(nRelCount < IO_RGEAR_REL_CONFIRM)
        {
            if(++nRelCount >= IO_RGEAR_REL_CONFIRM)
            {
                if(m_eCanState == STATE_CAN_NONE)
                {
///                    m_nTestTick1 = HAL_GetTick();
                    
                    LCAPI_PostMessage(ID_COMMAP, IDM_CAN_GEAR, &bRgear, sizeof(LC_DWORD));
                    CommandProcess(IDM_EVENT_RCAM_OFF, NULL, 0);
                }
            }
        }
        
        IF_NEQ_SET_EQ(nSetCount, 0);
    }
}

static LC_VOID ProcDebugLed()
{
    static LC_S32 nPrevElapse = 0;
    LC_S32 nCurrElapse;
    
    switch(m_ePowerState)
    {
    case STATE_POWER_FUZZY:
    case STATE_POWER_0:
#ifdef APP_PROGRAM
        if(m_eCanState == STATE_CAN_RECEIVED)
            nCurrElapse = LED_CAN_ELAPSE;
        else
            nCurrElapse = LED_CANLESS_ELAPSE;
#else
        nCurrElapse = LED_FACTORY_ELAPSE;
#endif
        break;
        
    case STATE_POWER_1:
        nCurrElapse = LED_ACCOFF_ELAPSE;
        break;
        
    case STATE_POWER_2:
        nCurrElapse = LED_POWEROFF_ELAPSE;
        break;
    }

    if(nPrevElapse != nCurrElapse)
    {
        nPrevElapse = nCurrElapse;
        LCAPI_SetDebugLed(nCurrElapse);
    }
    
    LCAPI_RunDebugLed();
}

static LC_VOID ProcErrorLed()
{
    static LC_S32 nPrevElapse = 0;
    LC_S32 nCurrElapse;
    
    switch(m_eCanState)
    {
    case STATE_CAN_TIMEOUT:
        nCurrElapse = LED_ERROR_CAN_TIMEOUT;
        break;
    case STATE_CAN_CLOSED:
        nCurrElapse = LED_ERROR_CAN_CLOSED;
        break;
    default:
        nCurrElapse = LED_ERROR_NONE;
        break;
    }
    
    if(nPrevElapse != nCurrElapse)
    {
        nPrevElapse = nCurrElapse;
        LCAPI_SetErrorLed(nCurrElapse);
    }
    
    LCAPI_RunErrorLed();
}

static void ProcRgear()
{
    static LC_S32 nOnCount = 0, nOffCount = RGEAR_OFF_CONFIRM;
    
    static LC_S32 nApDelay = 300;               // 600 => 300
    if(nApDelay > 0)
    {
        nApDelay--;
        return;
    }
   
    if(m_bCamRgear == LC_TRUE)
    {
        if(nOnCount < RGEAR_ON_CONFIRM)
        {
            if(++nOnCount >= RGEAR_ON_CONFIRM)
            {
                CommandProcess(IDM_EVENT_RCAM_ON, NULL, 0);
            }
        }
        IF_NEQ_SET_EQ(nOffCount, 0);
    }
    else
    {
        if(nOffCount < RGEAR_OFF_CONFIRM)
        {
            if(++nOffCount >= RGEAR_OFF_CONFIRM)
            {
                CommandProcess(IDM_EVENT_RCAM_OFF, NULL, 0);
            }
        }
        IF_NEQ_SET_EQ(nOnCount, 0);
    }
}

static void ProcBuzzer()
{
    if(m_nRcamState != eSTATE_RCAM_ON) return;
    if(m_nLoopBuzzerPoint == 0) return;

    m_nCurrBuzzer++;

    if(m_nCurrBuzzer == 0)
    {
        if(m_bMuteBuzzer == LC_FALSE)
            LCAPI_SetBuzzer(LC_TRUE);
    }
    else
    {
        if(m_nClrBuzzerPoint < m_nLoopBuzzerPoint)
        {
            if(m_nCurrBuzzer == m_nClrBuzzerPoint)
            {
                LCAPI_SetBuzzer(LC_FALSE);
            }
            else if(m_nCurrBuzzer >= m_nLoopBuzzerPoint)
            {
                m_nCurrBuzzer = -1;
            }
        }
    }
}
    
static void ProcPasSim()
{
    if(m_nRcamState != eSTATE_RCAM_ON) return;

    switch(m_eVehicleType)
    {
    case VEHICLE_NAVARA:
        return;
    }

    static int nCount;

    nCount++;
    
    if((nCount & 0x07) == 0)
    {
        if(m_bPasAuto == LC_TRUE)
        {
            DoTestPasManualMove(IDM_TEST_PAS_MANUALMOVE);
        }
    }
}

static void ProcOpsSim()
{
    if(m_nRcamState != eSTATE_RCAM_ON) return;

    switch(m_eVehicleType)
    {
    case VEHICLE_NAVARA:
    case VEHICLE_SMART:
        return;
    }
    
    static int nCount;
    LC_BYTE Buffer[4];

    nCount++;
    
    if((nCount & 0x03) != 0) return;
    
    int nPos = (nCount >> 2) & 0x03;

    if(m_abOpsAuto[nPos] == LC_TRUE)
    {
        Buffer[0] = 1 << nPos;
        DoTestOpsManualMove(IDM_TEST_OPS_MANUALMOVE, Buffer, 1);
    }
}

static void ProcAP()
{
    LCAPI_KillTimer(m_dwID, TID_AP);
    
    //  AP Release
    LCAPI_ResetAP(LC_FALSE);
    
    LCAPI_SetTimer(m_dwID, TID_LCD, 1200);         // 1 => 500msec for Erase Afterimage
    LCAPI_SetTimer(m_dwID, TID_RCAM, 10);
    
    LCAPI_Putc(0x90);
}

static void ProcPstate()
{
}

static void ProcAmp()
{
    LC_WORD wStatus;
    
    switch(m_nAmpState)
    {
    case eSTATE_AMP_OFF:
        switch(m_nDiagEvent)
        {
        case IDM_DIAG_ON:
            PROC_AMP_STATE(eSTATE_DIAG_OFF2, 1, 0xA0);
            break;
        case IDM_DIAG_OFF:
        default:
            switch(m_nAmpEvent)
            {
            case IDM_AMP_ON:
                PROC_AMP_STATE(eSTATE_MUTE_ENABLE, 1, 0xA1);
                break;
            case IDM_AMP_OFF:
            case IDM_AMP_SET:
                wStatus = 0;                    // Disable & Mute
                LCAPI_PostMessage(ID_COMMAP, IDM_AMP_STATUS, &wStatus, sizeof(LC_WORD));
//                break;
            default:
                PROC_AMP_STATE(eSTATE_AMP_OFF, 10, 0x00);
                break;
            }
            IF_NEQ_SET_EQ(m_nAmpEvent, 0);
            break;
        }
        IF_NEQ_SET_EQ(m_nDiagEvent, 0);
        break;

    case eSTATE_MUTE_ENABLE:
        LCAPI_SetAmpMute(LC_TRUE);              //  MUTE ON
        PROC_AMP_STATE(eSTATE_AMP_ENABLE, 10, 0xA2);
        break;

    case eSTATE_AMP_ENABLE:
        switch(m_nDiagEvent)
        {
        case IDM_DIAG_ON:
            PROC_AMP_STATE(eSTATE_DIAG_OFF, 1, 0xA3);
            break;
        case IDM_DIAG_OFF:
        default:
            LCAPI_SetAmpOnoff(LC_TRUE);         //  AMP Enable
            PROC_AMP_STATE(eSTATE_MUTESET, 1, 0xA4);
            break;
        }
        IF_NEQ_SET_EQ(m_nDiagEvent, 0);
        break;
        
    case eSTATE_MUTESET:
        switch(m_nDiagEvent)
        {
        case IDM_DIAG_ON:
            PROC_AMP_STATE(eSTATE_MUTE_DIAG, 1, 0xA6);
            break;
        case IDM_DIAG_OFF:
        default:
            LCAPI_SetAmpMute(m_bApsetMute);     //  MUTE Value
            PROC_AMP_STATE(eSTATE_AMP_ON, 1, 0xA5);
            
            wStatus = 0x100 | m_bApsetMute;     //  Enable | MUTE VAL
            LCAPI_PostMessage(ID_COMMAP, IDM_AMP_STATUS, &wStatus, sizeof(LC_WORD));
            break;
        }
        IF_NEQ_SET_EQ(m_nDiagEvent, 0);
        break;
        
    case eSTATE_AMP_ON:
        switch(m_nDiagEvent)
        {
        case IDM_DIAG_ON:
            PROC_AMP_STATE(eSTATE_MUTE_DIAG, 1, 0xA9);
            break;
        case IDM_DIAG_OFF:
        default:
            switch(m_nAmpEvent)
            {
            case IDM_AMP_OFF:
                PROC_AMP_STATE(eSTATE_MUTE_DISABLE, 10, 0xA7);
                break;
            case IDM_AMP_SET:
                PROC_AMP_STATE(eSTATE_MUTESET, 1, 0xA8);
                break;
            case IDM_AMP_ON:
            default:
                PROC_AMP_STATE(eSTATE_AMP_ON, 10, 0x00);
                break;
            }
            IF_NEQ_SET_EQ(m_nAmpEvent, 0);
            break;
        }
        IF_NEQ_SET_EQ(m_nDiagEvent, 0);
        break;

    case eSTATE_MUTE_DISABLE:
        LCAPI_SetAmpMute(LC_TRUE);              //  MUTE ON
        PROC_AMP_STATE(eSTATE_AMP_DISABLE, 10, 0xAA);
        break;
        
    case eSTATE_AMP_DISABLE:
        LCAPI_SetAmpOnoff(LC_FALSE);            //  AMP Disable
        PROC_AMP_STATE(eSTATE_AMP_OFF, 1, 0xAB);
        
        wStatus = 0;                            //  Disable & Mute
        LCAPI_PostMessage(ID_COMMAP, IDM_AMP_STATUS, &wStatus, sizeof(LC_WORD));
        break;

    case eSTATE_MUTE_DIAG:
        LCAPI_SetAmpMute(LC_TRUE);              //  MUTE ON
        PROC_AMP_STATE(eSTATE_AMP_DIAG, 10, 0xAC);
        break;
        
    case eSTATE_AMP_DIAG:
        LCAPI_SetAmpOnoff(LC_FALSE);            //  MUTE Disable
        PROC_AMP_STATE(eSTATE_DIAG_OFF, 1, 0xAD);

        wStatus = 0;                            //  Disable & Mute
        LCAPI_PostMessage(ID_COMMAP, IDM_AMP_STATUS, &wStatus, sizeof(LC_WORD));
        break;
        
    case eSTATE_DIAG_OFF:
        switch(m_nDiagEvent)
        {
        case IDM_DIAG_OFF:
            PROC_AMP_STATE(eSTATE_AMP_ENABLE, 1, 0xAE);
            break;
        case IDM_DIAG_ON:
        default:
            switch(m_nAmpEvent)
            {
            case IDM_AMP_OFF:
                PROC_AMP_STATE(eSTATE_DIAG_OFF2, 1, 0xAE);
                break;
            case IDM_AMP_ON:
            case IDM_AMP_SET:
                wStatus = 0;                    // Disable & Mute
                LCAPI_PostMessage(ID_COMMAP, IDM_AMP_STATUS, &wStatus, sizeof(LC_WORD));
//                break;
            default:
                PROC_AMP_STATE(eSTATE_DIAG_OFF, 10, 0x00);
                break;
            }
            IF_NEQ_SET_EQ(m_nAmpEvent, 0);
            break;
        }

        IF_NEQ_SET_EQ(m_nDiagEvent, 0);
        break;
        
    case eSTATE_DIAG_OFF2:
        switch(m_nDiagEvent)
        {
        case IDM_DIAG_OFF:
            PROC_AMP_STATE(eSTATE_AMP_OFF, 1, 0xAF);
            break;
        case IDM_DIAG_ON:
        default:
            switch(m_nAmpEvent)
            {
            case IDM_AMP_ON:
                PROC_AMP_STATE(eSTATE_DIAG_OFF, 1, 0xAE);
                break;
            case IDM_AMP_OFF:
            case IDM_AMP_SET:
                wStatus = 0;                    // Disable & Mute
                LCAPI_PostMessage(ID_COMMAP, IDM_AMP_STATUS, &wStatus, sizeof(LC_WORD));
//                break;
            default:
                PROC_AMP_STATE(eSTATE_DIAG_OFF2, 10, 0x00);
                break;
            }
            IF_NEQ_SET_EQ(m_nAmpEvent, 0);
            break;
        }

        IF_NEQ_SET_EQ(m_nDiagEvent, 0);
        break;
    }
}

static void ProcLcd()
{
    LC_WORD wStatus;
    
    switch(m_nLcdState)
    {
    case eSTATE_LCD_INIT:
        Proc1stLcdData();
        PROC_LCD_STATE(eSTATE_LCD_OFF, 1, 0xB0);
        break;
    case eSTATE_LCD_OFF:
        switch(m_nLcdEvent)
        {
        case IDM_LCD_ON:
            PROC_LCD_STATE(eSTATE_LCD_PON, 1, 0xB0);
            break;
        case IDM_LCD_OFF:
        case IDM_LCD_CUTOFF:
        case IDM_LCD_BRIGHTNESS:
            wStatus = m_nCurrPwm;
            LCAPI_PostMessage(ID_COMMAP, IDM_LCD_STATUS, &wStatus, sizeof(LC_WORD));
//            break;
        default:
            PROC_LCD_STATE(eSTATE_LCD_OFF, 10, 0x00);
            break;
        }
        IF_NEQ_SET_EQ(m_nLcdEvent, 0);
        
        //  Led Check
        switch(m_nFrontLedEvent)
        {
        case IDM_FLED_DIM:
            LCAPI_SetLedBrightness(FLED_MIN_BRIGHT, LC_FALSE);
            break;
        }
        IF_NEQ_SET_EQ(m_nFrontLedEvent, 0);
        break;

    case eSTATE_LCD_PON:
        LCAPI_SetLcdOnoff(LC_TRUE);             // LCD POWER-ON
        PROC_LCD_STATE(eSTATE_BL_EN, 300, 0xB1);
        break;

    case eSTATE_BL_EN:
        LCAPI_SetBlOnoff(LC_TRUE);              // Backlight Enable
//        PROC_LCD_STATE(eSTATE_PWM_EN, 1, 0xB2);
        PROC_LCD_STATE(eSTATE_PWM_EN, 10, 0xB2);
        break;

    case eSTATE_PWM_EN:
        LCAPI_SetLcdBrightness(LCD_MIN_BRIGHT, LC_TRUE);
///        LCAPI_SetLedBrightness(FLED_MIN_BRIGHT, LC_FALSE);
        
        m_nGoalPwm = m_nSetPwm;
        PROC_LCD_STATE(eSTATE_BRIGHTNESS_SET, 10, 0xB3);
        break;
 
    case eSTATE_BRIGHTNESS_SET:
        if(ProcBright() == LC_TRUE)
        {
            PROC_LCD_STATE (eSTATE_LCD_ON, 1, 0xB4);
            PROC_FLED_STATE(eSTATE_LCD_ON, 1000, 0x00);

            wStatus = 0x100 | m_nCurrPwm;       //  LCD Enable, LCD Brightness
            LCAPI_PostMessage(ID_COMMAP, IDM_LCD_STATUS, &wStatus, sizeof(LC_WORD));
        }
        else
        {
            PROC_LCD_STATE(eSTATE_BRIGHTNESS_SET, 10, 0xB5);
        }
        break;
 
    case eSTATE_LCD_ON:
        switch(m_nLcdEvent)
        {
        case IDM_LCD_OFF:
            PROC_LCD_STATE(eSTATE_BRIGHTNESS_FADEOUT, 1, 0xB6);
            break;
        case IDM_LCD_CUTOFF:
           m_nCurrPwm = LCD_MIN_BRIGHT;
            PROC_LCD_STATE(eSTATE_BRIGHTNESS_FADEOUT, 1, 0xB6);
            break;
        case IDM_LCD_BRIGHTNESS:
            PROC_LCD_STATE(eSTATE_BRIGHTNESS_SET, 1, 0xB7);
            break;
        default:
            PROC_LCD_STATE(eSTATE_LCD_ON, 10, 0x00);
            break;
        }
        IF_NEQ_SET_EQ(m_nLcdEvent, 0);
        
        switch(m_nFrontLedEvent)
        {
        case IDM_FLED_BRIGHTNESS:
            //  LED PWM
            LCAPI_SetLedBrightness(m_nGoalFrontLed, LC_FALSE);
            break;
        }
        IF_NEQ_SET_EQ(m_nFrontLedEvent, 0);
        break;
  
    case eSTATE_BRIGHTNESS_FADEOUT:
        if(ProcBright() == LC_TRUE)
        {
            PROC_LCD_STATE(eSTATE_PWM_DIS, 1, 0xB8);
        }
        else
        {
            PROC_LCD_STATE(eSTATE_BRIGHTNESS_FADEOUT, 10, 0xB9);
        }
        break;
        
    case eSTATE_PWM_DIS:
        LCAPI_SetLcdBrightness(0, LC_TRUE);
///        LCAPI_SetLedBrightness(FLED_MIN_BRIGHT, LC_FALSE);
        
        PROC_LCD_STATE(eSTATE_BL_DIS, 1, 0xBA);
        break;

    case eSTATE_BL_DIS:
        LCAPI_SetBlOnoff(LC_FALSE);             // Backlight Disable
        PROC_LCD_STATE(eSTATE_LCD_POFF, 1, 0xBB);
  
///        m_nTestTick3 = HAL_GetTick();
        
        wStatus = m_nCurrPwm;
        LCAPI_PostMessage(ID_COMMAP, IDM_LCD_STATUS, &wStatus, sizeof(LC_WORD));
        break;
        
    case eSTATE_LCD_POFF:
        LCAPI_SetLcdOnoff(LC_FALSE);            //  POWER-OFF
        PROC_LCD_STATE (eSTATE_LCD_OFF, 1, 0xBC);
        PROC_FLED_STATE(eSTATE_LCD_OFF, 1000, 0x00);
        break;
    }
}

static LC_VOID Proc1stLcdData()
{
    LC_DWORD dwReadData;
    LC_DWORD dwCurr, dwNight, dwDay, dwAuto;

    dwReadData = LCAPI_GetLcdBrightData();

    //  Check Data
    if(dwReadData == 0) return;
    dwCurr = (dwReadData) & 0x7F;
    if(!(dwCurr >= LCD_MIN_BRIGHT && dwCurr <= 100)) return;
    dwNight = (dwReadData >> 8) & 0x7F;
    if(!(dwNight >= LCD_MIN_BRIGHT && dwNight <= 100)) return;
    dwDay = (dwReadData >> 16) & 0x7F;
    if(!(dwDay >= LCD_MIN_BRIGHT && dwDay <= 100)) return;
    dwAuto = (dwReadData >> 24) & 0x7F;
    if(!(dwAuto == 0 || dwAuto == 1)) return;
    
    m_dwCurrLcdSet = dwCurr;
    m_dwNightLcdSet = dwNight;
    m_dwDayLcdSet = dwDay;
    m_bAutoLcdSet = (LC_BOOL)dwAuto;
    
    //  LCD
    LC_BOOL bIll;
    switch(m_eCanState)
    {
    case STATE_CAN_RECEIVED:
        bIll = m_bCanIll;
        break;
    default:
        bIll = m_bIoIll;
        break;
    }
    
    Proc1stLcdSet(bIll);
}

static LC_VOID Proc1stLcdSet(LC_BOOL bIll)
{
    if(m_bAutoLcdSet == LC_TRUE)
    {
        if(bIll == LC_TRUE)
        {
            m_nGoalPwm = (LC_S32)m_dwNightLcdSet;
        }
        else
        {
            m_nGoalPwm = (LC_S32)m_dwDayLcdSet;
        }
    }
    else
    {
        m_nGoalPwm = (LC_S32)m_dwCurrLcdSet;
    }
    m_nSetPwm = m_nGoalPwm;
}

static LC_BOOL ProcBright()
{
    if(m_nCurrPwm < m_nGoalPwm)
    {
        m_nCurrPwm += 2;
        if(m_nCurrPwm > m_nGoalPwm) m_nCurrPwm = m_nGoalPwm;
        
        //  LCD PWM
        LCAPI_SetLcdBrightness(m_nCurrPwm, LC_FALSE);
        
        LCAPI_AmpLcd_Putc(m_nCurrPwm);
    }
    else if(m_nCurrPwm > m_nGoalPwm)
    {
        m_nCurrPwm -= 2;
        if(m_nCurrPwm < m_nGoalPwm) m_nCurrPwm = m_nGoalPwm;
        
        //  LCD PWM
        LCAPI_SetLcdBrightness(m_nCurrPwm, LC_FALSE);
        
        LCAPI_AmpLcd_Putc(m_nCurrPwm);
    }
    
    if(m_nCurrPwm == m_nGoalPwm)
    {
///        LCAPI_Putc(m_nGoalFrontLed);
        
        return LC_TRUE;
    }
    else
        return LC_FALSE;
}

static void ProcFrontLed()
{
    //  Not Polling, Only one time
    LCAPI_KillTimer(m_dwID, TID_FLED);
    
    switch(m_nFrontLedState)
    {
    case eSTATE_LCD_OFF:
        CommandProcess(IDM_FLED_DIM, NULL, 0);
        break;
    case eSTATE_LCD_ON:
        CommandProcess(IDM_FLED_BRIGHTNESS, NULL, 0);
        break;
    }
}

static void ProcRcam()
{
    LC_WORD wStatus;

    switch(m_nRcamState)
    {
    case eSTATE_RCAM_OFF:
        switch(m_nRcamEvent)
        {
        case IDM_EVENT_RCAM_ON:
            PROC_RCAM_STATE(eSTATE_RGEAR_CONFIRM, 1, 0xC0);
            break;
        case IDM_EVENT_RCAM_OFF:
        default:
            PROC_RCAM_STATE(eSTATE_RCAM_OFF, 10, 0x00);
            break;
        }
        IF_NEQ_SET_EQ(m_nRcamEvent, 0);
        break;
        
    case eSTATE_RGEAR_CONFIRM:
        switch(m_nRcamEvent)
        {
        case IDM_EVENT_RCAM_OFF:
            PROC_RCAM_STATE(eSTATE_RCAM_OFF, 1, 0xC1);
            break;
        case IDM_EVENT_RCAM_ON:
        default:
            CommandProcess(IDM_LCD_CUTOFF, NULL, 0);
            PROC_RCAM_STATE(eSTATE_RGEAR_APSIG, 250, 0xC2);
            break;
        }
        IF_NEQ_SET_EQ(m_nRcamEvent, 0);
        break;
        
    case eSTATE_RGEAR_APSIG:
        switch(m_nRcamEvent)
        {
        case IDM_EVENT_RCAM_OFF:
            PROC_RCAM_STATE(eSTATE_XGEAR_APDISP, 1, 0xC3);
            break;
        case IDM_EVENT_RCAM_ON:
        default:
            wStatus = 1;                        //  R-CAM ON
            LCAPI_PostMessage(ID_COMMAP, IDM_RCAM_STATUS, &wStatus, sizeof(LC_WORD));

            LCAPI_SetRcamOnoff(LC_TRUE);
            LCAPI_SetRcamSignal(LC_TRUE);
            //  Check AP Delay
            PROC_RCAM_STATE(eSTATE_RGEAR_APDISP, 150, 0xC4);
            break;
        }
        IF_NEQ_SET_EQ(m_nRcamEvent, 0);
        break;
        
    case eSTATE_RGEAR_APDISP:
        switch(m_nRcamEvent)
        {
        case IDM_EVENT_RCAM_OFF:
            PROC_RCAM_STATE(eSTATE_XGEAR_APSIG, 1, 0xC5);
            break;
        case IDM_EVENT_RCAM_ON:
        default:
            CommandProcess(IDM_LCD_ON, NULL, 0);
            UtilReloadOpsPasData();             //  Reload VehicleType
            PROC_RCAM_STATE(eSTATE_RCAM_ON, 1, 0xC6);
            break;
        }
        IF_NEQ_SET_EQ(m_nRcamEvent, 0);
        break;
        
    case eSTATE_RCAM_ON:
        switch(m_nRcamEvent)
        {
        case IDM_EVENT_RCAM_OFF:
            PROC_RCAM_STATE(eSTATE_XGEAR_CONFIRM, 1, 0xC8);
            break;
        case IDM_EVENT_RCAM_ON:
        default:
            PROC_RCAM_STATE(eSTATE_RCAM_ON, 10, 0x00);
            break;
        }
        IF_NEQ_SET_EQ(m_nRcamEvent, 0);
        break;
        
    case eSTATE_XGEAR_CONFIRM:
        switch(m_nRcamEvent)
        {
        case IDM_EVENT_RCAM_ON:
            PROC_RCAM_STATE(eSTATE_RCAM_ON, 1, 0xC9);
            break;
        case IDM_EVENT_RCAM_OFF:
        default:
            CommandProcess(IDM_LCD_CUTOFF, NULL, 0);
            PROC_RCAM_STATE(eSTATE_XGEAR_APSIG, (m_eCanState == STATE_CAN_NONE) ? 50 : 250, 0xCA);
            break;
        }
        IF_NEQ_SET_EQ(m_nRcamEvent, 0);
        break;

    case eSTATE_XGEAR_APSIG:
        switch(m_nRcamEvent)
        {
        case IDM_EVENT_RCAM_ON:
            PROC_RCAM_STATE(eSTATE_RGEAR_APDISP, 1, 0xCB);
            break;
        case IDM_EVENT_RCAM_OFF:
        default:
            wStatus = 0;                        //  R-CAM OFF
            LCAPI_PostMessage(ID_COMMAP, IDM_RCAM_STATUS, &wStatus, sizeof(LC_WORD));
            
            LCAPI_SetRcamOnoff(LC_FALSE);
            LCAPI_SetRcamSignal(LC_FALSE);
            //  Check AP Delay
///            PROC_RCAM_STATE(eSTATE_XGEAR_APDISP, (m_eCanState == STATE_CAN_NONE) ? 850 : 150, 0xCC);
            PROC_RCAM_STATE(eSTATE_XGEAR_APDISP, (m_eCanState == STATE_CAN_NONE) ? 150 : 150, 0xCC);
            break;
        }
        IF_NEQ_SET_EQ(m_nRcamEvent, 0);
        break;
        
    case eSTATE_XGEAR_APDISP:
        switch(m_nRcamEvent)
        {
        case IDM_EVENT_RCAM_ON:
            PROC_RCAM_STATE(eSTATE_RGEAR_APSIG, 1, 0xCD);
            break;
        case IDM_EVENT_RCAM_OFF:
        default:
            CommandProcess(IDM_LCD_ON, NULL, 0);
            PROC_RCAM_STATE(eSTATE_RCAM_OFF, 1, 0xCE);
            break;
        }
        IF_NEQ_SET_EQ(m_nRcamEvent, 0);
        break;
    }
}


//  Util
static LC_VOID UtilReloadOpsPasData()
{
    LC_WORD awParkinfo[4];
    
    switch(m_eVehicleType)
    {
    case VEHICLE_SMART:
        awParkinfo[0] = 0xFFFD;
        break;
    case VEHICLE_VITO:
        awParkinfo[0] = 0xFFFC;
        break;
    default:
        return;
    }

    m_dwPasDir = ePAS_DIR_LEFT;
    m_nPasVal = PAS_PICTURE_CENTER;

    for(int i = 0; i < 4; i++)
    {
        m_aOpsDir[i] = eOPS_DIR_NEAR;
        m_aOpsVal[i] = OPS_OFF_VAL;
    }

    awParkinfo[1] = (LC_WORD)m_nPasVal;
    awParkinfo[2] = (m_aOpsVal[0] << 8) | m_aOpsVal[1];
    awParkinfo[3] = (m_aOpsVal[2] << 8) | m_aOpsVal[3];
       
    LCAPI_PostMessage(ID_COMMAP, IDM_PARKINFO_INIT, awParkinfo, sizeof(LC_WORD) * 4);
}

//  Macro Functions
static LC_VOID PROC_AMP_STATE(LC_S32 nState, LC_S32 nElapse, LC_S32 nPrint)
{
    m_nAmpState  = nState;
    LCAPI_SetTimer(m_dwID, TID_AMP, nElapse);
    if(nPrint)
        LCAPI_AmpLcd_Putc(nPrint);
}

static LC_VOID PROC_LCD_STATE(LC_S32 nState, LC_S32 nElapse, LC_S32 nPrint)
{
    m_nLcdState  = nState;
    LCAPI_SetTimer(m_dwID, TID_LCD, nElapse);
    if(nPrint)
        LCAPI_AmpLcd_Putc(nPrint);
}

static LC_VOID PROC_FLED_STATE(LC_S32 nState, LC_S32 nElapse, LC_S32 nPrint)
{
    m_nFrontLedState = nState;
    LCAPI_SetTimer(m_dwID, TID_FLED, nElapse);
    if(nPrint)
        LCAPI_AmpLcd_Putc(nPrint);
}

static LC_VOID PROC_RCAM_STATE(LC_S32 nState, LC_S32 nElapse, LC_S32 nPrint)
{
    m_nRcamState  = nState;
    LCAPI_SetTimer(m_dwID, TID_RCAM, nElapse);
    if(nPrint)
        LCAPI_AmpLcd_Putc(nPrint);
}

