//----------------------------------------------
//  Headers
//----------------------------------------------
#include "TaskDload.h"

#include "LC_API.h"
#include "LC_Q.h"

#include "DevFlash.h"


//----------------------------------------------
//  Definitions
//----------------------------------------------
#define EVENT_MASK              (EVENT_MESSAGE)


//----------------------------------------------
//  Variables
//----------------------------------------------
extern LC_HANDLE *g_phEvent;
extern LC_HANDLE *g_phQueue;
extern LC_HANDLE *g_phDevice;

static LC_DWORD m_dwID;

static volatile LC_BOOL m_bSuspend = LC_FALSE;

//  Task Queue
static LC_BYTE m_MsgQ[128];

static const xLC_Q xQueueList[1] = {
    {   .wHead = 0, .wTail = 0, .pBuffer = m_MsgQ,  .dwSize = sizeof(m_MsgQ), },
};


//----------------------------------------------
//  Private Functions
//----------------------------------------------
//  Initial
static LC_BOOL Init(void *arg);
static void OnResume();
static void OnSuspend();

//  Event & Queue
static LC_BOOL OnEventNotify(LC_DWORD dwEvent);
static void CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);

//  Process
static void DoSuspend(LC_DWORD dwCmd);
static void DoResume(LC_DWORD dwCmd);
static void DoEraseFlash(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static void DoWriteFlash(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);

//  Send
static void PostSuspendDone(LC_DWORD dwID);
static void PostResumeDone(LC_DWORD dwID);
static void PostEraseResponse(LC_DWORD dwID, LC_DWORD dwResponse);
static void PostWriteResponse(LC_DWORD dwID, LC_DWORD dwResponse);


//----------------------------------------------
//  Functions
//----------------------------------------------
void TaskUpdate(void *arg)
{
    LC_DWORD dwEvent;

    if(Init(arg) == LC_FALSE)
        return;

    do
    {
        dwEvent = LCAPI_WaitEvent(*(g_phEvent + m_dwID), EVENT_MASK, WAIT_INFINITE);

        if(dwEvent != 0)
            OnEventNotify(dwEvent);
    }
    while(LC_TRUE);
}

// Initial
LC_BOOL Init(void *arg)
{
    LC_WORD *pwBuffer = (LC_WORD *)arg;

    m_dwID = pwBuffer[0];
  
    *(g_phEvent + m_dwID) = LCAPI_CreateEvent();
    if(*(g_phEvent + m_dwID) == NULL) return LC_FALSE;
  
    *(g_phQueue + m_dwID) = (LC_HANDLE)LCAPI_CreateQ((xLC_Q *)&xQueueList[0]);
    if(*(g_phQueue + m_dwID) == (LC_HANDLE)LC_DEINIT) return LC_FALSE;

///    OnResume();
  
    LCAPI_PostEvent(ID_MC, EVENT_INIT);

    return LC_TRUE;
}

void OnResume()
{
    //  Set Timer
    
    //  Initial Variable
    
    //  Initial Lib
    *(g_phDevice + DEV_FLASH) = LCDRV_Open(DEV_FLASH, NULL);
}

void OnSuspend()
{
    //  Set For Energy Saving
    
    //  Deint Lib Function ... & etc
    LCDRV_Close(*(g_phDevice + DEV_FLASH));

    //  Timer Kill
}

//  Event & Queue
LC_BOOL OnEventNotify(LC_DWORD dwEvent)
{
    LC_DWORD dwCmd;
    LC_DWORD dwSize;
    LC_BYTE Buffer[128];
    
    if(dwEvent & EVENT_MESSAGE)
    {
        do
        {
            if(*(g_phQueue + m_dwID) == (void *)LC_DEINIT) break;
            
            int nLength;
            if((nLength = LCAPI_GetQLength((LC_DWORD)*(g_phQueue + m_dwID))) >= QMESSAGE_HEADER_SIZE)
            {
                dwSize = LCAPI_GetQMessage((LC_DWORD)*(g_phQueue + m_dwID), &dwCmd, Buffer, nLength - QMESSAGE_HEADER_SIZE);
                CommandProcess(dwCmd, (void *)Buffer, dwSize);
            }
            else
                break;
        }
        while(LC_TRUE);
    }

    return LC_TRUE;
}

void CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    switch(dwCmd)
    {
    case IDM_PROCESS_SUSPEND:
        DoSuspend(dwCmd);
        break;
    case IDM_PROCESS_RESUME:
        DoResume(dwCmd);
        break;
    case IDM_ERASE_FLASH:
    case IDM_ERASE_FLASH_PCSIM:
        DoEraseFlash(dwCmd, pData, dwSize);
        break;
    case IDM_WRITE_FLASH:
    case IDM_WRITE_FLASH_PCSIM:
        DoWriteFlash(dwCmd, pData, dwSize);
        break;
    default:
        LCAPI_Printf("(0x%X) Unknown Command 0x%X", m_dwID, dwCmd);
        break;
    }
}


//  Process
void DoSuspend(LC_DWORD dwCmd)
{
    m_bSuspend = LC_TRUE;
    
    OnSuspend();
    
    PostSuspendDone(ID_MC);
}

void DoResume(LC_DWORD dwCmd)
{
    m_bSuspend = LC_FALSE;
    
    OnResume();

    PostResumeDone(ID_MC);
}

void DoEraseFlash(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_DWORD dwResponse;
    
    if(LCDRV_Ioctl(*(g_phDevice + DEV_FLASH), IOCTL_ERASE_FLASH, pData, dwSize, &dwResponse, sizeof(LC_DWORD)) <= 0)
    {
        dwResponse = 0xF0;
    }

    PostEraseResponse((dwCmd == IDM_ERASE_FLASH) ? ID_COMMAP : ID_PCSIM, dwResponse);
}

void DoWriteFlash(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_DWORD dwResponse;
    
    if(LCDRV_Ioctl(*(g_phDevice + DEV_FLASH), IOCTL_WRITE_FLASH, pData, dwSize, &dwResponse, sizeof(LC_DWORD)) <= 0)
    {
        dwResponse = 0xF1;
    }
    
    PostWriteResponse((dwCmd == IDM_WRITE_FLASH) ? ID_COMMAP : ID_PCSIM, dwResponse);
}


//  Send
void PostSuspendDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_SUSPEND, &m_dwID, sizeof(LC_DWORD));
}

void PostResumeDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_RESUME, &m_dwID, sizeof(LC_DWORD));
}

void PostEraseResponse(LC_DWORD dwID, LC_DWORD dwResponse)
{
    LCAPI_PostMessage(dwID, IDM_ERASE_RESPONSE, &dwResponse, sizeof(LC_DWORD));
}

void PostWriteResponse(LC_DWORD dwID, LC_DWORD dwResponse)
{
    LCAPI_PostMessage(dwID, IDM_WRITE_RESPONSE, &dwResponse, sizeof(LC_DWORD));
}
