//----------------------------------------------
//  Headers
//----------------------------------------------
#include "TaskCommAP.h"


//----------------------------------------------
//  Definitions
//----------------------------------------------
#define EVENT_MASK              (EVENT_MESSAGE | EVENT_UART_RC)

#define RECV_BUFFER_SIZE        (520 + 12)      // (255 + 5) x 2 + 2(STX+ETX) + 8(Pos) + 2
#define SEND_BUFFER_SIZE        (260 + 12)      // (255 + 5)     + 2(STX+ETX) + 8(Pos) + 2

#define TID_DLOAD_RESPONSE      1000
#define TID_REQ_OSTIME          1001
#define TID_SEND_RCAM_STATUS    1002
#define TID_FORCE_APBOOT        1003

#define REQ_OSTIME_ELAPSE       60000           // 1 minute
#define SEND_RCAM_STATUS_ELAPSE 500
#define FORCE_APBOOT_ELAPSE     5000

#define CARINFO_CHAR            0xC0


//----------------------------------------------
//  Variables
//----------------------------------------------
extern LC_HANDLE *g_phEvent;
extern LC_HANDLE *g_phQueue;
extern LC_HANDLE *g_phDevice;

static LC_DWORD m_dwID;

static volatile LC_BOOL m_bSuspend = LC_FALSE;

//
static LC_BYTE m_RecvBuffer[RECV_BUFFER_SIZE];
static LC_BYTE m_SendBuffer[RETRY_PACKET_MAX][SEND_BUFFER_SIZE];

static LC_DWORD m_dwRecvSize;

static LC_DWORD m_dwRecvLink;
static LC_DWORD m_dwSendLink;

static LC_BOOL m_bError;

static LC_BOOL m_bApLink = LC_FALSE;
static LC_BOOL m_bApBoot = LC_FALSE;

static xUartRetry m_xUartRetry;

//  Task Queue
static LC_BYTE m_MsgQ[128];

static const xLC_Q xQueueList[1] = {
    {   .wHead = 0, .wTail = 0, .pBuffer = m_MsgQ,  .dwSize = sizeof(m_MsgQ), },
};


//  ACC Status
static LC_BOOL m_bPoweroffReady = LC_FALSE;

//  CAN Data
static LC_BYTE m_Info[4] = { 0xFF, 0xFF, 0xFF, 0xFF };
static LC_BYTE m_Hvac[8] = { 0xFF, 0xFF, 0xFF, 0xFF };
static LC_DWORD m_dwDoorOpen = 0;


//----------------------------------------------
//  Private Functions
//----------------------------------------------
//  Initial
static LC_BOOL Init(void *arg);
static void OnResume();
static void OnSuspend();

//  Event & Queue
static LC_BOOL OnEventNotify(LC_DWORD dwEvent);
static void CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);

//  Timer
static void OnTimeout(LC_DWORD dwNumber);

//  Recv Command
static void DoSuspend(LC_DWORD dwCmd);
static void DoResume(LC_DWORD dwCmd);
static void DoPoweroffReady(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static void DoReqOsTime(LC_DWORD dwCmd);
static void DoReqPoweroff(LC_DWORD dwCmd);
static void DoOnTimer(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);
static void DoAmpStatus(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoLcdStatus(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoRcamStatus(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoRecvUartNotify(LC_DWORD dwCmd);
static void DoSendRetry(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoCanInfo(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoCanKey(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoCanGear(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoCanBrake(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoCanIll(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoCanHvac(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoCanDoorOpen(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoEraseResponse(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoWriteResponse(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoSendSim(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoSendButton(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoSendButtonVito(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoParkinfoInit(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoLoadPas(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoLoadOps(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);

//  Send Command
static void PostApBoot();
static void PostSuspendDone(LC_DWORD dwID);
static void PostResumeDone(LC_DWORD dwID);

//  Recv Serial
static void RecvDataRequest(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvOsTime(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvAmpMute(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvWdtElapse(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvWdtClear();
static void RecvLcdOnoff(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvLcdBrightness(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvLedBrightness(LC_BYTE *pBuffer, LC_DWORD cbData);
static void RecvDownRequest();
static void RecvDownSendData(LC_BYTE *pBuffer, LC_DWORD cbData);

//  Send Serial
static void SendPoweroffReady();
static void SendReqPoweroff();
static void SendVersion();
static void SendMcuTime();
static void SendReqOsTime();
static void SendAmpStatus();
static void SendLcdStatus();
static void SendRcamStatus();
static void SendKeyStatus();
static void SendGearPosition();
static void SendBrake();
static void SendIll();
static void SendHvac();
static void SendDoorOpen();
static void SendButtonData(LC_DWORD dwButton);
static void SendButtonDataVito(LC_DWORD dwButton);
#ifndef APP_PROGRAM
static void SendDownResponse(LC_DWORD dwRespnse);
#endif
static void SendDownSendDataRsp(LC_DWORD dwResponse);

//  Serial API
static int RecvMessage(LC_BYTE *pBuffer, LC_DWORD cbData);
static int StripPacket(LC_BYTE *pDest, LC_BYTE *pSrc, LC_DWORD cbData);
static void StripMessage(LC_BYTE *pBuffer, LC_DWORD cbData);
static void OnRecvData(LC_BYTE *pBuffer, LC_DWORD cbData);
static void OnAcknowledge(LC_BYTE *pBuffer, LC_DWORD cbData);
static void SendAcknowledge(LC_BYTE *pBuffer, LC_DWORD cbData);

static int BuildPacket(LC_BYTE *pDest, LC_BYTE *pSrc, LC_DWORD cbData);
static int BuildRetry(LC_BYTE *pBuffer, LC_DWORD cbData);
static int BuildData(LC_BYTE *pBuffer, LC_DWORD cbData, LC_BOOL bAckRequest);


//----------------------------------------------
//  Functions
//----------------------------------------------
void TaskCommAP(void *arg)
{
    LC_DWORD dwEvent;

    if(Init(arg) == LC_FALSE)
        return;

    do
    {
        dwEvent = LCAPI_WaitEvent(*(g_phEvent + m_dwID), EVENT_MASK, WAIT_INFINITE);

        if(dwEvent != 0)
            OnEventNotify(dwEvent);
    }
    while(LC_TRUE);
}

static LC_BOOL Init(void *arg)
{
    LC_WORD *pwBuffer = (LC_WORD *)arg;

    m_dwID = pwBuffer[0];

    *(g_phEvent + m_dwID) = LCAPI_CreateEvent();
    if(*(g_phEvent + m_dwID) == NULL) return LC_FALSE;

    *(g_phQueue + m_dwID) = (void *)LCAPI_CreateQ((xLC_Q *)&xQueueList[0]);
    if(*(g_phQueue + m_dwID) == (void *)LC_DEINIT) return LC_FALSE;

///    OnResume();
    
    LCAPI_PostEvent(ID_MC, EVENT_INIT);

    return LC_TRUE;
}

static void OnResume()
{
    //  Set Timer
#ifndef APP_PROGRAM
    LC_DWORD dwBootType = LCLIB_GetBootType();
    if(dwBootType == RUN_DLOAD_AP)
    {
        LCAPI_SetTimer(m_dwID, TID_DLOAD_RESPONSE, 100);
    }
#endif

    //  Initial Variable
    m_dwRecvLink = LCAPI_GetRecvLink();
    m_dwSendLink = LCAPI_GetSendLink();

    m_xUartRetry.dwPos = 0;
    m_xUartRetry.dwSize = 0;
    for(int i = 0; i < RETRY_PACKET_MAX; i++)
    {
        m_xUartRetry.xInfo[i].bActive = LC_FALSE;
        m_xUartRetry.xInfo[i].pBuffer = m_SendBuffer[i];
    }
    
    //  Initial Lib
    *(g_phDevice + DEV_UART4) = LCDRV_Open(DEV_UART4, NULL);
    *(g_phDevice + DEV_SPI) = LCDRV_Open(DEV_SPI, NULL);
}

static void OnSuspend()
{
    //  Set for Energy Saving
    
    //  Device Driver Closed
    LCDRV_Close(*(g_phDevice + DEV_UART4));
    LCDRV_Close(*(g_phDevice + DEV_SPI));
    
    //  Timer Kill
    LCAPI_KillTimer(m_dwID, TID_DLOAD_RESPONSE);
    LCAPI_KillTimer(m_dwID, TID_REQ_OSTIME);
    LCAPI_KillTimer(m_dwID, TID_SEND_RCAM_STATUS);
    LCAPI_KillTimer(m_dwID, TID_FORCE_APBOOT);
}

//----------------------------------------------
static void OnTimeout(LC_DWORD dwNumber)
{
//    if(m_bApBoot == LC_FALSE) return;
    
    LCAPI_PostMessage(m_dwID, IDM_UART_SEND_RETRY, (void *)&dwNumber, sizeof(LC_DWORD));
}


//----------------------------------------------
static LC_BOOL OnEventNotify(LC_DWORD dwEvent)
{
    LC_DWORD dwCmd;
    LC_DWORD dwSize;
    LC_BYTE Buffer[128];

    if(dwEvent & EVENT_UART_RC)
    {
        CommandProcess(IDM_UART_RECV_NOTIFY, NULL, 0);
    }
    if(dwEvent & EVENT_MESSAGE)
    {
        do
        {
            if(*(g_phQueue + m_dwID) == (LC_HANDLE)LC_DEINIT) break;;
            
            int nLength;
            if((nLength = LCAPI_GetQLength((LC_DWORD)*(g_phQueue + m_dwID))) >= QMESSAGE_HEADER_SIZE)
            {
                dwSize = LCAPI_GetQMessage((LC_DWORD)*(g_phQueue + m_dwID), &dwCmd, Buffer, nLength - QMESSAGE_HEADER_SIZE);
                CommandProcess(dwCmd, (LC_PVOID)Buffer, dwSize);
            }
            else
                break;
        }
        while(LC_TRUE);
    }

    return LC_TRUE;
}

static void CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    switch(dwCmd & 0xFFFF)
    {
    case IDM_PROCESS_SUSPEND:
        DoSuspend(dwCmd);
        break;
    case IDM_PROCESS_RESUME:
        DoResume(dwCmd);
        break;
    case IDM_OFFREADY:
        DoPoweroffReady(dwCmd, pData, dwSize);
        break;
    case IDM_REQ_OS_TIME:
        DoReqOsTime(dwCmd);
        break;
    case IDM_REQOFF:
        DoReqPoweroff(dwCmd);
        break;
    case IDM_ON_TIMER:
        DoOnTimer(dwCmd, pData, dwSize);
        break;
    case IDM_AMP_STATUS:
        DoAmpStatus(dwCmd, pData, dwSize);
        break;
    case IDM_LCD_STATUS:
        DoLcdStatus(dwCmd, pData, dwSize);
        break;
    case IDM_RCAM_STATUS:
        DoRcamStatus(dwCmd, pData, dwSize);
        break;
    case IDM_CANDATA_CLOSED:
        break;
    case IDM_UART_RECV_NOTIFY:
        DoRecvUartNotify(dwCmd);
        break;
    case IDM_UART_SEND_RETRY:
        DoSendRetry(dwCmd, pData, dwSize);
        break;
    case IDM_CAN_INFO:
        DoCanInfo(dwCmd, pData, dwSize);
        break;
    case IDM_CAN_KEY:
        DoCanKey(dwCmd, pData, dwSize);
        break;
    case IDM_CAN_GEAR:
        DoCanGear(dwCmd, pData, dwSize);
        break;
    case IDM_CAN_BRAKE:
        DoCanBrake(dwCmd, pData, dwSize);
        break;
    case IDM_CAN_ILL:
        DoCanIll(dwCmd, pData, dwSize);
        break;
    case IDM_CAN_HVAC:
        DoCanHvac(dwCmd, pData, dwSize);
        break;
    case IDM_CAN_DOOR_OPEN:
        DoCanDoorOpen(dwCmd, pData, dwSize);
        break;
    case IDM_ERASE_RESPONSE:
        DoEraseResponse(dwCmd, pData, dwSize);
        break;
    case IDM_WRITE_RESPONSE:
        DoWriteResponse(dwCmd, pData, dwSize);
        break;
    case IDM_UART_SEND_SIM:
        DoSendSim(dwCmd, pData, dwSize);
        break;
    case IDM_SEND_BUTTON:
        DoSendButton(dwCmd, pData, dwSize);
        break;
    case IDM_SEND_BUTTON_VITO:
        DoSendButtonVito(dwCmd, pData, dwSize);
        break;
    case IDM_PARKINFO_INIT:
        DoParkinfoInit(dwCmd, pData, dwSize);
        break;
    case IDM_LOAD_PAS:
        DoLoadPas(dwCmd, pData, dwSize);
        break;
    case IDM_LOAD_OPS:
        DoLoadOps(dwCmd, pData, dwSize);
        break;
    default:
        LCAPI_Printf("(0x%X) Unknown Command 0x%X", m_dwID, dwCmd);
        break;
    }
}


//----------------------------------------------
//----------------------------------------------
//  Process
static void DoSuspend(LC_DWORD dwCmd)
{
    m_bSuspend = LC_TRUE;
    
    OnSuspend();
    
    PostSuspendDone(ID_MC);
}

static void DoResume(LC_DWORD dwCmd)
{
    m_bSuspend = LC_FALSE;

    OnResume();
    
    PostResumeDone(ID_MC);
}

static void DoPoweroffReady(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_BOOL *pbBuffer = (LC_BOOL *)pData;
    
    m_bPoweroffReady = (LC_BOOL)pbBuffer[0];
    
    if(m_bApBoot == LC_TRUE)
    {
        SendPoweroffReady();
    }
}

static void DoReqOsTime(LC_DWORD dwCmd)
{
    SendReqOsTime();
}

static void DoReqPoweroff(LC_DWORD dwCmd)
{
    SendReqPoweroff();
}

static void DoOnTimer(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    if(pdwBuffer[0] < RETRY_PACKET_MAX)
        OnTimeout(pdwBuffer[0]);
    else
    {
        switch(pdwBuffer[0])
        {
        case TID_DLOAD_RESPONSE:
#ifndef APP_PROGRAM
            LCAPI_KillTimer(m_dwID, TID_DLOAD_RESPONSE);
            SendDownResponse(0);
#endif
            break;
        case TID_REQ_OSTIME:
            LCAPI_KillTimer(m_dwID, TID_REQ_OSTIME);
            SendReqOsTime();
            break;
        case TID_SEND_RCAM_STATUS:
            LCAPI_KillTimer(m_dwID, TID_SEND_RCAM_STATUS);
            SendRcamStatus();
            break;
        case TID_FORCE_APBOOT:
            PostApBoot();
            break;
        }
    }
}

static void DoAmpStatus(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_WORD *pwBuffer = (LC_WORD *)pData;
    
    LCLIB_SetAmpStatus(pwBuffer[0]);

    if(m_bApBoot == LC_FALSE)
    {
        SendAmpStatus();
    }
}

static void DoLcdStatus(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_WORD *pwBuffer = (LC_WORD *)pData;
    
    LCLIB_SetLcdStatus(pwBuffer[0]);
    
    if(m_bApBoot == LC_TRUE)
    {
        SendLcdStatus();
    }
}

static void DoRcamStatus(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_WORD *pwBuffer = (LC_WORD *)pData;
    
    LCLIB_SetRcamStatus(pwBuffer[0]);
    
    if(m_bApBoot == LC_TRUE)
    {
        SendRcamStatus();
    }
}

static void DoRecvUartNotify(LC_DWORD dwCmd)
{
    int nLength;
    LC_BYTE Buffer[128];

    while((nLength = LCDRV_Read(*(g_phDevice + DEV_UART4), Buffer, 128)) > 0)
    {
        RecvMessage(Buffer, (LC_DWORD)nLength);
    }
}

static void DoSendRetry(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *dwNumber = (LC_DWORD *)pData;
    BuildData(m_xUartRetry.xInfo[*dwNumber].pBuffer, m_xUartRetry.xInfo[*dwNumber].dwSize,  LC_FALSE);
}

static void DoCanInfo(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    
    int nPos = 0;
    DoCanKey(IDM_CAN_KEY, pBuffer + nPos, 1);
    
    nPos++;
    DoCanGear(IDM_CAN_GEAR, pBuffer + nPos, 1);

    nPos++;
    DoCanBrake(IDM_CAN_BRAKE, pBuffer + nPos, 1);

    nPos++;
    DoCanIll(IDM_CAN_ILL, pBuffer + nPos, 1);
}

static void DoCanKey(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;

    if(m_Info[0] != (LC_BYTE)pdwBuffer[0])
    {
        m_Info[0] = (LC_BYTE)pdwBuffer[0];
    
        if(m_bApBoot == LC_TRUE)
        {
            SendKeyStatus();
        }
    }
}

static void DoCanGear(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    if(m_Info[1] != (LC_BYTE)pdwBuffer[0])
    {
        m_Info[1] = (LC_BYTE)pdwBuffer[0];
    
        if(m_bApBoot == LC_TRUE)
        {
            SendGearPosition();
        }
    }
}

static void DoCanBrake(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    if(m_Info[2] != (LC_BYTE)pdwBuffer[0])
    {
        m_Info[2] = (LC_BYTE)pdwBuffer[0];
        if(m_bApBoot == LC_TRUE)
        {
            SendBrake();
        }
    }
}

static void DoCanIll(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    if(m_Info[3] != (LC_BYTE)pdwBuffer[0])
    {
        m_Info[3] = (LC_BYTE)pdwBuffer[0];
        if(m_bApBoot == LC_TRUE)
        {
            SendIll();
        }
    }
}

static void DoCanHvac(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    LC_BOOL bDiff = LC_FALSE;

    for(int i = 0; i < 5; i++)
    {
        if(m_Hvac[i] != pBuffer[i])
        {
            bDiff = LC_TRUE;
            m_Hvac[i] = pBuffer[i];
        }
    }
    
    if(bDiff == LC_TRUE)
    {
        if(m_bApBoot == LC_TRUE)
        {
            SendHvac();
        }
    }
}

static void DoCanDoorOpen(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    if(m_dwDoorOpen != pdwBuffer[0])
    {
        m_dwDoorOpen = pdwBuffer[0];
        if(m_bApBoot == LC_TRUE)
        {
            SendDoorOpen();
        }
    }
}

static void DoEraseResponse(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    SendDownSendDataRsp(pdwBuffer[0]);
}

static void DoWriteResponse(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    
    SendDownSendDataRsp(pdwBuffer[0]);
}

static void DoSendSim(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    if(m_bApBoot == LC_FALSE) return;
    
    LC_BYTE *pBuffer = (LC_BYTE *)pData;
    LC_BOOL bAckRequest = (pBuffer[1] & ACK_REQUEST) ? LC_TRUE : LC_FALSE;

    // Download Check ??
    BuildData(pBuffer, dwSize, bAckRequest);
    
    switch(pBuffer[1] & (~ACK_REQUEST))
    {
    case FMT_CONTROL:
        LCAPI_toAP_Putc(pBuffer[2]);
        break;
    case FMT_CARINFO:
        LCAPI_toAP_Putc(pBuffer[2] | CARINFO_CHAR);
        break;
    }
}

static void DoSendButton(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    if(m_bApBoot == LC_FALSE) return;
    
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;

    SendButtonData(*pdwBuffer);
}

static void DoSendButtonVito(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    if(m_bApBoot == LC_FALSE) return;
    
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;

    SendButtonDataVito(*pdwBuffer);
}

static void DoParkinfoInit(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LCDRV_Ioctl(*(g_phDevice + DEV_SPI), IOCTL_PARKINFO_INIT, pData, dwSize, 0, NULL);
}

static void DoLoadPas(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LCDRV_Ioctl(*(g_phDevice + DEV_SPI), IOCTL_LOAD_PAS, pData, dwSize, 0, NULL);
}

static void DoLoadOps(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LCDRV_Ioctl(*(g_phDevice + DEV_SPI), IOCTL_LOAD_OPS, pData, dwSize, 0, NULL);
}


//  Send
static void PostApBoot()
{
    m_bApBoot = LC_TRUE;
    
    LCAPI_KillTimer(m_dwID, TID_FORCE_APBOOT);
    
//    SendRcamStatus();
    SendPoweroffReady();
    LCAPI_SetTimer(m_dwID, TID_SEND_RCAM_STATUS, SEND_RCAM_STATUS_ELAPSE);

    LCAPI_CanDiff_Putc(0x97);

    LCAPI_PostEvent(ID_SYSTEM, EVENT_APBOOT);
}

static void PostSuspendDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_SUSPEND, &m_dwID, sizeof(LC_DWORD));
}

static void PostResumeDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_RESUME, &m_dwID, sizeof(LC_DWORD));
}


//  Serial Recv
static void RecvDataRequest(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[0] & 0x7F)
    {
    case FMT_CONTROL:
        switch(pBuffer[3])
        {
        case CMD_MCU_VERSION:
            SendVersion();
            break;
        case CMD_MCU_TIME:
            SendMcuTime();
            break;
        case CMD_AMP_STATUS:
            SendAmpStatus();
            break;
        case CMD_LCD_STATUS:
            SendLcdStatus();
            break;
        case CMD_RCAM_STATUS:
            SendRcamStatus();
            break;
        }
        break;
        
    case FMT_CARINFO:
        switch(pBuffer[3])
        {
        case CMD_CAR_STATUS:
            SendKeyStatus();
            break;
        case CMD_GEAR_POSITION:
            SendGearPosition();
            break;
        case CMD_BRAKE:
            SendBrake();
            break;
        case CMD_ILLUMINATION:
            SendIll();
            break;
        case CMD_HVAC:
            SendHvac();
            break;
        case CMD_DOOR_OPEN:
            SendDoorOpen();
            break;
        }
        break;
    }        
}

static void RecvOsTime(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pBuffer;
    
    if(LCAPI_SetRtcTime(pdwBuffer[0]) == LC_FALSE)
    {
        LCAPI_SetTimer(m_dwID, TID_REQ_OSTIME, REQ_OSTIME_ELAPSE);
    }
}

static void RecvAmpMute(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_APSET_MUTE, pBuffer, 1);
    
    LCAPI_SetAmpMute((LC_BOOL)pBuffer[0]);
}

static void RecvWdtElapse(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_WDT_ELAPSE, pBuffer, 1);
}

static void RecvWdtClear()
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_WDT_CLEAR, NULL, 0);
}

static void RecvLcdOnoff(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    if(pBuffer[0] != 0)
        LCAPI_PostMessage(ID_SYSTEM, IDM_LCD_ON, NULL, 0);
    else
        LCAPI_PostMessage(ID_SYSTEM, IDM_LCD_OFF, NULL, 0);
}

static void RecvLcdBrightness(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_LCD_BRIGHTNESS, pBuffer, cbData);
}

static void RecvLedBrightness(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LCAPI_PostMessage(ID_SYSTEM, IDM_FLED_BRIGHTNESS, pBuffer, 1);
}

static void RecvDownRequest()
{
#ifdef APP_PROGRAM
    LCAPI_SetRecvLink(m_dwRecvLink);
    LCAPI_SetSendLink(m_dwSendLink);

    LC_DWORD dwDload = RUN_DLOAD_AP;
    LCAPI_PostMessage(ID_MC, IDM_JUMP_BOOT, &dwDload, sizeof(LC_DWORD));
#else
    SendDownResponse(0);
#endif
}

static void RecvDownSendData(LC_BYTE *pBuffer, LC_DWORD cbData)
{
#ifndef APP_PROGRAM
    LC_DWORD dwDst;
    LC_BYTE Buffer[32];
    LC_DWORD dwResponse;

    //  1.  Parsing Data
    dwResponse = LCLIB_ParseSrec(Buffer + 1, &dwDst, pBuffer + 3, cbData - 3);
    
    LCAPI_Putc(dwResponse);
    
    //  2.  Make Write Data and Write Flash
    if(dwResponse > 0x00 && dwResponse < 0x80)
    {
        Buffer[0] = (LC_BYTE)dwResponse;
        dwDst += 1;
        LCAPI_PostMessage(ID_UPDATE, IDM_WRITE_FLASH, (void *)Buffer, dwDst);
    }
    else if(dwResponse == 0)
    {
        LC_DWORD dwBuffer[2];
        dwBuffer[0] = 0x08010000;
        dwBuffer[1] = 0x00010000;

        LCAPI_PostMessage(ID_UPDATE, IDM_ERASE_FLASH, (void *)dwBuffer, sizeof(dwBuffer));
    }
    else
        SendDownSendDataRsp(dwResponse);
    //  3.  Send Response to AP

#endif
}


//  Serial Send
static void SendPoweroffReady()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_OFFREADY;
    Buffer[dwSize++] = 1;
    Buffer[dwSize++] = (LC_BYTE)m_bPoweroffReady;
  
    BuildData(Buffer, dwSize, LC_TRUE);

    LCAPI_toAP_Putc(CMD_OFFREADY);
}

static void SendReqPoweroff()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_REQOFF;
    Buffer[dwSize++] = 0;
  
    BuildData(Buffer, dwSize, LC_TRUE);

    LCAPI_toAP_Putc(CMD_REQOFF);
}

static void SendVersion()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[16];
    
    LC_DWORD dwVersionSize;
    LC_BYTE Version[8];
    
    dwVersionSize = LCLIB_ReadVersion(Version);

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_MCU_VERSION;
    Buffer[dwSize++] = dwVersionSize;          //  Size
    for(int i = 0; i < dwVersionSize; i++)
        Buffer[dwSize++] = Version[i];
  
    BuildData(Buffer, dwSize, LC_TRUE);

    LCAPI_toAP_Putc(CMD_MCU_VERSION);    
}

static void SendMcuTime()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[16];
    
    LC_DWORD dwMcuTime;

    dwMcuTime = LCAPI_GetRtcTime();
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_MCU_TIME;
    Buffer[dwSize++] = 4;
    Buffer[dwSize++] = (dwMcuTime      ) & 0xFF;
    Buffer[dwSize++] = (dwMcuTime >>  8) & 0xFF;
    Buffer[dwSize++] = (dwMcuTime >> 16) & 0xFF;
    Buffer[dwSize++] = (dwMcuTime >> 24) & 0xFF;
    
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_MCU_TIME);    
}

static void SendReqOsTime()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_REQ_OS_TIME;
    Buffer[dwSize++] = 0;
    
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_REQ_OS_TIME);    
}

static void SendAmpStatus()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[16];
    
    LC_WORD wStatus;

    wStatus = LCLIB_GetAmpStatus();

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_AMP_STATUS;
    Buffer[dwSize++] = 2;
    Buffer[dwSize++] = (wStatus      ) & 0xFF;
    Buffer[dwSize++] = (wStatus >>  8) & 0xFF;
    
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_AMP_STATUS);    
}

static void SendLcdStatus()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[16];
    
    LC_WORD wStatus;

    wStatus = LCLIB_GetLcdStatus();

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_LCD_STATUS;
    Buffer[dwSize++] = 2;
    Buffer[dwSize++] = (wStatus      ) & 0xFF;
    Buffer[dwSize++] = (wStatus >>  8) & 0xFF;
    
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_LCD_STATUS);    
}

static void SendRcamStatus()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[16];
    
    LC_WORD wStatus;

    wStatus = LCLIB_GetRcamStatus();

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_RCAM_STATUS;
    Buffer[dwSize++] = 1;
    Buffer[dwSize++] = (LC_BYTE)wStatus;
    
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_RCAM_STATUS);
}

static void SendKeyStatus()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CARINFO | ACK_REQUEST;
    Buffer[dwSize++] = CMD_CAR_STATUS;
    Buffer[dwSize++] = 1;                   // Size
    Buffer[dwSize++] = m_Info[0];
  
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_CAR_STATUS | CARINFO_CHAR);    
}

static void SendGearPosition()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CARINFO | ACK_REQUEST;
    Buffer[dwSize++] = CMD_GEAR_POSITION;
    Buffer[dwSize++] = 1;                   // Size
    Buffer[dwSize++] = m_Info[1];
  
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_GEAR_POSITION | CARINFO_CHAR);
    
#if 0
    LCAPI_Putc(m_Info[1]);
#endif
}

static void SendBrake()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CARINFO | ACK_REQUEST;
    Buffer[dwSize++] = CMD_BRAKE;
    Buffer[dwSize++] = 1;                   // Size
    Buffer[dwSize++] = m_Info[2];
  
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_BRAKE | CARINFO_CHAR);    
}

static void SendIll()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CARINFO | ACK_REQUEST;
    Buffer[dwSize++] = CMD_ILLUMINATION;
    Buffer[dwSize++] = 1;                   // Size
    Buffer[dwSize++] = m_Info[3];
  
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_ILLUMINATION | CARINFO_CHAR);    
}

static void SendHvac()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[16];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CARINFO | ACK_REQUEST;
    Buffer[dwSize++] = CMD_HVAC;
    Buffer[dwSize++] = 5;                   // Size
    Buffer[dwSize++] = m_Hvac[0];
    Buffer[dwSize++] = m_Hvac[1];
    Buffer[dwSize++] = m_Hvac[2];
    Buffer[dwSize++] = m_Hvac[3];
    Buffer[dwSize++] = m_Hvac[4];
  
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_HVAC | CARINFO_CHAR);
 
#if 0
    for(int i = 0; i < 5; i++)
    {
        LCAPI_Delaymsec(1);
        LCAPI_Putc(m_Hvac[i]);
    }
#endif
}

static void SendDoorOpen()
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CARINFO | ACK_REQUEST;
    Buffer[dwSize++] = CMD_DOOR_OPEN;
    Buffer[dwSize++] = 1;                   // Size
    Buffer[dwSize++] = (LC_DWORD)m_dwDoorOpen;
  
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_DOOR_OPEN | CARINFO_CHAR);    
}

#ifndef APP_PROGRAM
static void SendDownResponse(LC_DWORD dwResponse)
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_DLOAD_RESPONSE;
    Buffer[dwSize++] = 1;                   // Size
    Buffer[dwSize++] = (LC_BYTE)dwResponse;
  
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_DLOAD_RESPONSE);
}
#endif

static void SendDownSendDataRsp(LC_DWORD dwResponse)
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CONTROL | ACK_REQUEST;
    Buffer[dwSize++] = CMD_DLOAD_DATA_RESPONSE;
    Buffer[dwSize++] = 1;
    Buffer[dwSize++] = (LC_BYTE)dwResponse;
    
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_DLOAD_DATA_RESPONSE);
}

static void SendButtonData(LC_DWORD dwButton)
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CARINFO | ACK_REQUEST;
    Buffer[dwSize++] = CMD_HANDLE_REMOCON;
    Buffer[dwSize++] = 1;
    Buffer[dwSize++] = (LC_BYTE)dwButton;
    
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_HANDLE_REMOCON | CARINFO_CHAR);
}

static void SendButtonDataVito(LC_DWORD dwButton)
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];
    
    dwSize = 0;
    Buffer[dwSize++] = 0;
    Buffer[dwSize++] = FMT_CARINFO | ACK_REQUEST;
    Buffer[dwSize++] = CMD_HANDLE_REMOCON_VITO;
    Buffer[dwSize++] = 2;
    Buffer[dwSize++] = (LC_BYTE)((dwButton     ) & 0xFF);
    Buffer[dwSize++] = (LC_BYTE)((dwButton >> 8) & 0xFF);
    
    BuildData(Buffer, dwSize, LC_TRUE);
    
    LCAPI_toAP_Putc(CMD_HANDLE_REMOCON_VITO | CARINFO_CHAR);

#if 0
    LCAPI_Putc(Buffer[4]);
    LCAPI_Delaymsec(1);
    LCAPI_Putc(Buffer[5]);
#endif
}


//----------------------------------------------
//----------------------------------------------
//  Serial RX
static int RecvMessage(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    int nSize;
    LC_BYTE Buffer[RECV_BUFFER_SIZE];

    for(int i = 0; i < (int)cbData; i++)
    {
        if(pBuffer[i] == COMM_EOF)
        {
            if(m_dwRecvSize >= 5)
            {
                nSize = StripPacket(Buffer, m_RecvBuffer, m_dwRecvSize);
                if(nSize >= 5)
                {
                    StripMessage(Buffer, nSize - 1);
                }
            }

            m_dwRecvSize = 0;
            continue;
        }

        m_RecvBuffer[m_dwRecvSize++] = pBuffer[i];
    }

    return 0;
}

static int StripPacket(LC_BYTE *pDest, LC_BYTE *pSrc, LC_DWORD cbData)
{
    int nSize = 0;
    LC_BYTE chSum = 0;

    for(int i = 0; i < (int)cbData; i++, nSize++)
    {
        if(pSrc[i] == COMM_IDF)
        {
            i++;
            pDest[nSize] = pSrc[i] | 0x20;
        }
        else
        {
            pDest[nSize] = pSrc[i];
        }

        chSum += pDest[nSize];
    }

    // Parity Error
    if(chSum != 0)
    {
        return 0;
    }

    return nSize;
}

static void StripMessage(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LC_BYTE chLink;
    LC_BYTE chFmt;
    LC_BYTE chCmd;

    chLink = pBuffer[0];
    chFmt  = pBuffer[1];
    chCmd  = pBuffer[2];

    // check command later

    // clear error
    if(m_bError == LC_TRUE)
    {
        m_bError = LC_FALSE;
    }
    
    //  On Force AP Boot
    if(m_bApLink == LC_FALSE)
    {
        m_bApLink = LC_TRUE;
        LCAPI_SetTimer(m_dwID, TID_FORCE_APBOOT, FORCE_APBOOT_ELAPSE);
//        LCAPI_Putc(0x70);
    }

    // Receive ACK
    if(chFmt == FMT_BASIC && chCmd == COMM_ACK)
    {
        OnAcknowledge(pBuffer, 3);
        return;
    }

    // Send Ack
    if(chFmt & ACK_REQUEST)
    {
        SendAcknowledge(pBuffer, 3);
    }
  
    // Prev RxLink ....
//??    if(m_bApBoot == LC_TRUE)
    {
        if(chLink == m_dwRecvLink) return;
        m_dwRecvLink = chLink;
    }
    OnRecvData(pBuffer, cbData);
}

static void OnRecvData(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    switch(pBuffer[1] & 0x7F)
    {
    case FMT_CONTROL:
        switch(pBuffer[2])
        {
        case CMD_POWEROFF:
            m_bApBoot = LC_FALSE;
            LCAPI_PostMessage(ID_SYSTEM, IDM_POWEROFF, NULL, 0);
            break;
        case CMD_AP_REBOOT:
            LCAPI_ResetMcu((LC_DWORD)pBuffer[4]);
            break;

        case CMD_NOP:
            break;
        case CMD_DATA_REQUEST:
            RecvDataRequest(pBuffer + 1, cbData - 1);
            break;
        case CMD_AP_BOOT_COMPLETE:
            PostApBoot();
            break;
        case CMD_OS_TIME:
            RecvOsTime(pBuffer + 4, cbData - 4);
            break;
        case CMD_AMP_MUTE:
            RecvAmpMute(pBuffer + 4, cbData - 4);
            break;
        case CMD_WDT_ELAPSE:
            RecvWdtElapse(pBuffer + 4, cbData - 4);
            break;
        case CMD_WDT_CLEAR:
            RecvWdtClear();
            break;
        case CMD_LCD_ONOFF:
            RecvLcdOnoff(pBuffer + 4, cbData - 4);
            break;
        case CMD_LCD_BRIGHTNESS:
            RecvLcdBrightness(pBuffer + 4, cbData - 4);
            break;
        case CMD_LED_BRIGHTNESS:
            RecvLedBrightness(pBuffer + 4, cbData - 4);
            break;
            
        case CMD_DLOAD_REQUEST:
            RecvDownRequest();
            break;
        case CMD_DLOAD_DATA_SEND:
            RecvDownSendData(pBuffer + 1, cbData - 1);
            break;
        }
        break;
        
    case FMT_CARINFO:
        switch(pBuffer[2])
        {
        case CMD_DATA_REQUEST:
            RecvDataRequest(pBuffer + 1, cbData - 1);
            break;
        }
    }
}

static void OnAcknowledge(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    for(int i = 0; i < m_xUartRetry.dwSize; i++)
    {
        if(m_xUartRetry.xInfo[i].pBuffer[0] == pBuffer[0])
        {
            m_xUartRetry.xInfo[i].bActive = LC_FALSE;
            //  KillTimer
            LCAPI_KillTimer(m_dwID, i);
        }
    }
}

static void SendAcknowledge(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    LC_DWORD dwSize;
    LC_BYTE Buffer[8];

    dwSize = 0;
    Buffer[dwSize++] = pBuffer[0];
    Buffer[dwSize++] = FMT_BASIC;
    Buffer[dwSize++] = COMM_ACK;
    Buffer[dwSize++] = 0;                   // Size

    BuildData(Buffer, dwSize, LC_FALSE);
}


//----------------------------------------------
//----------------------------------------------
static int BuildPacket(LC_BYTE *pDest, LC_BYTE *pSrc, LC_DWORD cbData)
{
    int i;
    int nSize;
    unsigned char nSum = 0;
    
    nSize = 0;
    pDest[nSize++] = COMM_EOF;
    
    for(i = 0; i < (int)cbData; i++)
    {
        if(pSrc[i] == COMM_EOF || pSrc[i] == COMM_IDF)
        {
            pDest[nSize++] = COMM_IDF;
            pDest[nSize++] = pSrc[i] & (~0x20);
        }
        else
        {
            pDest[nSize++] = pSrc[i];
        }
        
        nSum += pSrc[i];
    }
    
    nSum = ((~(nSum & 0xFF)) + 1) & 0xFF;
    if(nSum == COMM_EOF || nSum == COMM_IDF)
    {
        pDest[nSize++] = COMM_IDF;
        pDest[nSize++] = nSum & (~0x20);
    }
    else
    {
        pDest[nSize++] = nSum;
    }
    
    pDest[nSize++] = COMM_EOF;
    
    return nSize;
}

static int BuildRetry(LC_BYTE *pBuffer, LC_DWORD cbData)
{
    int nPos = -1;
  
    // Find Same Command (bAcitve is TRUE)
    for(int i = 0; i < m_xUartRetry.dwSize; i++)
    {
        if(m_xUartRetry.xInfo[i].bActive == LC_TRUE
        && m_xUartRetry.xInfo[i].pBuffer[1] == pBuffer[1] && m_xUartRetry.xInfo[i].pBuffer[2] == pBuffer[2])
        {
            nPos = i;
            break;
        }
    }
    
    // Not found same data or Retry Packet is none
    if(nPos < 0)
    {
        // Find bAcitve is FALSE,
        for(int i = 0; i < m_xUartRetry.dwSize; i++)
        {
            if(m_xUartRetry.xInfo[i].bActive == LC_FALSE)
            {
                nPos = i;
                break;
            }
        }
    }
    
    if(nPos < 0)
    {
        // Check Size : size is less than RETRY_PACKET_MAX
        if(m_xUartRetry.dwSize < RETRY_PACKET_MAX)
        {
            nPos = m_xUartRetry.dwSize;

            m_xUartRetry.dwSize++;
            m_xUartRetry.dwPos = m_xUartRetry.dwSize;
        }
        // size is FULL and ALL PACKET Running
        else
        {
            if(m_xUartRetry.dwPos >= RETRY_PACKET_MAX)
            {
                m_xUartRetry.dwPos = 0;
            }
            nPos = m_xUartRetry.dwPos;
      
            m_xUartRetry.dwPos++;
        }
    }

    memcpy(m_xUartRetry.xInfo[nPos].pBuffer, pBuffer, cbData);
    m_xUartRetry.xInfo[nPos].dwSize = cbData;

    LCAPI_SetTimer(m_dwID, nPos, 200);
    m_xUartRetry.xInfo[nPos].bActive = LC_TRUE;

    return nPos;
}

static int BuildData(LC_BYTE *pBuffer, LC_DWORD cbData, LC_BOOL bAckRequest)
{
    LC_DWORD dwDstSize;
    LC_BYTE DstBuffer[SEND_BUFFER_SIZE * 2];

    m_dwSendLink = (m_dwSendLink + 1) & 0xFF;
    pBuffer[0] = (LC_BYTE)m_dwSendLink;

    if(bAckRequest == LC_TRUE)
    {
        BuildRetry(pBuffer, cbData);
    }

    dwDstSize = BuildPacket(DstBuffer, pBuffer, cbData);

    return LCDRV_Write(*(g_phDevice + DEV_UART4), DstBuffer, dwDstSize);
}
