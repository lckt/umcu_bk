//----------------------------------------------
//  Headers
//----------------------------------------------
#include "TaskCanV.h"


//----------------------------------------------
//  Definitions
//----------------------------------------------
#define EVENT_MASK              (EVENT_MESSAGE | EVENT_CAN1_RC)

#define TID_READ_CANINT         1300
#define TID_SMART_VOL_REL       1301

#define SMART_VOL_REL_ELAPSE    100

#define CHANGE_CONFIRM          1
#define NO_CHANGE_CONFIRM       3
#define NO_CHANGE_MAX           30

#define STDID_MAP(x)            (x << 5)
#define STDID_MAP_MASK          0xFFE0

#define EXTID_MAP(x)            (x << 3)
#define EXTID_MAP_MASK          0xFFFFFFF8


//----------------------------------------------
//  Variables
//----------------------------------------------
extern LC_HANDLE *g_phEvent;
extern LC_HANDLE *g_phQueue;
extern LC_HANDLE *g_phDevice;

static LC_DWORD m_dwID;

static volatile LC_BOOL m_bSuspend = LC_FALSE;

//  Task Queue
static LC_BYTE m_MsgQ[128];

static const xLC_Q xQueueList[1] = {
    {   .wHead = 0, .wTail = 0, .pBuffer = m_MsgQ,  .dwSize = sizeof(m_MsgQ), },
};


//  CAN Filter
xFilterBankInfoType xCAN1_FILTERBANK_INFO[] =
{
    MASKID16BIT, 0, 0, STDID_MAP(CANID_PAS),    STDID_MAP(CANID_PAS),  STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 1, 0, STDID_MAP(CANID_OPS),    STDID_MAP(CANID_OPS),  STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 2, 0, STDID_MAP(CANID_GEAR),   STDID_MAP(CANID_GEAR), STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 3, 0, STDID_MAP(CANID_HVAC),   STDID_MAP(CANID_HVAC), STDID_MAP_MASK, STDID_MAP_MASK,
    EOL
};

xFilterBankInfoType xCAN1_FILTERBANK_INFO_SMART[] =
{
    MASKID16BIT, 0, 0, STDID_MAP(CANID_SMART_PAS),  STDID_MAP(CANID_SMART_PAS), STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 1, 0, STDID_MAP(CANID_SMART_GEAR), STDID_MAP(CANID_SMART_GEAR),STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 2, 0, STDID_MAP(CANID_SMART_KEY),  STDID_MAP(CANID_SMART_KEY), STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 3, 0, STDID_MAP(CANID_SMART_ILL),  STDID_MAP(CANID_SMART_ILL), STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 4, 0, STDID_MAP(CANID_SMART_HVAC), STDID_MAP(CANID_SMART_HVAC),STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 5, 0, STDID_MAP(CANID_SMART_DOOR), STDID_MAP(CANID_SMART_DOOR),STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 6, 1, STDID_MAP(CANID_SMART_1A1),  STDID_MAP(CANID_SMART_1A1), 0             , 0,
    EOL
};

xFilterBankInfoType xCAN1_FILTERBANK_INFO_VITO[] =
{
    MASKID16BIT, 0, 0, STDID_MAP(CANID_VITO_PAS),   STDID_MAP(CANID_VITO_PAS),  STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 1, 0, STDID_MAP(CANID_VITO_OPS),   STDID_MAP(CANID_VITO_OPS),  STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 2, 0, STDID_MAP(CANID_VITO_GEAR),  STDID_MAP(CANID_VITO_GEAR), STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 3, 0, STDID_MAP(CANID_VITO_ILL),   STDID_MAP(CANID_VITO_ILL),  STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 4, 0, STDID_MAP(CANID_VITO_RMC),   STDID_MAP(CANID_VITO_RMC),  STDID_MAP_MASK, STDID_MAP_MASK,
    MASKID16BIT, 5, 1, STDID_MAP(CANID_VITO_DOOR),  STDID_MAP(CANID_VITO_DOOR), 0             , 0,
    EOL
};

xFilterBankInfoType xCAN2_FILTERBANK_INFO[] =
{
    EOL
};


//  CAN Data
static LC_BYTE m_Info[4] = { 0xFF, 0xFF, 0xFF, 0xFF };
static LC_BYTE m_Hvac[8] = { 0xFF, 0xFF, 0xFF, 0xFF };

//  Vehicle Type
static eVEHICLE_TYPE m_eVehicleType;


//----------------------------------------------
//  Private Functions
//----------------------------------------------
//  Initial
static LC_BOOL Init(LC_PVOID arg);
static void OnResume();
static void OnSuspend();

//  Event & Queue
static LC_BOOL OnEventNotify(LC_DWORD dwEvent);
static void CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize);

//  Recv Command
static void DoSuspend(LC_DWORD dwCmd);
static void DoResume(LC_DWORD dwCmd);
static void DoOnTimer(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize);
static void DoRecvCAN1Notify(LC_DWORD dwCmd);

//  Send Command
static void PostSuspendDone(LC_DWORD dwID);
static void PostResumeDone(LC_DWORD dwID);

//  Recv CAN
static void RecvSmartPas(LC_BYTE *pBuffer);
static void RecvSmartGear(LC_BYTE *pBuffer);
static void RecvSmartKey(LC_BYTE *pBuffer);
static void RecvSmartIll(LC_BYTE *pBuffer);
static void RecvSmartHvac(LC_BYTE *pBuffer);
static void RecvSmartDoor(LC_BYTE *pBuffer);

static void RecvVitoPas(LC_BYTE *pBuffer);
static void RecvVitoOps(LC_BYTE *pBuffer);
static void RecvVitoGear(LC_BYTE *pBuffer);
static void RecvVitoIll(LC_BYTE *pBuffer);
static void RecvVitoRmc(LC_BYTE *pBuffer);


//----------------------------------------------
//  Functions
//----------------------------------------------
void TaskCanV(LC_PVOID arg)
{
    LC_DWORD dwEvent;

    if(Init(arg) == LC_FALSE)
        return;

    do
    {
        dwEvent = LCAPI_WaitEvent(*(g_phEvent + m_dwID), EVENT_MASK, WAIT_INFINITE);

        if(dwEvent != 0)
            OnEventNotify(dwEvent);
    }
    while(LC_TRUE);
}

// Initial
static LC_BOOL Init(LC_PVOID arg)
{
    LC_WORD *pwBuffer = (LC_WORD *)arg;
  
    m_dwID = pwBuffer[0];
  
    *(g_phEvent + m_dwID) = LCAPI_CreateEvent();
    if(*(g_phEvent + m_dwID) == NULL) return LC_FALSE;
 
    *(g_phQueue + m_dwID) = (void *)LCAPI_CreateQ((xLC_Q *)&xQueueList[0]);
    if(*(g_phQueue + m_dwID) == (void *)LC_DEINIT) return LC_FALSE;

///    OnResume();
    
    LCAPI_PostEvent(ID_MC, EVENT_INIT);

    return LC_TRUE;
}

static void OnResume()
{
    //  SetTimer
    LCAPI_SetTimer(m_dwID, TID_READ_CANINT, 1000);
    
    //  Init Lib Function ... & etc
    
    //  Device Driver Open
    LC_DWORD dwBootType = LCLIB_GetBootType();
    switch(dwBootType)
    {
    case RUN_APP:
    case RUN_FACTORY:
        {
            m_eVehicleType = LCAPI_GetVehicle();
            
            switch(m_eVehicleType)
            {
            case VEHICLE_SMART:
                *(g_phDevice + DEV_CAN1) = LCDRV_Open(DEV_CAN1, xCAN1_FILTERBANK_INFO_SMART);
                break;
            case VEHICLE_VITO:
                *(g_phDevice + DEV_CAN1) = LCDRV_Open(DEV_CAN1, xCAN1_FILTERBANK_INFO_VITO);
                break;
            }
        }
        break;
    }
}

static void OnSuspend()
{
    //  Set for Energy Saving
    
    //  Device Driver Closed
    LCDRV_Close(*(g_phDevice + DEV_CAN1));

    //  Timer Kill
    LCAPI_KillTimer(m_dwID, TID_READ_CANINT);
}


//  Event & Queue
static LC_BOOL OnEventNotify(LC_DWORD dwEvent)
{
    LC_DWORD dwCmd;
    LC_DWORD dwSize;
    LC_BYTE Buffer[128];

	if(dwEvent & EVENT_CAN1_RC)
	{
		CommandProcess(IDM_CAN1_DIFF_NOTIFY, NULL, 0);
	}
    if(dwEvent & EVENT_MESSAGE)
    {
        do
        {
            if(*(g_phQueue + m_dwID) == (LC_HANDLE)LC_DEINIT) break;
            
            int nLength;
            if((nLength = LCAPI_GetQLength((LC_DWORD)*(g_phQueue + m_dwID))) >= QMESSAGE_HEADER_SIZE)
            {
                dwSize = LCAPI_GetQMessage((LC_DWORD)*(g_phQueue + m_dwID), &dwCmd, Buffer, nLength - QMESSAGE_HEADER_SIZE);
                
                CommandProcess(dwCmd, (LC_PVOID)Buffer, dwSize);
            }
            else
                break;
        }
        while(LC_TRUE);
    }

    return LC_TRUE;
}

static void CommandProcess(LC_DWORD dwCmd, LC_PVOID pData, LC_DWORD dwSize)
{
    switch(dwCmd & 0xFFFF)
    {
    case IDM_PROCESS_SUSPEND:
        DoSuspend(dwCmd);
        break;
    case IDM_PROCESS_RESUME:
        DoResume(dwCmd);
        break;
    case IDM_ON_TIMER:
        DoOnTimer(dwCmd, pData, dwSize);
        break;
    case IDM_CAN1_DIFF_NOTIFY:
        DoRecvCAN1Notify(dwCmd);
        break;
    default:
        LCAPI_Printf("(0x%X) Unknown Command 0x%X", m_dwID, dwCmd);
        break;
    }
}

//  Process
static void DoSuspend(LC_DWORD dwCmd)
{
    m_bSuspend = LC_TRUE;
    
    OnSuspend();

    PostSuspendDone(ID_MC);
}

static void DoResume(LC_DWORD dwCmd)
{
    m_bSuspend = LC_FALSE;
    
    OnResume();
    
    PostResumeDone(ID_MC);
}

static void DoOnTimer(LC_DWORD dwCmd, void *pData, LC_DWORD dwSize)
{
    LC_DWORD *pdwBuffer = (LC_DWORD *)pData;
    static LC_DWORD dwCanIntCount = 0;
    static LC_S32 nChangeCount = 0, nNoChangeCount = 0;

    switch(pdwBuffer[0])
    {
    case TID_READ_CANINT:
        {
            LC_DWORD dwCount;
            if(LCDRV_Ioctl(*(g_phDevice + DEV_CAN1), IOCTL_READ_CANINT, NULL, 0, &dwCount, sizeof(LC_DWORD)) > 0)
            {
                if(dwCanIntCount != dwCount)
                {
                    dwCanIntCount = dwCount;

                    if(nChangeCount < CHANGE_CONFIRM)
                    {
                        nChangeCount++;
                        if(nChangeCount >= CHANGE_CONFIRM)
                        {
                            LCAPI_PostMessage(ID_SYSTEM, IDM_CANDATA_RECEIVED, NULL, 0);
                        }
                    }
                    
                    if(nNoChangeCount != 0) nNoChangeCount = 0;
                    
//                    LCAPI_CanRecv_Putc(0xEB);
                }
                else
                {
                    if(nNoChangeCount < NO_CHANGE_MAX)
                    {
                        nNoChangeCount++;
                        if(nNoChangeCount == NO_CHANGE_CONFIRM)
                        {
                            LCAPI_PostMessage(ID_SYSTEM, IDM_CANDATA_TIMEOUT, NULL, 0);
                        }
                        else if(nNoChangeCount >= NO_CHANGE_MAX)
                        {
///                            LCAPI_KillTimer(m_dwID, TID_READ_CANINT);
//                            LCAPI_SetPhyStb(LC_FALSE);
                            //  PostMessage
                            LCAPI_PostMessage(ID_SYSTEM, IDM_CANDATA_CLOSED, NULL, 0);
                        }
                    }
                    
                    if(nChangeCount != 0) nChangeCount = 0;
                }
            }
            else
            {
                //  Error Display
            }
        }
        break;
    case TID_SMART_VOL_REL:
        {
            LCAPI_KillTimer(m_dwID, TID_SMART_VOL_REL);
            LC_DWORD dwCurr = 0;
            LCAPI_PostMessage(ID_COMMAP, IDM_SEND_BUTTON_VITO, &dwCurr, sizeof(LC_DWORD));
        }
        break;
    }
}

static void DoRecvCAN1Notify(LC_DWORD dwCmd)
{
    int nLength;
    CanTxMsgTypeDef Msg;

    while((nLength = LCDRV_Read(*(g_phDevice + DEV_CAN1), &Msg, sizeof(CanTxMsgTypeDef))) > 0)
    {
        if(nLength == sizeof(CanTxMsgTypeDef))
        {
            switch(m_eVehicleType)
            {
            case VEHICLE_SMART:
                switch(Msg.StdId)
                {
                case CANID_SMART_PAS:
                    RecvSmartPas(Msg.Data);
                    break;
                case CANID_SMART_GEAR:
                    RecvSmartGear(Msg.Data);
                    break;
                case CANID_SMART_KEY:
                    RecvSmartKey(Msg.Data);
                    break;
                case CANID_SMART_ILL:
                    RecvSmartIll(Msg.Data);
                    break;
                case CANID_SMART_HVAC:
                    RecvSmartHvac(Msg.Data);
                    break;
                case CANID_SMART_DOOR:
                    RecvSmartDoor(Msg.Data);
                    break;
                }
                break;
            case VEHICLE_VITO:
                switch(Msg.StdId)
                {
                case CANID_VITO_PAS:
                    RecvVitoPas(Msg.Data);
                    break;
                case CANID_VITO_OPS:
                    RecvVitoOps(Msg.Data);
                    break;
                case CANID_VITO_GEAR:
                    RecvVitoGear(Msg.Data);
                    break;
                case CANID_VITO_ILL:
                    RecvVitoIll(Msg.Data);
                    break;
                case CANID_VITO_RMC:
                    RecvVitoRmc(Msg.Data);
                    break;
                }
                break;
            }
        }
    }
}


//  Send
static void PostSuspendDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_SUSPEND, &m_dwID, sizeof(LC_DWORD));
}

static void PostResumeDone(LC_DWORD dwID)
{
    LCAPI_PostMessage(dwID, IDM_PROCESS_RESUME, &m_dwID, sizeof(LC_DWORD));
}


//  Recv CAN
static void RecvSmartPas(LC_BYTE *pBuffer)
{
    //  Smart Prev  : 0x983C ~ 0x8000 ~ 0x67A4 : Step 270.. 0x98C9 ~ 0x8000 ~ 0x6737
    //  Smart Curr  : 0x9BB8 ~ 0x8000 ~ 0x6480 ; Step 288.. 0x9A70 ~ 0x8000 ~ 0x6590(66B0)
    LC_DWORD dwPasCan = (pBuffer[1] << 8) | (pBuffer[2]);
    
    static LC_S32 nPasPrev = PAS_PICTURE_CENTER;
    LC_S32 nPasCurr = (LC_S32)(PAS_SMART_LIMIT_LEFT - dwPasCan) / PAS_SMART_SECTION_SIZE;

    if(nPasCurr < 0) nPasCurr =  0;
    if(nPasCurr >= PAS_PICTURE_MAX) nPasCurr = PAS_PICTURE_MAX - 1;
    
    if(nPasPrev != nPasCurr)
    {
        nPasPrev = nPasCurr;
        LCAPI_PostMessage(ID_COMMAP, IDM_LOAD_PAS, &nPasCurr, sizeof(LC_S32));
    }
}

static void RecvSmartGear(LC_BYTE *pBuffer)
{
    //  Info 1
    LC_DWORD dwGear = 0;
    
    if((pBuffer[3] & 0x20) == 0x00)
    {
        dwGear = (pBuffer[3] >> 2) & 0x03;
    }
    else
    {
        return;
    }
    
    //  Compare
    if(m_Info[1] != (LC_BYTE)dwGear)
    {
        m_Info[1] = (LC_BYTE)dwGear;
        
        LCAPI_PostMessage(ID_COMMAP, IDM_CAN_GEAR, &dwGear, sizeof(LC_DWORD));
        LCAPI_PostMessage(ID_SYSTEM, (dwGear == 1) ? IDM_RGEAR_ON : IDM_RGEAR_OFF, NULL, 0);
    }
}

static void RecvSmartKey(LC_BYTE *pBuffer)
{
    //  Info 0
    static LC_DWORD dwPrev = 0x0000;
    LC_DWORD dwCurr = 0xFFFF;
    
    switch((pBuffer[2] >> 3) & 0x0F)
    {
        //  OFF
    case 0x0A:  if(!(pBuffer[1] & 0x80)) dwCurr = 1; break;
        //  ACC
    case 0x0B:  if(!(pBuffer[1] & 0x80)) dwCurr = 2; break;
        //  Ignitioning
    case 0x0C:  if(pBuffer[1] & 0x80) dwCurr = 3;    break;
        //  Ignition
    case 0x0D:  if(pBuffer[1] & 0x80) dwCurr = 4;    break;
        //  Engine Start1
    case 0x0E:  if(pBuffer[1] & 0x80) dwCurr = 5;    break;
    }

    if(dwCurr != 0xFFFF)
    {
        //  Compare
        if(m_Info[0] != (LC_BYTE)dwCurr)
        {
            m_Info[0] = (LC_BYTE)dwCurr;
            LCAPI_PostMessage(ID_COMMAP, IDM_CAN_KEY, &dwCurr, sizeof(LC_DWORD));
            LCAPI_SetAcc((dwCurr >= 2) ? LC_TRUE : LC_FALSE);
        }
    }

    //  Volume Button
    dwCurr = 0x0000;
    
    if(pBuffer[0] & 0x0F)
    {
       switch(pBuffer[2] & 0x07)
       {
       case 0x03:
           dwCurr = 0x10;
           break;
       case 0x04:
           dwCurr = 0x20;
           break;
       }
    }

    if(dwPrev != dwCurr)
    {
        dwPrev = dwCurr;
        if(dwCurr)
        {
            LCAPI_PostMessage(ID_COMMAP, IDM_SEND_BUTTON_VITO, &dwCurr, sizeof(LC_DWORD));
        }
        else
        {
            LCAPI_SetTimer(m_dwID, TID_SMART_VOL_REL, SMART_VOL_REL_ELAPSE);
        }
        LCAPI_CanDiff_Putc(0x98);
    }
}

static void RecvSmartIll(LC_BYTE *pBuffer)
{
    //  Info 3
    LC_DWORD dwIll;
    
    //  ILL
    dwIll = (pBuffer[5] & 0x80) ? 0x01 : 0x00;
    //  Light
//    dwIll = (pBuffer[1] & 0x01) ? 0x01 : 0x00;
    
    //  Compare
    if(m_Info[3] != (LC_BYTE)dwIll)
    {
        m_Info[3] = (LC_BYTE)dwIll;
        LCAPI_PostMessage(ID_COMMAP, IDM_CAN_ILL, &dwIll, sizeof(LC_DWORD));
        LCAPI_PostMessage(ID_SYSTEM, IDM_ILL, &m_Info[3], sizeof(LC_BYTE));
        LCAPI_CanDiff_Putc(0x99);
    }
}

static void RecvSmartHvac(LC_BYTE *pBuffer)
{
    LC_BYTE Hvac[8] = { 0 };
    
    //  A/C
    if(pBuffer[6] & 0x01)
        Hvac[0] = Hvac[0] | 0x01;
    
    //  F-WINDOW MAX
    if((pBuffer[7] & 0xC0) == 0x40)
        Hvac[0] = Hvac[0] | 0x08;
    
    //  R-HEAT ON
    if((pBuffer[0] & 0x30) == 0x10)
        Hvac[0] = Hvac[0] | 0x10;
    
    //  WIND STRENGH & AUTO, OFF
    Hvac[7] = (pBuffer[3] >> 4) & 0x0F;
    switch(Hvac[7])
    {
    case 0:
        if((pBuffer[7] & 0xC0) == 0x40)
        {
            Hvac[1] = 4;
        }
        else
        {
            Hvac[0] = Hvac[0] | 0x02;
        }
        break;
    case 1:
    case 2:
    case 3:
    case 4:
        Hvac[1] = Hvac[7];
        break;
    case 0x0F:
        Hvac[0] = Hvac[0] | 0x04;
    default:
        Hvac[1] = 0;
        break;
    }
    
    //  WIND DIRECTION
    switch(pBuffer[2] & 0xB0)
    {
        //  BODY
    case 0x20:
        Hvac[2] = 1;
        break;
    case 0x30:
        Hvac[2] = 2;
        break;
    case 0x10:
        Hvac[2] = 3;
        break;
    case 0x90:
        Hvac[2] = 4;
        break;
    case 0x80:
        Hvac[2] = 5;
        break;
    case 0xB0:
        Hvac[2] = 6;
        break;
    case 0xA0:
        Hvac[2] = 7;
        break;
    }
    
#if 0    
    if(pBuffer[2] & 0x80)
        Hvac[2] = 5;
    else
    {
        switch(pBuffer[2] & 0x30)
        {
        //  BODY
        case 0x20:
            Hvac[2] = 1;
            break;
        //  BODY + LEG
        case 0x30:
            Hvac[2] = 2;
            break;
        //  LEG
        case 0x10: 
            Hvac[2] = 3;
            break;
        }
    }
#endif
    
    //  AIR FLOW
    if(pBuffer[2] & 0x08)
        Hvac[3] = 2;
    else
        Hvac[3] = 1;
    
    //  TEMPERATURE : modify needed
    if(pBuffer[1] == 0x08)
        Hvac[4] = 0;
    else if(pBuffer[1] == 0x0A)
        Hvac[4] = 0x78;
    else if(pBuffer[1] >= 0x24 && pBuffer[1] <= 0x34)
        Hvac[4] = pBuffer[1];

    
    //  Compare
    LC_BOOL bChange = LC_FALSE;

    for(int i = 0; i < 5; i++)
    {
        if(m_Hvac[i] != Hvac[i])
        {
            m_Hvac[i] = Hvac[i];
            bChange = LC_TRUE;
        }
    }
    
    if(bChange == LC_TRUE)
    {
        LCAPI_PostMessage(ID_COMMAP, IDM_CAN_HVAC, Hvac, 5);
        LCAPI_CanDiff_Putc(0x9A);
    }
}

static void RecvSmartDoor(LC_BYTE *pBuffer)
{
    static LC_DWORD dwPrevDoor = 0;
    LC_DWORD dwCurrDoor;
    
    dwCurrDoor = ((pBuffer[3] & 0x08) ? 1 << 0 : 0)
               | ((pBuffer[3] & 0x20) ? 1 << 1 : 0)
               | ((pBuffer[6] & 0x10) ? 1 << 2 : 0)
               | ((pBuffer[6] & 0x40) ? 1 << 3 : 0);

    if(dwPrevDoor != dwCurrDoor)
    {
        dwPrevDoor = dwCurrDoor;
        LCAPI_PostMessage(ID_COMMAP, IDM_CAN_DOOR_OPEN, &dwCurrDoor, sizeof(LC_S32));
    }
}

static void RecvVitoPas(LC_BYTE *pBuffer)
{
    //  VITO  : 0x0BF0 ~ 0x1000 ~ 0x1416 : Step  44.. 0x0BCA ~ 0x1000 ~ 0x140A
    LC_DWORD dwPasCan = (pBuffer[0] << 8) | (pBuffer[1]);
    
    static LC_S32 nPasPrev = PAS_PICTURE_CENTER;
    LC_S32 nPasCurr = (LC_S32)(dwPasCan - PAS_VITO_LIMIT_LEFT) / PAS_VITO_SECTION_SIZE;
    
    if(nPasCurr < 0) nPasCurr = 0;
    else if(nPasCurr >= PAS_PICTURE_MAX) nPasCurr = PAS_PICTURE_MAX - 1;

    if(nPasPrev != nPasCurr)
    {
        nPasPrev = nPasCurr;
        LCAPI_PostMessage(ID_COMMAP, IDM_LOAD_PAS, &nPasCurr, sizeof(LC_S32));
    }
}

static void RecvVitoOps(LC_BYTE *pBuffer)
{
    static LC_BYTE aOpsPrev[4];
    LC_BYTE aOpsCurr[4];
    
    aOpsCurr[0] = (pBuffer[6]     ) & 0x0F;      // rL
    aOpsCurr[1] = (pBuffer[6] >> 4) & 0x0F;      // rR
    aOpsCurr[2] = (pBuffer[3]     ) & 0x0F;      // fL
    aOpsCurr[3] = (pBuffer[3] >> 4) & 0x0F;      // fR
    
    aOpsCurr[0] = (aOpsCurr[0] > OPS_VITO_OFF && aOpsCurr[0] <= OPS_VITO_MAX) ? (OPS_VITO_MAX - aOpsCurr[0]) : OPS_OFF_VAL;
    aOpsCurr[1] = (aOpsCurr[1] > OPS_VITO_OFF && aOpsCurr[1] <= OPS_VITO_MAX) ? (OPS_VITO_MAX - aOpsCurr[1]) : OPS_OFF_VAL;
    aOpsCurr[2] = (aOpsCurr[2] > OPS_VITO_OFF && aOpsCurr[2] <= OPS_VITO_MAX) ? (OPS_VITO_MAX - aOpsCurr[2]) : OPS_OFF_VAL;
    aOpsCurr[3] = (aOpsCurr[3] > OPS_VITO_OFF && aOpsCurr[3] <= OPS_VITO_MAX) ? (OPS_VITO_MAX - aOpsCurr[3]) : OPS_OFF_VAL;

    LC_BOOL bDiff = LC_FALSE;
    for(int i = 0; i < 4; i++)
    {
        if(aOpsPrev[i] != aOpsCurr[i])
        {
            bDiff = LC_TRUE;
            aOpsPrev[i] = aOpsCurr[i];
        }
    }
    
    if(bDiff == LC_TRUE)
    {
        LCAPI_PostMessage(ID_COMMAP, IDM_LOAD_OPS, aOpsCurr, 4);
    
        //  Check Buzzer Value
        LCAPI_PostMessage(ID_SYSTEM, IDM_STEP_BUZZER, aOpsCurr, 4);
    }
}

static void RecvVitoGear(LC_BYTE *pBuffer)
{
    //  Info 1
    LC_DWORD dwGear = 0;
    
    switch(pBuffer[0])
    {
        //  P
    case 0x01:  dwGear = 0; break;
        //  R
    case 0x49:  dwGear = 1; break;
        //  N
    case 0x11:  dwGear = 2; break;
        //  D
    case 0x21:  dwGear = 3; break;
    default:
        return;
    }
    
    //  Compare
    if(m_Info[1] != (LC_BYTE)dwGear)
    {
        m_Info[1] = (LC_BYTE)dwGear;
        
        LCAPI_PostMessage(ID_COMMAP, IDM_CAN_GEAR, &dwGear, sizeof(LC_DWORD));
        LCAPI_PostMessage(ID_SYSTEM, (dwGear == 1) ? IDM_RGEAR_ON : IDM_RGEAR_OFF, NULL, 0);
    }
}

static void RecvVitoIll(LC_BYTE *pBuffer)
{
    //  Info 3
    LC_DWORD dwIll = 0;
    
    if(pBuffer[0] == 0x08 && (pBuffer[1] & 0x0F) == 0x04)
        dwIll = 1;
    
    //  Compare
    if(m_Info[3] != (LC_BYTE)dwIll)
    {
        m_Info[3] = (LC_BYTE)dwIll;
        LCAPI_PostMessage(ID_COMMAP, IDM_CAN_ILL, &dwIll, sizeof(LC_DWORD));
        LCAPI_PostMessage(ID_SYSTEM, IDM_ILL, &m_Info[3], sizeof(LC_BYTE));
    }
}

static void RecvVitoRmc(LC_BYTE *pBuffer)
{
    static LC_DWORD dwPrev = 0x0000;
    LC_DWORD dwCurr = 0x0000;
    
    switch(pBuffer[4])
    {
    case 0x01:
    case 0x02:
    case 0x04:
    case 0x08:
    case 0x10:
    case 0x20:
    case 0x40:
    case 0x80:
        dwCurr = (LC_DWORD)pBuffer[4];
        break;
    default:
        switch(pBuffer[5])
        {
        case 0x01:
        case 0x02:
        case 0x04:
        case 0x08:
            dwCurr = (LC_DWORD)pBuffer[5] << 8;
            break;
        }
        break;
    }
    
    if(dwPrev != dwCurr)
    {
        dwPrev = dwCurr;
        LCAPI_PostMessage(ID_COMMAP, IDM_SEND_BUTTON_VITO, &dwCurr, sizeof(LC_DWORD));
    }
}

